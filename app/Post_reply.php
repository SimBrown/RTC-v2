<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/* This implements model for Forum-Posts-Replies */

class Post_reply extends Model
{
    protected $table = 'forum_posts_replies';

    protected $fillable = ['id','body','post_id','user_id'];
}
