<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveEvent extends Model
{
    protected $table = 'live_events';
    protected $fillable = ['title', 'url', 'id'];
}
