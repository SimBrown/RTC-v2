<?php

namespace App\Http\Controllers;

use App\Category;
use App\Section;
use App\Survey;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Middleware\UserGroupCheck;

class SectionController extends Controller
{
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function manage()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageForum'])){
            return view("errors.no-permission");
        }

        $categories = DB::table('categories')
        ->join('section','categories.section','=','section.id')
        ->select('categories.id','categories.description','categories.section','section.description as section_name')
        ->where('categories.archived','!=','1')
        ->where('section.archived','!=','1')
        ->get();

        $sections = DB::table('section')
        ->select('section.id','section.description')
        ->where('section.archived','!=','1')
        ->get();

    

        return view('admin.forum.section-index',compact('categories','sections'));
    }

          /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archived()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageForum'])){
            return view("errors.no-permission");
        }

        $categories = DB::table('categories')
        ->join('section','categories.section','=','section.id')
        ->select('categories.id','categories.description','categories.section','section.description as section_name')
        ->where('categories.archived','=','1')
        ->where('section.archived','=','1')
        ->get();

        $sections = DB::table('section')
        ->select('section.id','section.description')
        ->where('section.archived','=','1')
        ->get();


    

        return view('admin.forum.section-archived',compact('categories','sections'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function insertOrUpdate(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageForum'])){
            return view("errors.no-permission");
        }

        if($request->input('archived')!=""){

            $section = Section::updateOrCreate(
                ['id' => $request->input('sec_id')],
                ['archived'=> $request->input('archived')]
            );
        }
        else{
            $section = Section::updateOrCreate(
                ['id' => $request->input('form_sec_id')],
                ['description' => $request->input('form_description')]
            );
        }
        
        $sections = DB::table('section')
        ->select('section.id','section.description')
        ->where('section.archived','!=','1')
        ->get();

        $categories = DB::table('categories')
        ->join('section','categories.section','=','section.id')
        ->select('categories.id','categories.description','categories.section','section.description as section_name')
        ->where('categories.archived','!=','1')
        ->where('section.archived','!=','1')
        ->get();

        
        return view('admin.forum.section-index',compact('categories','sections'));
    }

}
