<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;
use App\Http\Middleware\UserGroupCheck;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $surveys = DB::table('surveys')
        ->join('survey_options','surveys.id','=','survey_options.survey_id')
        ->whereNull('surveys.result')
        ->select('surveys.*', 'survey_options.id as survey_options_id', 
        'survey_options.description as survey_options_descr')
        ->limit(10)
        ->get();

        return view('survey/index',compact('surveys'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageUsers'])){
            return view("errors.no-permission");
        }

        $users = DB::table('users')
        ->where('archived','!=','1')
        ->get();

        $groups = DB::table('role_groups')
        ->where('id','!=','1')
        ->get();

        return view('admin.users.users-index',compact('users','groups'));
    }

       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manageUserGroups()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageUsers'])){
            return view("errors.no-permission");
        }

        $users = DB::table('users')
        ->where('archived','!=','1')
        ->get();

        $groups = DB::table('role_groups')
        ->where('id','!=','1')
        ->get();

        return view('admin.users.users-group',compact('users','groups'));
    }

    /**
     * Returns user role_groups
     */
    public function getRoleGroup($userId){
        
        $userGroups = DB::table('users')
        ->join('role_groups_user','users.id','=','role_groups_user.user_id')
        ->join('role_groups','role_groups_user.group_id','=','role_groups.id')
        ->select('users.id','users.name','email','role_groups.name as groupName')
        ->where('archived','!=','1')
        ->get();


        return $userGroups;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archived()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageUsers'])){
            return view("errors.no-permission");
        }

        $users = DB::table('users')
        ->select('users.id','users.name','email')
        ->where('archived','=','1')
        ->get();

        return view('admin.users.users-archived',compact('users'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUserGroups(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageUsers'])){
            return view("errors.no-permission");
        }

        $uid = $request->input('user_id');
        $groups = $request->input('groups');
        
        DB::statement("DELETE FROM role_groups_user WHERE user_id = $uid AND group_id != 1");

        foreach($groups as $g){
            DB::statement("INSERT INTO `role_groups_user`(`group_id`, `user_id`) VALUES ('$g','$uid')");
        }

    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function insertOrUpdate(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageUsers'])){
            return view("errors.no-permission");
        }

        if($request->input('archived')!=""){
            
            $user = User::updateOrCreate(
                ['id' => $request->input('user_id')],
                ['archived'=> $request->input('archived')]
            );
        }
        else{
            
             
            $user = User::updateOrCreate(
                ['id' => $request->input('form_user_id')],
                ['email' => $request->input('form_email'),
                'name' => $request->input('form_user_name')]
            );

        }
        
        return redirect(route('user.manage'));
    }
}