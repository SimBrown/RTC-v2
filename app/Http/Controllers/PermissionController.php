<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Middleware\UserGroupCheck;

class PermissionController extends Controller
{
    /**
     * Display list of groups and services
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
         //checking permissions
        if(!UserGroupCheck::check(['ManagePermissions'])){
            return view("errors.no-permission");
        }

        $services = DB::table('services')
        ->select('id','description')
        ->get();

        $groups = DB::table('role_groups')
        ->select('id','name')
        ->get();

        
        return view('admin.settings.permissions.permission',compact("services","groups"));
    }

    /* Sends data to init checkboxes */

    public function init(Request $request){

        //checking permissions
        if(!UserGroupCheck::check(['ManagePermissions'])){
        return view("errors.no-permission");
        }
        
        $groups = DB::table('role_groups')
        ->select('id','name')
        ->get();
        
        $tmp = DB::table('role_groups_services')
        ->select('groupId','serviceID')
        ->get();

        $checkboxes = [];

        foreach($groups as $g){
            $checkboxes[$g->id] = [];
        }

        foreach($tmp as $t){

            array_push($checkboxes[$t->groupId],$t->serviceID);

        }

        return $checkboxes;
    }

    /** Remove a service in a group if exist */
    public function removeServiceFromGroup($groupId,$serviceID)
    {
        DB::delete("DELETE FROM role_groups_services
                    WHERE role_groups_services.groupId = $groupId
                    AND role_groups_services.serviceID = $serviceID");
    }

    /** Add a service in a group if NOT exist */
    public function addServiceToGroup($groupId,$serviceID)
    {
        /** l'indice composto da groupId + serviceID è settato a unique,
         *  verranno quindi ignorate le insert duplicate 
         * */
        DB::insert("INSERT IGNORE INTO role_groups_services (groupId, serviceID)
                    VALUES ($groupId,$serviceID)");
        
    }

    

    /**
     *  This method will be fired by a POST request
     *  and will call addServiceToGroup and removeServiceFromGroup
     *  on data received by request
     */
    public function editPermissions(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManagePermissions'])){
        return view("errors.no-permission");
        }
      
        $toAdd = $request->input('permission_add');

        foreach($toAdd as $key=>$value){
            if($key!=0){
                foreach($toAdd[$key] as $s){
                    self::addServiceToGroup($key,$s);
                }
            }
        }

        $toRem = $request->input('permission_remove');

        foreach($toRem as $key=>$value){
            if($key!=0){
                foreach($toRem[$key] as $s){
                    self::removeServiceFromGroup($key,$s);
                }
            }
        }
    }        

    /** Show all services  */
    public function showGroups()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManagePermissions'])){
        return view("errors.no-permission");
        }
        
        $groups = DB::table('role_groups')->get();

        return view('admin.settings.permissions.groups',compact("groups"));
        
    }

    /** Remove group (Admin can't be deleted) */
    public function removeGroup($groupId)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManagePermissions'])){
        return view("errors.no-permission");
        }
        /* Server-side check to not delete admin group */
        if($groupId != 1){
            DB::delete("DELETE FROM role_groups
                        WHERE id = $groupId");
        }  
    }

    /** Edit group (Admin can't be edited) */
    public function editGroup(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManagePermissions'])){
            return view("errors.no-permission");
            }

        $groupId = $request->input('groupId');
        $groupName = $request->input('groupName');
        $groupDescription = $request->input('groupDescription');
        
        /* Server-side check to not edit admin group */
        if($groupId != 1){
            DB::table('role_groups')
            ->where('id', $groupId)
            ->update(['name' => $groupName,'description' => $groupDescription ]);
        } 
        return redirect(route('settings.manageGroups'));
    }

    /** Remove group (Admin can't be deleted) */
    public function addGroup(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManagePermissions'])){
            return view("errors.no-permission");
            }

        $groupName = $request->input('groupName');
        $groupDesc = $request->input('groupDesc');

        DB::table('role_groups')->insert(
            ['name' => $groupName, 'description' => $groupDesc]
        );

        return redirect(route('settings.manageGroups'));
    }


}

