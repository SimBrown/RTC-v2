<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class notificationController extends Controller
{
     /**
     * Show user settings page.
     *
     * @return \Illuminate\Http\Response
    */
    public function thunderCheck()
    {
        $uid = Auth::User()->id;

        // $result =  DB::table('notifications')
        // ->select('notifications.id','notifications.user_id','notifications.body')
        // ->where('notifications.user_id', '=', $uid)
        // ->get();

        $notification = DB::select("SELECT users.name as user_from_name, users.surname as user_from_surname,notifications.user_from as user_from_id,users.image,notifications.post_id, notifications.type, (SELECT COUNT(id)FROM notifications WHERE user_id = $uid AND hasRead != 1  AND (notifications.type = 'comment' OR notifications.type = 'like')) AS notifications_amount
        FROM (notifications JOIN users ON notifications.user_from = users.id)
        WHERE user_id = $uid AND notifications.hasRead != 1 AND (notifications.type = 'comment' OR notifications.type = 'like')
        ORDER BY notifications.id DESC");

        return view('notifications.notification-block',compact('notification'));
        
    }

    public function allNotificationCheck(){
        $result = array();
        
        $result['thunder_notifications'] = notificationController::thunderCheck()->render();
        $result['message_notifications'] = notificationController::message_check()->render();
        $result['friends_notifications'] = notificationController::friends_check()->render();

        return json_encode($result);
    }
    
    /*Message notifications check */
    public function message_check()
    {
        $uid = Auth::User()->id;

        // $result =  DB::table('notifications')
        // ->select('notifications.id','notifications.user_id','notifications.body')
        // ->where('notifications.user_id', '=', $uid)
        // ->get();
     
        $message_notification = DB::select("SELECT users.name as user_from_name, users.surname as user_from_surname,notifications.user_from as user_from_id,users.image,notifications.post_id, notifications.type, (SELECT COUNT(id)FROM notifications WHERE user_id = $uid AND hasRead != 1 AND notifications.type = 'message') AS notifications_amount
        FROM (notifications JOIN users ON notifications.user_from = users.id)
        WHERE user_id = $uid AND notifications.hasRead != 1 AND notifications.type = 'message'
        ORDER BY notifications.id DESC");
        

        return view('notifications.message-notifications-block',compact('message_notification'));
        
    }

    /*Message notifications check */
    public function friends_check()
    {
        $uid = Auth::User()->id;

        // $result =  DB::table('notifications')
        // ->select('notifications.id','notifications.user_id','notifications.body')
        // ->where('notifications.user_id', '=', $uid)
        // ->get();
     
        $friends_notification = DB::select("SELECT users.name as user_from_name, users.surname as user_from_surname,notifications.user_from as user_from_id,users.image,notifications.post_id, notifications.type, 
        (SELECT COUNT(id)FROM notifications WHERE user_id = $uid AND hasRead != 1 AND notifications.type = 'friends') AS notifications_amount
        FROM (notifications JOIN users ON notifications.user_from = users.id)
        WHERE user_id = $uid AND notifications.hasRead != 1 AND notifications.type = 'friends'
        ORDER BY notifications.id DESC");

        return view('notifications.friends-notifications-block',compact('friends_notification'));
        
    }


      /**
     * Marks all notifications as read.
     *
     * @return \Illuminate\Http\Response
    */
    public function read()
    {
        $uid = Auth::User()->id;

        // $result =  DB::table('notifications')
        // ->select('notifications.id','notifications.user_id','notifications.body')
        // ->where('notifications.user_id', '=', $uid)
        // ->get();
        
        $type = $_GET['type'];

        switch($type){

            case "thunder" : DB::statement("UPDATE `notifications` SET `hasRead`= '1' WHERE user_id = $uid AND (type='like' OR type='comment') AND hasRead = '0' "); break;
            case "messages" : DB::statement("UPDATE `notifications` SET `hasRead`= '1' WHERE user_id = $uid AND type='message' AND hasRead = '0' "); break;
            case "friends" : DB::statement("UPDATE `notifications` SET `hasRead`= '1' WHERE user_id = $uid AND type='friends' AND hasRead = '0' "); break;
                        
        }

    }
}
