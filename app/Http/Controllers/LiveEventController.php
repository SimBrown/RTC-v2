<?php

namespace App\Http\Controllers;

use App\LiveEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use Auth;

class LiveEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $liveEvents = LiveEvent::all();

        return view('liveEvent/index',compact('liveEvents'));
    }

    public function insertOrUpdate(Request $request)
    {
        if(!Auth::user()){ return "Devi essere Autenticato."; }             
            else  if(!Auth::user()){ return "Devi essere Autenticato."; }   
              else if(Auth::user()->role != '2') return "<h2>Admin Area</h2> <br> Accesso Negato. <br> (set role to 0 to become admin)";

        LiveEvent::updateOrCreate(
            ['id' => $request->input('liveEvent_id')],
            ['url' => $request->input('url'),
            'title' => $request->input('title')]
        );

        $liveEvent = LiveEvent::orderBy('id','desc')->get();

        return view('admin.liveEvent.admin-liveEvent',compact('liveEvent'));
    }

    public function manage()
    {
      /*   if(!Auth::user()){ return "Devi essere Autenticato."; }             
         else  if(!Auth::user()){ return "Devi essere Autenticato."; }   
           else if(Auth::user()->role != '2') return "<h2>Admin Area</h2> <br> Accesso Negato. <br> (set role to 0 to become admin)";
 */

        $liveEvent = LiveEvent::orderBy('id','desc')->get();
        
        return view('admin.liveEvent.admin-liveEvent',compact('liveEvent'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $liveEvent = LiveEvent::find($id);

        return view('liveEvent/show',compact('liveEvent'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('liveEvent/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $liveEvent = new liveEvent;
        $liveEvent->title = $request->input('title');
        $liveEvent->url = $request->input('url');
        
        
        $liveEvent->save();

        return $this->show($liveEvent->id);
    }
}