<?php

namespace App\Http\Controllers;

use App\Post;
use App\Post_reply;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Storage;
use App\Http\Middleware\UserGroupCheck;

class PostController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($pid)
    {
        if(!Auth::user()){ return "Devi essere Autenticato."; }             
        
        $post = DB::table('forum_posts')
            ->where('forum_posts.id', '=', $pid)
            ->join('users', 'users.id', '=', 'forum_posts.user_id')
            ->select(
                "forum_posts.id as post_id",
                "users.name as p_author",
                "forum_posts.body",
                "forum_posts.created_at as p_created_at",
                "forum_posts.title",
                "forum_posts.image"
            )
            ->get();

        $replies = DB::table('forum_posts')
            ->join('forum_posts_replies', 'forum_posts.id', '=', 'forum_posts_replies.post_id')
            ->join('users', 'users.id', '=', 'forum_posts_replies.user_id')
            ->where('forum_posts.id', '=', $pid)
            ->select(
                "users.name as r_author",
                "forum_posts_replies.body",
                "forum_posts_replies.created_at as r_created_at"
            )
            ->orderBy('forum_posts_replies.created_at','asc')
            ->get();
        
        $query = DB::table('categories')
            ->join('forum_posts', 'forum_posts.category_id', '=', 'categories.id')
            ->join('section', 'categories.section', '=', 'section.id')
            ->where('forum_posts.id', '=', $pid)
            ->select('section.description as sec_name', 'categories.description as cat_name', 'categories.id as cat_id')
            ->get();

        $path['section'] = $query[0]->sec_name;
        $path['category'] = $query[0]->cat_name;
        $path['category_id'] = $query[0]->cat_id;

        $featured_topics = Post::orderBy('id','desc')->limit(3)->get();
        
        $recent_topics = DB::select("SELECT a.*, b.name as name FROM forum_posts a 
                                    JOIN users b on a.user_id = b.id
                                    ORDER BY a.created_at ASC");

        return view('forum.showPost', compact('post', 'replies', 'path','featured_topics', 'recent_topics'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageForum'])){
        return view("errors.no-permission");
        }   

        $posts = DB::table('forum_posts')
            ->join('users', 'forum_posts.user_id', '=', 'users.id')
            ->join('categories', 'forum_posts.category_id', '=', 'categories.id')
            ->join('section', 'categories.section', '=', 'section.id')
            ->select('forum_posts.id as postId', 'forum_posts.title', 'forum_posts.image', 'forum_posts.reputation', 'name as author', 'forum_posts.created_at', 'categories.description as catName', 'categories.id as cat_id', 'section.description as secName')
            ->where('forum_posts.archived', '!=', 1)
            ->get(); 

        $categories = DB::table('categories')
            ->select('categories.id', 'categories.description', 'categories.section')
            ->where('categories.archived', '!=', '1')
            ->get();

        return view('admin.forum.post-index', compact('posts', 'categories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archived()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageForum'])){
            return view("errors.no-permission");
        }

        $posts = DB::table('forum_posts')
            ->join('users', 'forum_posts.user_id', '=', 'users.id')
            ->join('categories', 'forum_posts.category_id', '=', 'categories.id')
            ->join('section', 'categories.section', '=', 'section.id')
            ->select('forum_posts.id as postId', 'forum_posts.title', 'forum_posts.reputation', 'name as author', 'forum_posts.created_at', 'categories.description as catName', 'categories.id as cat_id', 'section.description as secName')
            ->where('forum_posts.archived', '=', 1)
            ->get();

        $categories = DB::table('categories')
            ->select('categories.id', 'categories.description', 'categories.section')
            ->where('categories.archived', '=', '1')
            ->get();

        return view('admin.forum.post-archived', compact('categories', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insertOrUpdate(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageForum'])){
            return view("errors.no-permission");
        }

        if (null !== $request->file('image')) {
            $path = Storage::put('public/images', $request->file('image'));
            // cause of a problem storing images we have to cut off the prefix "public/" from the path
            $path = substr($path, 7);
        }

        if ($request->input('archived') != "") {
            $post = Post::updateOrCreate(
                ['id' => $request->input('post_id')],
                ['archived' => $request->input('archived'),
                'user_id' => Auth::user()->id]);
        } else {
            $post = Post::updateOrCreate(
                ['id' => $request->input('form_sec_id')],
                ['title' => $request->input('form_description'),
                    'category_id' => $request->input('sec_select'),
                    'user_id' => Auth::user()->id]
            );
        }
        if (isset($path)) {
            Post::updateOrCreate(
                ['id' => $post->id],
                ['image' => $path]);
        }

        $posts = DB::table('forum_posts')
            ->join('users', 'forum_posts.user_id', '=', 'users.id')
            ->join('categories', 'forum_posts.category_id', '=', 'categories.id')
            ->join('section', 'categories.section', '=', 'section.id')
            ->select('forum_posts.id as postId', 'forum_posts.title', 'forum_posts.reputation', 'name as author', 'forum_posts.created_at', 'categories.description as catName', 'categories.id as cat_id', 'section.description as secName')
            ->where('forum_posts.archived', '!=', 1)
            ->get();

        $categories = DB::table('categories')
            ->select('categories.id', 'categories.description', 'categories.section')
            ->where('categories.archived', '!=', '1')
            ->get();

        return view('admin.forum.post-index', compact('categories', 'posts'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insertEditReply(Request $request, $pid)
    {
        Post_reply::updateOrCreate(
            ['id' => $request->input('reply_id')],
            ['post_id' => $pid,
             'body' => $request->input('reply_body'),
             'user_id' => Auth::User()->id]
        );

        return \Redirect::route('forum.showPost', [$pid]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //TODO
        return view('forum.createPost');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post;
        $post->body = $request->input('body');

        $post->save();

        return Redirect::to('forum');
    }

}
