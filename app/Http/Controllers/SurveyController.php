<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Survey;
use App\SurveyOption;
use App\SurveyAnswer;
use DB;
use App\Http\Middleware\UserGroupCheck;

class SurveyController extends Controller
{   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function options(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check([3])){
            return view("errors.no-permission");
        }

        $survey_options = DB::table('survey_options')
        ->where('survey_options.survey_id', '=', $request->input('survey_id'))
        ->selectRaw('survey_options.*, COUNT(survey_options.id) as options')
        ->groupBy('survey_options.id')
        ->get();

        return response()->json(['options' => $survey_options]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageSurvey'])){
            return view("errors.no-permission");
        }

        $surveys =  Survey::All();

        return view('admin.survey/index',compact('surveys'));
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insertOrUpdate(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageSurvey'])){
            return view("errors.no-permission");
        }

        $survey = Survey::updateOrCreate(
            ['id' => $request->input('survey_id')],
            ['description' => $request->input('description'),
            'title' => $request->input('title'),
            'reward' => (int)$request->input('reward')]
        );

        $option_counter = (int) $request->input('option_counter');
        while ($option_counter > 0) {
            SurveyOption::updateOrCreate(
                ['id' => $request->input('option_hidden' . $option_counter)],
                ['survey_id' => $survey->id,
                'description' => $request->input('option' . $option_counter)]
            );
            $option_counter = $option_counter - 1;
        }

        $surveys =  Survey::All();

        return view('admin.survey/index',compact('surveys'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $survey_answer = new SurveyAnswer;
        $survey_answer->survey_option_id = $request->input('survey_option_id'); 
        $survey_answer->survey_id = $request->input('survey_id');        
        $survey_answer->user_id = Auth::user()->id;

        $survey_answer->save();
       
        return Redirect::to('survey/' + (int) $request->input('survey_id'));
    }

    public function storeHome(Request $request)
    {
        $survey_answer = new SurveyAnswer;
        $survey_answer->survey_option_id = $request->input('survey_option_id'); 
        $survey_answer->survey_id = $request->input('survey_id');        
        $survey_answer->user_id = Auth::user()->id;

        $survey_answer->save();
       
        return Redirect::to('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $surveys = DB::table('surveys')
        ->leftjoin('survey_options','survey_options.survey_id','=','surveys.id')
        ->whereNull('surveys.result')
        ->where('surveys.id',$id)
        ->select('surveys.*', 'survey_options.id as survey_options_id', 
        'survey_options.description as survey_options_descr')
        ->limit(10)
        ->get();

        $survey = new Survey;

        if(count($surveys)>0){     
            $survey->id = $surveys[0]->id;        
            $survey->title = $surveys[0]->title;
            $survey->description = $surveys[0]->description;
        }
        
        return view('survey/show',compact('surveys', 'survey'));
    }

}
