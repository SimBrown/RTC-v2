<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class chatController extends Controller
{
    /**
     * Get chat of selected user
    */
    public function getChat($friend_id)
    {
        $uid = Auth::User()->id;

        $friend = DB::table('users')
        ->select('users.id','users.name','users.surname','users.image')
        ->where('users.id','=',$friend_id)
        ->get();

        $chat_messages = DB::select("SELECT chat_messages.*,users.image,users.name,users.surname FROM (chat_messages JOIN users ON users.id = chat_messages.from_id) WHERE (chat_messages.from_id = $friend_id AND chat_messages.to_id = $uid) OR (chat_messages.from_id = $uid AND chat_messages.to_id = $friend_id)ORDER BY chat_messages.time");

        return view('chat.chat-ajax',compact('friend','chat_messages'));
    }

    /**
     * Send message to user (chat)
    */
    public function sendMessage($friend_id)
    {
        $uid = Auth::User()->id;

        $message_body = $_POST['message-body'];

        DB::table('chat_messages')
        ->insert(['from_id' => Auth::User()->id, 'body' => $message_body, 'to_id' => $friend_id]);

        Controller::send_message_notification($friend_id);

        $message = collect();

        $message->body = $message_body;

        $message->time = date('Y-m-d H:i:s', time());

        return view('chat.single-message-ajax',compact("message"));


    }

        /**
     * Send message to user (profile messenger)
    */
    public function sendMessage_ProfileMessenger($friend_id)
    {
        $uid = Auth::User()->id;

        $message_body = $_POST['message-body'];

        DB::table('chat_messages')
        ->insert(['from_id' => Auth::User()->id, 'body' => $message_body, 'to_id' => $friend_id]);

        Controller::send_message_notification($friend_id);

        $c = collect();

        $c->body = $message_body;

        $c->user_id = $friend_id;

        $c->time = date('Y-m-d H:i:s', time());

        return view('profile.single-message-ajax',compact("c"));

    }

    
    /**
     * Looks for new messages
    */
    public function message_new_get($friend_id)
    {
        $uid = Auth::User()->id;

        $last_message_time = $_POST['last_message_time'];

        $new_messages = DB::raw("SELECT * FROM chat_messages 
        WHERE ((chat_messages.from_id = $friend_id AND chat_messages.to_id = $uid)) 
        AND chat_messages.time > '$last_message_time' ORDER BY chat_messages.time");

        $new_messages = DB::select($new_messages);
        
        $image = DB::table('users')->select('users.image')->where('users.id','=',$friend_id)->get();

        return view('chat.message-group-ajax',compact("new_messages",'image'));

    }
}
