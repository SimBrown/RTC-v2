<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\UserGroupCheck;

class AdminController extends Controller
{
     /**
     * Show the admin dashboard.
     *
     */
    public function index()
    {  
        //checking permissions
        if(!UserGroupCheck::check(["AdminDash"])){
            return view("errors.no-permission");
        }
        else return view("admin.dash-home");
    }
}
