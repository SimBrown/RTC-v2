<?php

namespace App\Http\Controllers;

use App\Prize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;


class PrizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prize = Prize::orderBy('prizes.id','desc')
            ->leftjoin('prize_users','prize_users.prizes_id','=','prizes.id')
            ->whereNull('prize_users.id')
            ->limit(10)
            ->get();
        
        return view('prize/index',compact('prize'));
    }

    public function insertOrUpdate(Request $request)
    {
        if(!Auth::user()){ return "Devi essere Autenticato."; }             
            else  if(!Auth::user()){ return "Devi essere Autenticato."; }   
              else if(Auth::user()->role != '2') return "<h2>Admin Area</h2> <br> Accesso Negato. <br> (set role to 0 to become admin)";

              Prize::updateOrCreate(
            ['id' => $request->input('prize_id')],
            ['description' => $request->input('description'),
            'cost' => $request->input('cost')]
        );

        $prize = Prize::orderBy('id','desc')->get();

        return view('admin.prize.admin-prize',compact('prize'));
    }

    public function manage()
    {
        if(!Auth::user()){ return "Devi essere Autenticato."; }             
         else  if(!Auth::user()){ return "Devi essere Autenticato."; }   
           else if(Auth::user()->role != '2') return "<h2>Admin Area</h2> <br> Accesso Negato. <br> (set role to 0 to become admin)";


        $prize = Prize::orderBy('id','desc')->get();
        
        return view('admin.prize.admin-prize',compact('prize'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('prize/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $prize = new Prize;
        $prize->description = $request->input('description'); 
        $prize->cost = $request->input('cost');        


        $prize->save();
       
        return $this->show($prize->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prize  $prize
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prize = Prize::find($id);
        
        return view('prize/show',compact('prize'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prize  $prize
     * @return \Illuminate\Http\Response
     */
    public function edit(Prize $prize)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prize  $prize
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prize $prize)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prize  $prize
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prize $prize)
    {
        //
    }
}
