<?php

namespace App\Http\Controllers;

use App\News;
use App\NewsLike;
use App\NewsComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Middleware\UserGroupCheck;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();

        return view('news/index')->with(['news'=>$news]);
    }

    public function insertOrUpdate(Request $request)
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageNews'])){
            return view("errors.no-permission");
        }
       
        if (null !== $request->file('image')) {
            $path = Storage::put('public/images', $request->file('image'));
            // cause of a problem storing images we have to cut off the prefix "public/" from the path
            $path = substr($path, 7);
        }
    
        $new = News::updateOrCreate(
            ['id' => $request->input('news_id')],
            ['content' => $request->input('content'),
            'title' => $request->input('title')]
        );

        if (isset($path)) {
            News::updateOrCreate(
                ['id' => $new->id],
                ['image' => $path]);
        }

        $news = News::orderBy('id','desc')->get();

        return view('admin.news.admin-news',compact('news'));
    }

    public function manage()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageNews'])){
            return view("errors.no-permission");
        }

        $news = News::orderBy('id','desc')->get();
        return view('admin.news.admin-news',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = new News;
        $news->title = $request->input('title');
        $news->content = $request->input('content');
        
        
        $news->save();

        return Redirect::to('news');
    }

    public function comment(Request $request) {
        $news_comment = new NewsComment;
        $news_comment->user_id = $request->input('user_id');
        $news_comment->news_id = $request->input('news_id');
        $news_comment->body = $request->input('body');
        $news_comment->save();
    }

    public function getComment(Request $request) {
        $news_comment = DB::table('news_comments')
        ->join('users','news_comments.user_id','=','users.id')
        ->where('news_comments.news_id',$request->input('news_id'))
        ->get();;
        return response()->json(['news_comments'=>$news_comment]);
    }

    public function like(Request $request) {
        $PRESSED = "0";
        $NOT_PRESSED = "1";
        if ($request->input('like') !== $PRESSED) {
            $news_like = DB::table('news_likes')
            ->where('user_id', '=', $request->input('user_id'))
            ->where('news_id', '=', $request->input('news_id'));
            
            $news_like->delete();
        }
        else 
        {
            $news_like = new NewsLike;
            $news_like->news_id = $request->input('news_id');
            $news_like->user_id = $request->input('user_id');
            $news_like->save();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show($news_id)
    {
        $news = DB::select("SELECT
                        *,
                        (CASE WHEN :user_id IN (SELECT b.user_id
                        FROM news_likes b
                        WHERE a.id = b.news_id)
                            THEN 0
                        ELSE 1 END)             AS liked,
                        (SELECT count(b.id)
                        FROM news_likes b
                        WHERE a.id = b.news_id) AS likes
                        FROM news a
                        WHERE a.id = :news_id;",
                 ['news_id' => $news_id, 'user_id' => Auth::user()->id]);

        $comments = DB::select("SELECT a.*, COUNT(b.id) AS comment_likes
            FROM news_comments a
            LEFT JOIN news_comment_likes b ON a.id = b.news_comments_id
            WHERE a.news_id = :news_id
            GROUP BY a.id;", ['news_id' => $news_id]);

        //dd($news);
            
        return view('news/show',compact('news', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        //
    }
}
