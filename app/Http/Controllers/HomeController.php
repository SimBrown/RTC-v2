<?php

namespace App\Http\Controllers;

use App\News;
use App\Post;
use App\Survey;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Hash;
use DB;
use App\User;
use App\User_posts;
use Illuminate\Support\Facades\Storage;
use Exception;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $news = DB::table('community_news')
        ->orderby('created_at','desc')
        ->get();
        
        $forum_post = Post::orderBy('id','desc')->take(5)->get();

        $surveys =  DB::select("SELECT a.*
            FROM surveys a
            LEFT JOIN survey_answers a2 on a.id = a2.survey_id and a2.user_id = :user_id
            JOIN survey_options b ON a.id = b.survey_id
            WHERE a.result IS NULL AND a2.id IS NULL
            LIMIT 1;", ['user_id' =>Auth::user()->id]);

        if ($surveys) {
            $survey_options = DB::table('survey_options')
                ->where('survey_options.survey_id', '=', $surveys[0]->id)
                ->selectRaw('survey_options.*, COUNT(survey_options.id) as options')
                ->groupBy('survey_options.id')
                ->get();
        }

        $uid = Auth::User()->id;

        /** Prende i post esclusivamente dagli utenti amici */
        $query = "SELECT DISTINCT user_posts.id as pid,users.image as user_image, user_posts.body, users.name as author_name,users.surname as author_surname, user_posts.image, user_posts.created_At as created,
                
                /* Likes counter */
                (SELECT COUNT(*) FROM (user_posts JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
                WHERE user_post_likes.post_id = pid) AS likes,
                
                /* Selecting 1 if user has liked 0 elsewhere */
                (CASE WHEN EXISTS (SELECT *
                        FROM user_post_likes
                        WHERE user_post_likes.user_id = $uid AND user_post_likes.post_id = pid) 
                THEN '1' ELSE '0' END) AS has_liked,

                /* Comments counter */
                (SELECT COUNT(*) FROM (user_posts JOIN user_post_comments ON user_posts.id = user_post_comments.post_id)
                WHERE user_post_comments.post_id = pid) AS comment_numb
                
                FROM ((user_posts LEFT JOIN users ON user_posts.author = users.id) LEFT JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
                WHERE user_posts.author IN (SELECT friend_id FROM friends_relations  WHERE user_id = $uid) OR user_posts.author = $uid
                GROUP BY user_posts.id
                ORDER BY user_posts.created_at DESC
                LIMIT 0,8"; 

        $feed = DB::select(DB::raw($query));

        $feed_likes = DB::select("SELECT user_posts.id as lpid, users.name,users.image
        FROM  user_post_likes 
        JOIN  users ON user_post_likes.user_id = users.id
        JOIN  user_posts ON user_post_likes.post_id = user_posts.id");  
        
        $feed_comments = DB::select("SELECT users.name as name, users.surname as surname,user_post_comments.body,user_post_comments.created_at,users.image as author_image, user_post_comments.post_id as id
        FROM  user_post_comments 
        JOIN  users ON user_post_comments.user_id = users.id
        JOIN  user_posts ON user_post_comments.post_id = user_posts.id
        WHERE user_posts.author IN (SELECT friend_id FROM friends_relations  WHERE user_id = $uid) OR user_posts.author = $uid");

        /* $user_post = DB::select("SELECT * 
            FROM user_posts 
            WHERE author IN 
            ($userID,(SELECT friend_id FROM friends_relations  WHERE user_id = $userID))");
        
        /** Crea un feed unico unendo Annunci e Post **/
        /**$feed = $news->merge($user_post)->sortByDesc('created_at'); */

        return view('home', compact('feed','feed_likes','feed_comments','forum_post','surveys','live_events', 'survey_options'));
    }

    /**
     * Show user settings page.
     *
     * @return \Illuminate\Http\Response
    */
    public function notifications()
    {
        $uid = Auth::User()->id;
        $el_page = 8;

        /* Default page */
        $page = 1;

        if(isset($_GET['page'])){
            $page = (intVal($_GET['page'])-1)* $el_page;
        }

        $all_notification = DB::select("SELECT users.name as user_from_name, users.surname as user_from_surname,notifications.user_from as user_from_id,users.image,notifications.post_id, notifications.type,
        (SELECT COUNT(id)FROM notifications WHERE user_id = $uid) AS notifications_amount
        FROM (notifications JOIN users ON notifications.user_from = users.id)
        WHERE user_id = $uid
        ORDER BY notifications.id DESC
        LIMIT $page,8");

        return view('profile.settings.notifications',compact('all_notification'));
    }

        /**
     * Show user settings page.
     *
     * @return \Illuminate\Http\Response
    */
    public function personal_informations(){

        return view('profile.settings.personal-information',compact('all_notification'));
    }
    
    /**
     * Updated user settings.
     *
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request)
    {
        if (null !== $request->file('image')) {
            $path = Storage::put('public/images', $request->file('image'));
            // cause of a problem storing images we have to cut off the prefix "public/" from the path
            $path = substr($path, 7);
        }
        $user = User::updateOrCreate(
            ['id' => $request->input('user_id')],
            ['name' => $request->input('name'),
            'email' => $request->input('email'),
            'address' => $request->input('address')]
        );
        if (isset($path)) {
            User::updateOrCreate(
                ['id' => $user->id],
                ['image' => $path]);
        }
        return view('profile.yourAccount');
    }

    /**
     * Post status
     */
    public function insertPost(Request $request)
    {
        if (null !== $request->file('image')) {

            $path = Storage::put('public/images', $request->file('image'));
            $path = substr($path, 7);

            $body = $request->input('post_body');


            DB::table('user_posts')
            ->insert(['author' => Auth::User()->id, 'body' => $body, 'image' => $path]);

        }
    }

     /**
     * Like post.
     *
     * @return \Illuminate\Http\Response
    */
    public function likePost(Request $request)
    {
        DB::table('user_post_likes')->insert(
            ['post_id' => $request->input('post-id') , 'user_id' => Auth::User()->id]
        );
        
        $pid = $request->input('post-id');
        $uid = Auth::User()->id;

        /* Sending notification */
        controller::send_post_notification('like',$pid);

        /** Prende i post esclusivamente dagli utenti amici */
        $query = "SELECT DISTINCT user_posts.id as pid,users.image as user_image, user_posts.body, users.name as author, user_posts.image, user_posts.created_At as created,
        (SELECT COUNT(*) FROM (user_posts JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
        WHERE user_post_likes.post_id = pid) AS likes,
        (CASE WHEN EXISTS (SELECT *
                FROM user_post_likes
                WHERE user_post_likes.user_id = $uid AND user_post_likes.post_id = pid) 
        THEN '1' ELSE '0' END) AS has_liked,

        /* Comments counter */
        (SELECT COUNT(*) FROM (user_posts JOIN user_post_comments ON user_posts.id = user_post_comments.post_id)
        WHERE user_post_comments.post_id = pid) AS comment_numb

        FROM ((user_posts LEFT JOIN users ON user_posts.author = users.id) LEFT JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
        WHERE user_posts.id = $pid
        GROUP BY user_posts.id
        ORDER BY user_posts.created_at DESC"; 

        $n = DB::select(DB::raw($query));

        $feed_likes = DB::select("SELECT user_posts.id as lpid, users.name,users.image
        FROM  user_post_likes 
        JOIN  users ON user_post_likes.user_id = users.id
        JOIN  user_posts ON user_post_likes.post_id = user_posts.id
        WHERE user_posts.id = $pid ");

        $feed_comments = DB::select("SELECT user_posts.id as pid,users.name as name, users.surname as surname, user_post_comments.body,user_post_comments.created_at,users.image as author_image, user_post_comments.post_id as id
        FROM  user_post_comments 
        JOIN  users ON user_post_comments.user_id = users.id
        JOIN  user_posts ON user_post_comments.post_id = user_posts.id
        WHERE user_posts.id = $pid");



        return view('post.new-bottom-bar', compact('n','feed_likes','feed_comments'));
    }

     /**
     * Dislike post.
     *
     * @return \Illuminate\Http\Response
    */
    public function dislikePost(Request $request)
    {
        DB::table('user_post_likes')->where('post_id', '=', $request->input('post-id'))->where('user_id','=', Auth::User()->id)->delete();

        $uid = Auth::User()->id;
        $pid = $request->input('post-id');

        /** Prende i post esclusivamente dagli utenti amici */
        $query = "SELECT DISTINCT user_posts.id as pid,users.image as user_image, user_posts.body, users.name as author, user_posts.image, user_posts.created_At as created,
        (SELECT COUNT(*) FROM (user_posts JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
        WHERE user_post_likes.post_id = pid) AS likes,
        (CASE WHEN EXISTS (SELECT *
                FROM user_post_likes
                WHERE user_post_likes.user_id = $uid AND user_post_likes.post_id = pid) 
        THEN '1' ELSE '0' END) AS has_liked,
    
        /* Comments counter */
         (SELECT COUNT(*) FROM (user_posts JOIN user_post_comments ON user_posts.id = user_post_comments.post_id)
        WHERE user_post_comments.post_id = pid) AS comment_numb

        FROM ((user_posts LEFT JOIN users ON user_posts.author = users.id) LEFT JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
        WHERE user_posts.id = $pid
        GROUP BY user_posts.id
        ORDER BY user_posts.created_at DESC"; 

        $n = DB::select(DB::raw($query));

        $feed_likes = DB::select("SELECT user_posts.id as lpid, users.name,users.image
        FROM  user_post_likes 
        JOIN  users ON user_post_likes.user_id = users.id
        JOIN  user_posts ON user_post_likes.post_id = user_posts.id
        WHERE user_posts.id = $pid ");

        return view('post.new-bottom-bar', compact('n','feed_likes'));
    }

       /**
     * Dislike post.
     *
     * @return \Illuminate\Http\Response
    */
    public function commentPost(Request $request)
    {
        $uid = Auth::User()->id;
        
        controller::send_post_notification('comment',$request->input('post-id'));

        DB::table('user_post_comments')->insert(
                    ['post_id' => $request->input('post-id') , 'user_id' => Auth::User()->id , 'body' => $request->input('comment-body')]
                    );
        
        $comment= collect();//new collection
        $comment->body = $request->input('comment-body');

        return view('post.comment', compact('comment'));
    }


    /**
     * Returns path of the preloaded image
     */
    public function imagePreload(Request $request)
    {
        if (null !== $request->file('image')) {
            
            $path = Storage::put('public/images/status-uploaded', $request->file('image'));
            $path = substr($path, 7);
            
            return $path;
        }
    }

    
    /**
     * Returns path of the preloaded image
     */
    public function showFriends($uid)
    {
        $user_profile = DB::table('users')
        ->where('users.id','=',$uid)
        ->get();

        return view('profile.friends',compact('user_profile'));
    }

        
    /**
     * Returns path of the preloaded image
     */
    public function findFriends()
    {

        $find_friends = DB::select("SELECT * FROM users ORDER BY RAND() LIMIT 16");

        return view('profile.find-friends',compact('find_friends'));
    }


        /**
     * Returns path of the preloaded image
     */
    public function changeUserPhoto(Request $request)
    {
        $uid = Auth::User()->id;

        if (null !== $request->file('image')) {
            
            $path = Storage::put('public/images/avatars', $request->file('image'));
            $path = substr($path, 7);

            DB::table('users')
            ->where('id', $uid)
            ->update(['image' => $path]);

            return $path;
        }
    }

    /**
     *  Update user informations
     */
    public function updateInfo(Request $request)
    {
        $uid = Auth::User()->id;
        
        $user  = User::find($uid);
        
        $user->name = $request->input('first_name');
        $user->surname =$request->input('last_name');
        $user->email = $request->input('email');
        $user->birthdate = date('Y-m-d', strtotime(str_replace("/","-",$request->input('datetimepicker'))));
        $user->address = $request->input('address');

        $user->save();

        return redirect(route('profile.informations'));
    }
    
    /**
     * Show user password change page
     */
    public function changePassword(Request $request)
    {
        return view('profile.settings.change-password');
    }

    
    /**
     * Changes user password
     */
    public function updatePassword(Request $request)
    {
        $uid = Auth::User()->id;
        
        $user  = User::find($uid);
        
        if(Hash::check($request->input('current_pwd'), Auth::User()->password)){
            $user->password = Hash::make($request->input('new_pwd'));
            $user->save();
            return redirect(route('profile.change-password'));
        }
        else{
            $error = "Wrong Password!";
            return view('profile.settings.change-password',compact('error'));
        }

    }

    /**
     * Posts a new status
     */
    public function postStatus(Request $request)
    {
        $uid = Auth::User()->id;

        $post = new User_posts;

        $post->author = $uid;
        $post->body = $request->post_body;
        $post->image = $request->image;

        $post->save();


        $feed_likes = DB::select("SELECT user_posts.id as lpid, users.name,users.image
                                FROM  user_post_likes 
                                JOIN  users ON user_post_likes.user_id = users.id
                                JOIN  user_posts ON user_post_likes.post_id = user_posts.id
                                WHERE (user_posts.id = $uid)");     
        
        return view('post.new-post',compact('post','feed_likes'));
    }
        
    /**
     * Returns profile of an user
     */
    public function showProfile($uid)
    {
        
        $query = "SELECT DISTINCT users.address as userAddress,user_posts.id as pid,users.image as user_image, user_posts.body, users.name as author_name,users.surname as author_surname, user_posts.image, user_posts.created_At as created,
       
        /* Likes counter */
        (SELECT COUNT(*) FROM (user_posts JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
        WHERE user_post_likes.post_id = pid) AS likes,

        /* Selecting 1 if user has liked 0 elsewhere */
        (CASE WHEN EXISTS (SELECT *
                        FROM user_post_likes
                        WHERE user_post_likes.user_id = $uid AND user_post_likes.post_id = pid) 
                THEN '1' ELSE '0' END) AS has_liked,
       
        /* Comments counter */
        (SELECT COUNT(*) FROM (user_posts JOIN user_post_comments ON user_posts.id = user_post_comments.post_id)
        WHERE user_post_comments.post_id = pid) AS comment_numb

        FROM ((user_posts LEFT JOIN users ON user_posts.author = users.id) LEFT JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
        WHERE user_posts.author = $uid
        GROUP BY user_posts.id
        ORDER BY user_posts.created_at DESC"; 

        $feed = DB::select(DB::raw($query));

        
        $feed_likes = DB::select("SELECT user_posts.id as lpid, users.name,users.image
                                FROM  user_post_likes 
                                JOIN  users ON user_post_likes.user_id = users.id
                                JOIN  user_posts ON user_post_likes.post_id = user_posts.id
                                WHERE (user_posts.id = $uid)");     


        $feed_comments = DB::select("SELECT user_posts.id as pid,users.name as name, users.surname as surname, user_post_comments.body,user_post_comments.created_at,users.image as author_image, user_post_comments.post_id as id
        FROM  user_post_comments 
        JOIN  users ON user_post_comments.user_id = users.id
        JOIN  user_posts ON user_post_comments.post_id = user_posts.id
        WHERE user_posts.author = $uid");   

        $user_profile = DB::table('users')
        ->where('users.id','=',$uid)
        ->get();
        
        return view('profile.user-profile',compact('feed','feed_likes','feed_comments','user_profile'));

    }

    /** Make Friendship Request */

    static function requestFriendship($friend) {
        
        $uid = Auth::User()->id;

        /* Target user must accept friendship */
        DB::table('friends_relations')->insert(
            ['user_id' => $uid, 'friend_id' => $friend]
        );

        controller::send_friends_notification($friend);

    }

    /* Show friend requests in profile */
    static function friend_requests() {

       /* requests are passed by appserviceprovider as they're needed in header */
       return view('profile.settings.friend-requests',compact('friends_requests'));
    }

    /** Make Friendship */

    static function acceptFriendship($friend) {
    
        /* Target user must accept friendship */
        DB::table('friends_relations')->insert(
            ['user_id' => Auth::User()->id, 'friend_id' => $friend]
        );
    }

    /** Check Friendship */
    static function checkFriendship($uid2){
        $uid = Auth::User()->id;
        $friend = DB::select("SELECT * FROM friends_relations WHERE (user_id = $uid AND friend_id = $uid2) OR (user_id = $uid2 AND friend_id = $uid) ");
        
        if(count($friend)==1){
            if($friend[0]->user_id == $uid) return 'Request Sent';
            else if($friend[0]->user_id == $uid2) return 'Confirm Request';
        }
        else if(count($friend)==2) return 'You are Friends';
        else return 'Add to friends';
    }

    /* Shows profile messenger */
    static function profileMessenger() {

        $uid = Auth::User()->id;

        $contact = DB::select("SELECT cm.*,us.id as user_id,us.name,us.surname,us.image
        FROM chat_messages AS cm
        LEFT JOIN users AS us ON us.id = cm.from_id
        WHERE
           cm.id = (
              SELECT MAX(id)
              FROM chat_messages
              WHERE from_id = cm.from_id AND from_id != $uid AND to_id = $uid
           )
        ORDER BY cm.time DESC
        ");
    
        return view('profile.messages',compact('contact'));
    }

    /* Load more post */
    static function loadMore($p) {

        $el_page = 8;
        $page = ($p-1)*$el_page;

        $uid = Auth::User()->id;

        /** Prende i post esclusivamente dagli utenti amici */
        $query = "SELECT DISTINCT user_posts.id as pid,users.image as user_image, user_posts.body, users.name as author_name,users.surname as author_surname, user_posts.image, user_posts.created_At as created,
        
        /* Likes counter */
        (SELECT COUNT(*) FROM (user_posts JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
        WHERE user_post_likes.post_id = pid) AS likes,
        
        /* Selecting 1 if user has liked 0 elsewhere */
        (CASE WHEN EXISTS (SELECT *
                FROM user_post_likes
                WHERE user_post_likes.user_id = $uid AND user_post_likes.post_id = pid) 
        THEN '1' ELSE '0' END) AS has_liked,

        /* Comments counter */
        (SELECT COUNT(*) FROM (user_posts JOIN user_post_comments ON user_posts.id = user_post_comments.post_id)
        WHERE user_post_comments.post_id = pid) AS comment_numb
        
        FROM ((user_posts LEFT JOIN users ON user_posts.author = users.id) LEFT JOIN user_post_likes ON user_posts.id = user_post_likes.post_id)
        WHERE user_posts.author IN (SELECT friend_id FROM friends_relations  WHERE user_id = $uid) OR user_posts.author = $uid
        GROUP BY user_posts.id
        ORDER BY user_posts.created_at DESC
        LIMIT $page,8"; 

        $feed = DB::select(DB::raw($query));

        $feed_likes = DB::select("SELECT user_posts.id as lpid, users.name,users.image
        FROM  user_post_likes 
        JOIN  users ON user_post_likes.user_id = users.id
        JOIN  user_posts ON user_post_likes.post_id = user_posts.id");  
        
        $feed_comments = DB::select("SELECT users.name as name, users.surname as surname,user_post_comments.body,user_post_comments.created_at,users.image as author_image, user_post_comments.post_id as id
        FROM  user_post_comments 
        JOIN  users ON user_post_comments.user_id = users.id
        JOIN  user_posts ON user_post_comments.post_id = user_posts.id
        WHERE user_posts.author IN (SELECT friend_id FROM friends_relations  WHERE user_id = $uid) OR user_posts.author = $uid");
    
        return view('post.feed',compact('feed','feed_likes','feed_comments'));
    }

    /* Shows profile messenger */
    static function getConversation($friend) {

        $uid = Auth::User()->id;

        $conversation = DB::select("SELECT users.id as user_id, users.name, users.surname, users.image, chat_messages.body, chat_messages.time FROM (`chat_messages` JOIN users ON users.id = chat_messages.from_id) WHERE (chat_messages.to_id = $uid AND chat_messages.from_id = $friend) OR (chat_messages.to_id = $friend AND chat_messages.from_id = $uid)  ORDER BY chat_messages.time ASC");
        
        return view('profile.message-block-ajax',compact('conversation'));
    }


    /* Searches for people */
    static function search() {

        $keyword = $_GET['keyword'];

        $results = DB::select("SELECT CONCAT(users.name,' ',users.surname) AS full_name,users.name,users.surname,users.id,users.image,users.address FROM users WHERE CONCAT(users.name,' ',users.surname) LIKE '%$keyword%'");

        return view('search-results',compact("results"));
    }
    

    
}

