<?php

namespace App\Http\Controllers;

use App\Section;
use App\Post;
use App\Category;
use App\Survey;
use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;


use Illuminate\Http\Request;

class ForumController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $survey = Survey::orderBy('id','desc')->limit(3)->get();

        $categories = Category::All();
        $sections = Section::All();

        $featured_topics = Post::orderBy('id','desc')->limit(3)->get();

        $recent_topics = DB::select("SELECT a.*, b.name as name FROM forum_posts a 
                            JOIN users b on a.user_id = b.id
                            ORDER BY a.created_at ASC");
       
        return view('forum.index', compact('categories','sections','survey', 'featured_topics', 'recent_topics'));
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //TODO
        $categories = Category::All();
        return view('forum/create',compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createPost(Request $request)
    {
        $post = new Post;
        $post->body = $request->input('body');
        $post->title = $request->input('title');
        $post->category_id = $request->input('category_id');
        $post->user_id = Auth::user()->id;       
        
        
        $post->save();

        return Redirect::to('forum');
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');
    
        $post = Post::where('title', 'like', "%{$keyword}%")
        ->orWhere('body', 'like', "%{$keyword}%")
        ->get();
        
        return view('forum.showResults', compact('post','keyword'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showCreate()
    {
        $categories = Category::All();
        return view('forum/createPost',compact('categories'));
    }

}
