<?php

namespace App\Http\Controllers;

use App\News;
use App\Post;
use Auth;
use App\Category;
use App\Survey;
use DB;
use Illuminate\Http\Request;
use App\Http\Middleware\UserGroupCheck;


class ForumCategoryController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($cat)
    {
        $post = DB::table('categories')
        ->join('forum_posts','categories.id','=','forum_posts.category_id')
        ->join('users','users.id','=','forum_posts.user_id')
        ->join('section','section.id','=','categories.section')
        /* ->join('posts_replies','posts_replies.post_id','=','forum_posts.id') */
        ->select(   
            'forum_posts.id',
            'forum_posts.title',
            'users.name',
            'forum_posts.created_at as post_timestamp',
            /* 'posts_replies.created_at as post_reply_timestamp', */
            'section.description as section_name',
            'categories.description as category_name',
            'categories.id as category_id'
            )
        ->where('forum_posts.category_id','=',$cat)
        ->groupBy('forum_posts.id')
        ->orderBy('forum_posts.created_at','asc')
        ->get();
     

        $query =  DB::table('categories')
        ->join('section','categories.section','=','section.id')
        ->select('section.description as sec_name','categories.description as cat_name')
        ->where('categories.id','=', $cat)
        ->get();

        $path['section'] = $query[0]->sec_name;
        $path['category'] = $query[0]->cat_name;
        $path['category_id'] = $cat;
     
        return view('forum.showCategory',compact('post','path'));
    }

         /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
         //checking permissions
         if(!UserGroupCheck::check(['ManageForum'])){
            return view("errors.no-permission");
        }
        $categories = DB::table('categories')
        ->join('section','categories.section','=','section.id')
        ->select('categories.id','categories.description','categories.section','section.description as section_name')
        ->where('categories.archived','!=','1')
        ->where('section.archived','!=','1')
        ->get();

        $sections = DB::table('section')
        ->select('section.id','section.description')
        ->where('section.archived','!=','1')
        ->get();

        return view('admin.forum.category-index',compact('categories','sections'));
    }

          /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archived()
    {
        //checking permissions
        if(!UserGroupCheck::check(['ManageForum'])){
            return view("errors.no-permission");
        }


        $categories = DB::table('categories')
        ->join('section','categories.section','=','section.id')
        ->select('categories.id','categories.description','categories.section','section.description as section_name')
        ->where('categories.archived','=','1')
        ->get();
        
        $sections = DB::table('section')
        ->select('section.id','section.description')
        ->get();

        return view('admin.forum.category-archived',compact('categories','sections'));
    }


    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function insertOrUpdate(Request $request)
    {
        //checking permissions
         if(!UserGroupCheck::check(['ManageForum'])){
            return view("errors.no-permission");
        }
        
        if($request->input('archived')!=""){

            $category = Category::updateOrCreate(
                ['id' => $request->input('cat_id')],
                ['archived'=> $request->input('archived')]
            );
        }
        else{
            $category = Category::updateOrCreate(
                ['id' => $request->input('form_cat_id')],
                ['description' => $request->input('form_description'),
                'section' => $request->input('sec_select')]
            );
        }
        
        $sections = DB::table('section')
        ->select('section.id','section.description')
        ->get();

        $categories = DB::table('categories')
        ->join('section','categories.section','=','section.id')
        ->select('categories.id','categories.description','categories.section','section.description as section_name')
        ->where('categories.archived','!=','1')
        ->where('section.archived','!=','1')
        ->get();

        
        return view('admin.forum.category-index',compact('categories','sections'));
    }
}
