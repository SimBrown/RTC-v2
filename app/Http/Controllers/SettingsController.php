<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use DB;

class SettingsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show()
    {    
        return view('admin/settings/content-settings');
    }



    /**
     * Edit values of the specified resource 
     * 
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $input = $request->except('_token');
        
        foreach($input as $key=>$value){

            Settings::where('name', $key)
            ->update(['value' => $value]);

        }

        //reload settings
        parent::__construct();

        return view('admin.settings/content-settings',compact('settings'));
    }
}
