<?php

namespace App\Http\Middleware;

use User;
use Auth;
use DB;

class UserGroupCheck
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function check($services)
    {
        $userGroups = DB::table('role_groups_user')
        //retrieve user groups
        ->join('role_groups','role_groups_user.group_id','=','role_groups.id')
        ->where('role_groups_user.user_id','=',Auth::User()->id)
        //retrieve groups services
        ->join('role_groups_services','role_groups.id','=','role_groups_services.groupId')
        ->join('services','role_groups_services.serviceID','=','services.id')
        ->select('services.id','services.name')
        ->groupBy('services.name')
        ->get();
        
        foreach($services as $s){

            if(!$userGroups->contains('name',$s)){
                return false;
            }
        }

        return true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('admin.users.users-archived',compact('1'));
    }
}
