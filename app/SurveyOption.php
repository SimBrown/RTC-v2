<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyOption extends Model
{
    protected $table = 'survey_options';

    protected $fillable = ['survey_id', 'description'];    
}
