-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 12, 2018 at 12:28 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rtc`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `section` int(11) UNSIGNED DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `description`, `created_at`, `updated_at`, `section`, `archived`) VALUES
(1, 'Guide', '2018-06-08 16:56:52', '2018-08-28 09:15:22', 4, 0),
(2, 'Presentazioni', '2018-06-17 12:53:31', '2018-06-17 12:53:31', 1, 0),
(3, 'Informazioni Generali', '2018-06-17 12:54:04', '2018-06-17 12:54:04', 2, 0),
(4, 'Segnalazione Bug', '2018-06-17 12:54:13', '2018-06-17 12:54:13', 3, 0),
(5, 'Il Bar Di RTC', '2018-06-17 12:54:41', '2018-06-17 12:54:41', 5, 0),
(6, 'Collaborazioni', '2018-06-17 12:54:59', '2018-06-17 12:54:59', 6, 0),
(7, 'Sponsor', '2018-06-17 12:55:06', '2018-06-17 12:55:06', 6, 0),
(8, 'Consigli', '2018-06-17 12:55:56', '2018-06-17 12:56:04', 4, 0),
(9, 'Tutorial', '2018-06-17 12:56:15', '2018-06-17 12:56:15', 1, 0),
(10, 'asd', '2018-08-28 12:37:13', '2018-09-12 08:13:33', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

CREATE TABLE `chat_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_id` int(11) UNSIGNED NOT NULL,
  `to_id` int(11) UNSIGNED NOT NULL,
  `body` varchar(200) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_messages`
--

INSERT INTO `chat_messages` (`id`, `from_id`, `to_id`, `body`, `time`) VALUES
(682, 1, 203, 'Prova', '2018-09-12 12:27:35');

-- --------------------------------------------------------

--
-- Table structure for table `community_news`
--

CREATE TABLE `community_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `community_news`
--

INSERT INTO `community_news` (`id`, `created_at`, `updated_at`, `title`, `content`, `image`) VALUES
(1, '2018-07-21 22:00:00', NULL, 'NEWS 1', 'TESTO NEWS 1', ''),
(2, '2018-07-26 22:00:00', NULL, 'NEWS 2', 'TESTO NEWS 2', '');

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts`
--

CREATE TABLE `forum_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reputation` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forum_posts`
--

INSERT INTO `forum_posts` (`id`, `body`, `reputation`, `user_id`, `category_id`, `created_at`, `updated_at`, `title`, `archived`, `image`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, 1, 5, '2018-05-25 18:25:00', '2018-08-28 09:34:30', 'Post di prova', 0, ''),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, 1, 1, '2018-03-30 17:25:00', NULL, 'Post di prova', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts_replies`
--

CREATE TABLE `forum_posts_replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forum_posts_replies`
--

INSERT INTO `forum_posts_replies` (`id`, `post_id`, `created_at`, `updated_at`, `body`, `user_id`) VALUES
(18, 1, '2018-06-25 20:45:39', '2018-06-25 20:45:39', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur \r\nadipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco\r\n laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor \r\nin reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla \r\npariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa \r\nqui officia deserunt mollit anim id est laborum.</p><p><br></p>', 1),
(19, 1, '2018-06-25 20:45:50', '2018-06-25 20:45:50', '<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur \r\nadipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco\r\n laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor \r\nin reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla \r\npariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa \r\nqui officia deserunt mollit anim id est laborum.</p></blockquote><p></p>', 1),
(20, 1, '2018-06-25 20:46:21', '2018-06-25 20:46:21', '<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur \r\nadipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco\r\n laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor \r\nin reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla \r\npariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa \r\nqui officia deserunt mollit anim id est laborum.</p><p></p></blockquote><p>Ut enim ad minim veniam, quis nostrud exercitation ullamco\r\n laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor \r\nin reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla \r\npariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa \r\nqui officia deserunt mollit anim id est laborum.</p><p></p>', 1),
(21, 2, '2018-07-02 14:10:48', '2018-07-02 14:10:48', '<p>risposta</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `friends_relations`
--

CREATE TABLE `friends_relations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `friend_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friends_relations`
--

INSERT INTO `friends_relations` (`id`, `user_id`, `friend_id`) VALUES
(19, 1, 203),
(40, 1, 208),
(67, 1, 213),
(61, 1, 217),
(52, 1, 222),
(49, 1, 275),
(63, 1, 282),
(36, 1, 288),
(62, 1, 299),
(50, 1, 302),
(39, 1, 308),
(38, 1, 321),
(65, 1, 323),
(37, 1, 330),
(64, 1, 343),
(41, 1, 350),
(51, 1, 355),
(20, 203, 1),
(66, 213, 1),
(60, 217, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups_members`
--

CREATE TABLE `groups_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups_post`
--

CREATE TABLE `groups_post` (
  `id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `image` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2018_04_27_084117_create_news_table', 1),
(10, '2018_05_12_075525_create_roles_table', 2),
(11, '2018_05_12_075604_create_role_user_table', 2),
(12, '2018_05_12_084126_create_categories_table', 2),
(13, '2018_05_12_084256_create_posts_table', 2),
(14, '2018_05_12_085246_create_comments_table', 2),
(15, '2018_05_12_085355_create_surveys_table', 2),
(16, '2018_05_12_090109_create_survey_options_table', 2),
(18, '2018_05_12_090515_create_news_likes_table', 2),
(19, '2018_05_12_090555_create_news_comments_table', 2),
(20, '2018_05_12_090742_create_news_comment_likes_table', 2),
(21, '2018_05_12_090822_create_prizes_table', 2),
(22, '2018_05_12_091021_create_prize_users_table', 2),
(23, '2018_05_12_091112_create_live_events_table', 2),
(24, '2018_05_12_091454_create_private_messages_table', 2),
(25, '2018_05_27_114746_alter_forum_category_table', 3),
(26, '2018_05_27_115052_create_forum_sections_table', 4),
(27, '2018_05_30_153431_alter_post_table', 5),
(28, '2018_05_30_160037_create_posts_replies_table', 6),
(29, '2018_05_31_161011_alter_posts_replies_table', 7),
(31, '2018_06_08_160712_alter_categories_table', 8),
(32, '2018_06_08_175110_alter_section_table', 9),
(35, '2018_05_12_090237_create_survey_answers_table', 10),
(37, '2018_06_08_192932_alter_post_archive', 11),
(38, '2018_06_12_110624_alter_user_table', 11),
(39, '2018_06_12_142014_alter_user_table_archived', 12),
(43, '2018_06_15_162206_create_settings_table', 13),
(44, '2018_07_23_161208_user_post_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `news_comments`
--

CREATE TABLE `news_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_comments`
--

INSERT INTO `news_comments` (`id`, `news_id`, `user_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'commento', '2018-07-23 13:40:47', '2018-07-23 13:40:47'),
(2, 1, 1, 'ricommento', '2018-07-23 13:41:01', '2018-07-23 13:41:01');

-- --------------------------------------------------------

--
-- Table structure for table `news_comment_likes`
--

CREATE TABLE `news_comment_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_comments_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_likes`
--

CREATE TABLE `news_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_likes`
--

INSERT INTO `news_likes` (`id`, `news_id`, `user_id`, `created_at`, `updated_at`) VALUES
(49, 1, 1, '2018-07-23 08:29:27', '2018-07-23 08:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_from` int(10) UNSIGNED DEFAULT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `entity_from_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `hasRead` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `user_from`, `post_id`, `entity_from_id`, `type`, `hasRead`) VALUES
(611, 1, 203, NULL, NULL, 'message', 1),
(612, 1, 203, NULL, NULL, 'message', 1),
(615, 203, 1, NULL, NULL, 'message', 1),
(616, 203, 1, NULL, NULL, 'message', 1),
(617, 1, 203, NULL, NULL, 'message', 1),
(618, 1, 203, NULL, NULL, 'message', 1),
(619, 1, 203, NULL, NULL, 'message', 1),
(620, 203, 1, NULL, NULL, 'message', 1),
(621, 1, 203, NULL, NULL, 'message', 1),
(622, 1, 203, NULL, NULL, 'message', 1),
(623, 203, 1, NULL, NULL, 'message', 1),
(624, 1, 203, NULL, NULL, 'message', 1),
(625, 1, 203, NULL, NULL, 'message', 1),
(626, 203, 1, NULL, NULL, 'message', 1),
(627, 1, 203, NULL, NULL, 'message', 1),
(628, 1, 203, NULL, NULL, 'message', 1),
(629, 1, 203, NULL, NULL, 'message', 1),
(630, 1, 203, NULL, NULL, 'message', 1),
(638, 203, 1, NULL, NULL, 'message', 1),
(644, 203, 1, NULL, NULL, 'message', 1),
(645, 1, 203, NULL, NULL, 'message', 1),
(648, 203, 1, NULL, NULL, 'message', 1),
(649, 203, 1, NULL, NULL, 'message', 1),
(650, 203, 1, NULL, NULL, 'message', 1),
(651, 203, 1, NULL, NULL, 'message', 1),
(652, 203, 1, NULL, NULL, 'message', 1),
(653, 203, 1, NULL, NULL, 'message', 1),
(654, 203, 1, NULL, NULL, 'message', 1),
(655, 203, 1, NULL, NULL, 'message', 1),
(656, 203, 1, NULL, NULL, 'message', 1),
(657, 203, 1, NULL, NULL, 'message', 1),
(720, 203, 1, NULL, NULL, 'message', 0),
(722, 1, 217, NULL, NULL, 'friends', 1),
(723, 1, 217, NULL, NULL, 'friends', 1),
(724, 217, 1, NULL, NULL, 'friends', 0),
(725, 1, 217, NULL, NULL, 'message', 1),
(728, 299, 1, NULL, NULL, 'friends', 0),
(729, 217, 1, NULL, NULL, 'message', 0),
(730, 203, 1, NULL, NULL, 'message', 0),
(731, 217, 1, NULL, NULL, 'message', 0),
(732, 217, 1, NULL, NULL, 'message', 0),
(733, 203, 1, NULL, NULL, 'message', 0),
(734, 217, 1, NULL, NULL, 'message', 0),
(735, 217, 1, NULL, NULL, 'message', 0),
(736, 203, 1, NULL, NULL, 'message', 0),
(737, 1, 203, NULL, NULL, 'message', 1),
(738, 1, 203, NULL, NULL, 'message', 1),
(739, 203, 1, NULL, NULL, 'message', 0),
(740, 203, 1, NULL, NULL, 'message', 0),
(741, 203, 1, NULL, NULL, 'message', 0),
(742, 1, 203, NULL, NULL, 'message', 1),
(743, 203, 1, NULL, NULL, 'message', 0),
(744, 1, 203, NULL, NULL, 'message', 1),
(745, 1, 203, NULL, NULL, 'message', 1),
(746, 1, 203, NULL, NULL, 'message', 1),
(747, 1, 203, NULL, NULL, 'message', 1),
(748, 203, 1, NULL, NULL, 'message', 0),
(749, 203, 1, NULL, NULL, 'message', 0),
(750, 1, 203, NULL, NULL, 'message', 1),
(751, 1, 203, NULL, NULL, 'message', 1),
(752, 203, 1, NULL, NULL, 'message', 0),
(753, 203, 1, NULL, NULL, 'message', 0),
(754, 203, 1, NULL, NULL, 'message', 0),
(755, 203, 1, NULL, NULL, 'message', 0),
(756, 1, 203, NULL, NULL, 'message', 1),
(757, 282, 1, NULL, NULL, 'friends', 0),
(758, 343, 1, NULL, NULL, 'friends', 0),
(761, 323, 1, NULL, NULL, 'friends', 0),
(763, 1, 213, NULL, NULL, 'friends', 1),
(764, 213, 1, NULL, NULL, 'message', 1),
(765, 1, 213, NULL, NULL, 'message', 1),
(767, 213, 1, 99, NULL, 'like', 1),
(768, 1, 213, NULL, NULL, 'message', 1),
(769, 213, 1, NULL, NULL, 'message', 0),
(770, 213, 1, NULL, NULL, 'message', 0),
(771, 213, 1, NULL, NULL, 'message', 0),
(772, 203, 1, NULL, NULL, 'message', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('simonbrown994@gmail.com', '$2y$10$TC58MUYRpYxpPj5u7yyNRupylbIurwmsXCG4It93CTyZe.jNtRs1e', '2018-06-12 20:23:31');

-- --------------------------------------------------------

--
-- Table structure for table `role_groups`
--

CREATE TABLE `role_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_groups`
--

INSERT INTO `role_groups` (`id`, `name`, `description`) VALUES
(1, 'Amministrazione', 'Tutti i servizi disponibili.'),
(2, 'Moderazione Forum', 'Accesso dashboard admin, gestione forum'),
(3, 'Statistiche', 'Accesso dashboard admin.'),
(10, 'Personalizzato', 'Valori custom'),
(11, 'Personalizzato 1', 'Personalizzato 1'),
(12, 'Personalizzato 2', 'Personalizzato 2'),
(13, 'Personalizzato 3', 'Personalizzato 3'),
(14, 'Personalizzato 4', 'Personalizzato 5'),
(15, 'Utente', 'Accesso Utente');

-- --------------------------------------------------------

--
-- Table structure for table `role_groups_services`
--

CREATE TABLE `role_groups_services` (
  `id` int(11) NOT NULL,
  `groupId` int(10) UNSIGNED NOT NULL,
  `serviceID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_groups_services`
--

INSERT INTO `role_groups_services` (`id`, `groupId`, `serviceID`) VALUES
(1, 1, 1),
(22, 1, 2),
(16, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(54, 2, 1),
(63, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `role_groups_user`
--

CREATE TABLE `role_groups_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_groups_user`
--

INSERT INTO `role_groups_user` (`id`, `group_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(26, 15, 214, NULL, NULL),
(27, 2, 213, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `description`, `created_at`, `updated_at`, `archived`) VALUES
(1, 'Introduzione', NULL, '2018-06-21 13:53:45', 0),
(2, 'Supporto', NULL, '2018-06-21 13:54:20', 0),
(3, 'Reparto Tecnico', NULL, NULL, 0),
(4, 'Strategie', NULL, NULL, 0),
(5, 'Off-Topic', NULL, '2018-06-08 17:13:16', 0),
(6, 'Press', '2018-06-08 16:04:40', '2018-06-08 17:15:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `description`) VALUES
(1, 'AdminDash', 'Accesso alla dashboard di amministrazione'),
(2, 'ManageNews', 'Gestione News'),
(3, 'ManageSurvey', 'Gestione Sondaggi'),
(4, 'ManageUsers', 'Gestione Utenti'),
(5, 'ManageForum', 'Gestione Forum'),
(6, 'ManageLiveEvents', 'Gestione eventi live'),
(7, 'ManagePermissions', 'Gestione Permessi');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'test', 'prova', '2018-07-22 22:00:00', NULL),
(2, 'test', 'prova', '2018-07-24 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE `surveys` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` int(10) UNSIGNED NOT NULL,
  `result` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reward` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `title`, `description`, `author`, `result`, `reward`, `created_at`, `updated_at`) VALUES
(2, 'SONDAGGIO 2', ' Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vi', 1, NULL, 10.00, NULL, NULL),
(3, 'asd', 'sd', 1, NULL, 3.00, '2018-06-08 19:56:43', '2018-06-08 19:56:43');

-- --------------------------------------------------------

--
-- Table structure for table `survey_answers`
--

CREATE TABLE `survey_answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `survey_option_id` int(10) UNSIGNED NOT NULL,
  `survey_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `survey_answers`
--

INSERT INTO `survey_answers` (`id`, `survey_option_id`, `survey_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 1, '2018-08-27 14:50:48', '2018-08-27 14:50:48');

-- --------------------------------------------------------

--
-- Table structure for table `survey_options`
--

CREATE TABLE `survey_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `survey_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `survey_options`
--

INSERT INTO `survey_options` (`id`, `description`, `survey_id`, `created_at`, `updated_at`) VALUES
(1, 'Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae.', 2, '2018-05-07 22:00:00', NULL),
(2, 'Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae.', 2, '2018-05-07 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `birthdate`, `email`, `archived`, `image`, `address`, `updated_at`, `created_at`, `password`, `remember_token`) VALUES
(1, 'Simone', 'Marrone', '1994-07-21', 'simonbrown994@gmail.com', 0, 'images/avatars/lnY3qGhm0Mn4xlqfWcKoOio39tiIxWtOLc0MSBM3.jpeg', 'Italy', '2018-09-12 00:02:56', '2018-04-27 12:21:36', '$2y$10$tX4Dj9fI0A32EOB5KMOcnuTG3NXdRBPfQLSAggA2r9WrPP9L1WG0K', 'HmW2KVfEG2UFlssu6XJOOtK2SzPEcXTblGAr3Vj5k8EnCs0lwEB98AsjGoHI'),
(203, 'Sharon', 'Rubini', '2018-11-10', 'sharonrubini95@gmail.com', 0, 'images/Sharon.jpg', 'Italia', NULL, NULL, '$2y$10$GqyapmPDm1n.WDmzCfiJN.F/4UadPuV5mNzbk7WovVWTk5h06/HF2', 'Rb4CLWs1TSs5Z5JodBEcltFbAq3skphsBXDgMfaiIlOMH6APtdxvRzRhTq3r'),
(207, 'Felix', 'Stout', NULL, 'Felix@Stout.com', 0, 'images/avatars/032.jpg', 'Sierra Leone', '2018-09-11 12:00:25', '2018-09-11 12:00:25', '$2y$10$Som4xa.KlaAjkzN8eO0G2..1XhfJJmPvMWgbSBTnfpMuBjzTGucMu', NULL),
(208, 'Michael', 'Trujillo', NULL, 'Michael@Trujillo.com', 0, 'images/avatars/012.jpg', 'Iraq', '2018-09-11 12:00:25', '2018-09-11 12:00:25', '$2y$10$7uzQ/3XB3mjEZhQRIBrQOOdBf4eMO.54c6FlGGSP7mCr2Lb9xR0r2', NULL),
(209, 'Emi', 'Sellers', NULL, 'Emi@Sellers.com', 0, 'images/avatars/013.jpg', 'Gibraltar', '2018-09-11 12:00:25', '2018-09-11 12:00:25', '$2y$10$rzHkpy/kySgFSOluzrlVLuFWFeTSte0b1ZOeMw9SY3QpWjXNAbSLK', NULL),
(210, 'Richard', 'Ramos', NULL, 'Richard@Ramos.com', 0, 'images/avatars/010.jpg', 'Tajikistan', '2018-09-11 12:00:25', '2018-09-11 12:00:25', '$2y$10$Pr83xVSi1nHOhKouIxY.BuJrS.rKObQsm3YSqi3Vh15eSQgkfOh2W', NULL),
(211, 'Teddy', 'Casey', NULL, 'Teddy@Casey.com', 0, 'images/avatars/003.jpg', 'Tajikistan', '2018-09-11 12:00:25', '2018-09-11 12:00:25', '$2y$10$nhx17LgwWtmczKfLl1xueuRBKXzUvIPzlA5L5IVzeI2X764BvGprq', NULL),
(212, 'Itoe', 'Young', NULL, 'Itoe@Young.com', 0, 'images/avatars/035.jpg', 'United Arab Emirates', '2018-09-11 12:00:25', '2018-09-11 12:00:25', '$2y$10$/3EBoK20rMMUwm8U6FsLzes2FH2KrTiHB5ZnVczz0ASpuTpsqkKji', NULL),
(213, 'Bill', 'Fields', NULL, 'Bill@Fields.com', 0, 'images/avatars/016.jpg', 'Turkmenistan', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$zj31sK6X1HXL3vlPVWb2ROI5u.QlugAD8EBo/gFaJWQpaf9zThwdC', NULL),
(214, 'Kumiko', 'Rhodes', NULL, 'Kumiko@Rhodes.com', 0, 'images/avatars/021.jpg', 'United States', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$JwByhY.JayQxeWkH0Z..Nefg5/pVbHPFDXA25pOR0cb.z61.sklTi', 'EdVWBT9wTNMuUfF67eFZvo19QjhgnZk0QwGfsAO4jw6NOWihCSL6nnCw8JNN'),
(215, 'Katrina', 'Johns', NULL, 'Katrina@Johns.com', 0, 'images/avatars/006.jpg', 'Brunei Darussalam', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$T2dUPVlr1cJWMHHrlKRRVOxQOHs4tsDCv6Fprl9pxbX9K832nKCq.', NULL),
(216, 'Leslie', 'Frank', NULL, 'Leslie@Frank.com', 0, 'images/avatars/009.jpg', 'Cuba', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$KWcgBs4ZBfSCg2oIPimZGeXuLl33VYsfLDtRKWUV7BIQ9fQ.aqaRu', NULL),
(217, 'Youko', 'Rich', NULL, 'Youko@Rich.com', 0, 'images/avatars/006.jpg', 'Belgium', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$mvgbT31xO4SDLDB2LFsLm.e6jlgZpdOZgBEnLh1pHamhrvyT27JJO', NULL),
(218, 'Fay', 'Whitehead', NULL, 'Fay@Whitehead.com', 0, 'images/avatars/036.jpg', 'Sudan', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$L7ySA113g7t0FRzwO.03NOn.ByV2PWI4Cw21amGdFubL.qYLsMMqS', NULL),
(219, 'Bertha', 'Livingston', NULL, 'Bertha@Livingston.com', 0, 'images/avatars/026.jpg', 'Sao Tome and Principe', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$gQ.wpZ2e.nT8/.Ei2fGbLuVJ1rCXKOuOlqeUI5ox5zw/Yu5C7iz8K', NULL),
(220, 'Peter', 'Beard', NULL, 'Peter@Beard.com', 0, 'images/avatars/007.jpg', 'Liechtenstein', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$3vDU708ktJv9g6VGSlW5EuwktGKh7MEaWJPs9yTZ.QFEqEdyRRawG', NULL),
(221, 'Nicole', 'Levy', NULL, 'Nicole@Levy.com', 0, 'images/avatars/008.jpg', 'Canada', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$DH2Nz380S53hWe5u5iUE3uyZPGv1O63elgXo9OK6WAjLnSckbZqqC', NULL),
(222, 'Cristobal', 'Witt', NULL, 'Cristobal@Witt.com', 0, 'images/avatars/038.jpg', 'Oman', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$.cRXIBUv896SJjr8p61tAOpYNlQhbA.XklzBQiOd7ZuctkndGIWSG', NULL),
(223, 'Ana', 'Vang', NULL, 'Ana@Vang.com', 0, 'images/avatars/013.jpg', 'Greece', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$x3AwkPdG6yBoH8LYuGDAheky54kkCslUJUx2IRe8974dfoc7sXjdu', NULL),
(224, 'Hermine', 'Koch', NULL, 'Hermine@Koch.com', 0, 'images/avatars/027.jpg', 'Italy', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$8NWYPUiVRdp2P8b3fQd88OSxDP157lqVvmTr1AwsfBU5ZxseHnwke', NULL),
(225, 'Miyuki', 'Mack', NULL, 'Miyuki@Mack.com', 0, 'images/avatars/004.jpg', 'Bahrain', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$Uyknf8zWLrXBzAF3qmQZNOKgYaKApZ1S6IZNv7iuCYu.VTcQ0zvAq', NULL),
(226, 'Franklin', 'Holloway', NULL, 'Franklin@Holloway.com', 0, 'images/avatars/014.jpg', 'Guinea', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$YHQR6eWuGu8SZf85/X54H.4ThTVkAr4TMGpz8FOY3MztW/pX3rU1a', NULL),
(227, 'Nicholas', 'Berry', NULL, 'Nicholas@Berry.com', 0, 'images/avatars/032.jpg', 'Virgin Islands (British)', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$iMqp/dVgf5Bld6HPrkDtbukRm2v2bZoxQgUVFscOwHYjFfMWZvv/C', NULL),
(228, 'Dean', 'Whitley', NULL, 'Dean@Whitley.com', 0, 'images/avatars/033.jpg', 'Bouvet Island', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$dKHUxX/DgOTtwTgDy0J.zO2f2o6hd.b/L7cIh3jB3yDeFkSotvemW', NULL),
(229, 'Teresa', 'Cortez', NULL, 'Teresa@Cortez.com', 0, 'images/avatars/005.jpg', 'Zambia', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$6bYpv11roZB3rsvkJJLC1Oky4TywkC.9S7aJNhx81QWrIcC8hjiTi', NULL),
(230, 'Naoko', 'Arnold', NULL, 'Naoko@Arnold.com', 0, 'images/avatars/020.jpg', 'Syrian Arab Republic', '2018-09-11 12:00:26', '2018-09-11 12:00:26', '$2y$10$CaEsluMKppmghNicJC9w1eHnGGysROnsIcankkTe/VQN81aXZbPbS', NULL),
(231, 'Karl', 'Alvarez', NULL, 'Karl@Alvarez.com', 0, 'images/avatars/018.jpg', 'Bahrain', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$USmwQdHCJgXWXEOdSjhbNuFr5xxwny/Ixi3mL5LnE0uYyeY2yb62.', NULL),
(232, 'Philippe', 'Bernard', NULL, 'Philippe@Bernard.com', 0, 'images/avatars/007.jpg', 'Greece', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$Qkgm.E1UYeBXazuTPiyBL.2l008yyAy3QMi7ruDKY8j1jofj2yQ/m', NULL),
(233, 'Ernesto', 'Armstrong', NULL, 'Ernesto@Armstrong.com', 0, 'images/avatars/024.jpg', 'Nauru', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$G6JIYgFDDgKhEWtVYE944.ozRYItVKCtwSnjDZ35uAceSoxFqSnIC', NULL),
(234, 'Gaston', 'Collins', NULL, 'Gaston@Collins.com', 0, 'images/avatars/007.jpg', 'Argentina', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$Ql7THWXGMl1hG7uxdM2vBeIbehcm0yN6.KAtaaWRyf9zMJuE.T/W.', NULL),
(235, 'Yumi', 'Buck', NULL, 'Yumi@Buck.com', 0, 'images/avatars/018.jpg', 'Seychelles', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$QgBgxisX/NfxtHO0HFVZaOzdULVqGnLl/hSbGFrnG4IkTyGtaWolK', NULL),
(236, 'Tanya', 'Ramirez', NULL, 'Tanya@Ramirez.com', 0, 'images/avatars/020.jpg', 'Myanmar', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$E8NKNpgstIM.C6sYHTp2feIAzphE2/PxEw5eoxrIIY20B2M3PF2j6', NULL),
(237, 'Van', 'Gillespie', NULL, 'Van@Gillespie.com', 0, 'images/avatars/038.jpg', 'Pitcairn', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$vRfj9VFRHujvVwvom7B.C.Cg1N766BLRsAMbbXNxNsrTiPWkN2qSO', NULL),
(238, 'Virginie', 'Herman', NULL, 'Virginie@Herman.com', 0, 'images/avatars/039.jpg', 'Armenia', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$0KJzK5c5dQMdIOFySU9mBupXWYLDfzPWeQ01NXzVQttCztuIOU.Ye', NULL),
(239, 'Tomas', 'Greene', NULL, 'Tomas@Greene.com', 0, 'images/avatars/025.jpg', 'Netherlands', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$GhYkFJOsjy9A6l.uXqc5cux6PwAw.I26h.1O4c.u.lo7/YvxcWP9.', NULL),
(240, 'Gert', 'Brooks', NULL, 'Gert@Brooks.com', 0, 'images/avatars/007.jpg', 'Norfolk Island', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$9jL.2lWjf04loCd6gLhueOhVD1eMuaLN4FDkPXRWi/q8EEKM0fqiC', NULL),
(241, 'Gert', 'Simmons', NULL, 'Gert@Simmons.com', 0, 'images/avatars/016.jpg', 'Bhutan', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$xihVJB8/4Ju5j539zfAY2uigyY844BhQzFe0d263EtzbezeF/DPim', NULL),
(242, 'Irene', 'Rodgers', NULL, 'Irene@Rodgers.com', 0, 'images/avatars/008.jpg', 'Costa Rica', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$MRl0RcboGr.RuyUaSU7AeumZfTIx2L2cAcGSSM0Boi.8nVecvrpfy', NULL),
(243, 'Nana', 'Gregory', NULL, 'Nana@Gregory.com', 0, 'images/avatars/025.jpg', 'Bahrain', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$UK4cNFGMVc6zgkxzaviRkOCPoqvwpWhSIkq6Iw9h3xtJmogtl4eUe', NULL),
(244, 'Bertha', 'Vinson', NULL, 'Bertha@Vinson.com', 0, 'images/avatars/009.jpg', 'Maldives', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$UkdTdKFFyEk.LYk.lwdxMeO9Lqg9Z0WObySBnxzoJ2LA69yL/CkBy', NULL),
(245, 'Barry', 'Patel', NULL, 'Barry@Patel.com', 0, 'images/avatars/020.jpg', 'Guyana', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$iY7Tq2z6k5b8/RN2Adv.EutDzduBXAXIpvZh8kPrqAatyNmPbQFVO', NULL),
(246, 'Jeanne', 'Castaneda', NULL, 'Jeanne@Castaneda.com', 0, 'images/avatars/010.jpg', 'Yugoslavia', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$LyoGhOtZogwU7NuVDDeciOCXFONW2xDXRhsCgJIlhaXduC5geLsqq', NULL),
(247, 'Barry', 'Moon', NULL, 'Barry@Moon.com', 0, 'images/avatars/003.jpg', 'Mali', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$XZM8awFUZKD.eql1llxkGOtZEim3fSbl2JGFlKi/6pEA8BkOFjpsS', NULL),
(248, 'Katrina', 'Hunt', NULL, 'Katrina@Hunt.com', 0, 'images/avatars/037.jpg', 'Morocco', '2018-09-11 12:00:27', '2018-09-11 12:00:27', '$2y$10$nssPketOssY/lPZIigpOd..GchadKsuUQfVeMmU7aYdwsVP.BZeRa', NULL),
(249, 'Vince', 'Ware', NULL, 'Vince@Ware.com', 0, 'images/avatars/022.jpg', 'Cambodia', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$rx1Yy9jDyZoBL3Jz4mkrbOTs4be4ALk3p.RYWUo5uBUSN8/WDNU3i', NULL),
(250, 'Omar', 'Gibson', NULL, 'Omar@Gibson.com', 0, 'images/avatars/034.jpg', 'Cocos (Keeling) Islands', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$u8j5VOTgIaO2D6ShQwk2X.O1HRCQcJhcxD0fzTcAT1wO98GGFU/Nq', NULL),
(251, 'Sayuri', 'Bentley', NULL, 'Sayuri@Bentley.com', 0, 'images/avatars/004.jpg', 'Norfolk Island', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$Plf0xlZcMQy7F2b6K9vSseVC98Jjmu9.vNv9LuLGvI2nraDpaV6LC', NULL),
(252, 'Fay', 'Robinson', NULL, 'Fay@Robinson.com', 0, 'images/avatars/026.jpg', 'Samoa', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$7SMvC6Or.7DSb/BN5P7OrOuJyGb3ZHg5VaKqsfQZjwQ4e6A/1cbRO', NULL),
(253, 'Virginie', 'Little', NULL, 'Virginie@Little.com', 0, 'images/avatars/023.jpg', 'Germany', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$VTckoEKAJacKrZ.8N1vb2usDN4Yi8lcljPj1luI6UwZ30LuayIgRe', NULL),
(254, 'Arlene', 'House', NULL, 'Arlene@House.com', 0, 'images/avatars/025.jpg', 'Zimbabwe', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$kBBi1TBNrN8rMZvNBrlpl.7KtuQGWzYtM7MXFwhnNfZAfwT0FlKi.', NULL),
(255, 'Dean', 'Munoz', NULL, 'Dean@Munoz.com', 0, 'images/avatars/037.jpg', 'Barbados', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$u1LObR5Zu3UOVO7h2EGLZeCkjbTIpgpTWzumPd0lHvFWSNsBrfqZ2', NULL),
(256, 'Hanna', 'Sherman', NULL, 'Hanna@Sherman.com', 0, 'images/avatars/019.jpg', 'Central African Republic', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$njMPmjw2.JzL8K0kciQbce0G3yE/Bi8us/Ch6nRTAfBIKHi2G/8t2', NULL),
(257, 'Grace', 'Gentry', NULL, 'Grace@Gentry.com', 0, 'images/avatars/003.jpg', 'Equatorial Guinea', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$IVDK1Q8weVuAPRXmNV1DN.BvAIRCLO1JH0Wz9j8MffFgBV08iQV8S', NULL),
(258, 'Otto', 'Curtis', NULL, 'Otto@Curtis.com', 0, 'images/avatars/026.jpg', 'French Southern Territories', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$0weU9gO2fZpe5/h/4.ZSfeThJ.gqJ19ElHdVVpEI8NFkkNL3YB.6a', NULL),
(259, 'Grace', 'Douglas', NULL, 'Grace@Douglas.com', 0, 'images/avatars/022.jpg', 'Greece', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$qoVEomtw000EzJKRM18cg.KR6UnEuSJccQI0hAqK2Y6sbly0e8ZiC', NULL),
(260, 'Emi', 'Rhodes', NULL, 'Emi@Rhodes.com', 0, 'images/avatars/015.jpg', 'Armenia', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$6ssVemuSzUBj6mQc5pnAM.mGO/TFIhOZyjhbv0X0Ubb5xE5xMQUfe', NULL),
(261, 'Bonnie', 'Gardner', NULL, 'Bonnie@Gardner.com', 0, 'images/avatars/034.jpg', 'Nicaragua', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$G7cwDf2jGSlgZxq8flP/oO7VmElE/cShKqA3Mbk6LHyfGAKoHOH1.', NULL),
(262, 'Isidore', 'Vasquez', NULL, 'Isidore@Vasquez.com', 0, 'images/avatars/029.jpg', 'Equatorial Guinea', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$Nyz3OewGdpGzmLwUZnYGqu9JFJv.h5cRXCUJJ3bD3umi1i.UwON62', NULL),
(263, 'Charley', 'Mann', NULL, 'Charley@Mann.com', 0, 'images/avatars/017.jpg', 'Panama', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$KNE5fVzGHfR0LKpUEL3F3.cEYl9xUG3880cHqxeDhUyFv0WeFmpuS', NULL),
(264, 'Cristobal', 'Porter', NULL, 'Cristobal@Porter.com', 0, 'images/avatars/028.jpg', 'Norway', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$SCaFb7mOIfVGa9GHMPVW7uB2siz3rFN6.R7/gi1DiDMiJz3lDzFy.', NULL),
(265, 'Ivan', 'Collier', NULL, 'Ivan@Collier.com', 0, 'images/avatars/017.jpg', 'Romania', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$o.J8pOVurGXcnZuTfT/o2unoUphzZfkaO4KrUn3LhgevAb4T46seK', NULL),
(266, 'Jeanne', 'Melton', NULL, 'Jeanne@Melton.com', 0, 'images/avatars/005.jpg', 'Malaysia', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$BQ7iTsCS8M3osQP5koaLFuJP0jjCNAmjBvfhkZZxP2oa0DNqp9XKC', NULL),
(267, 'Isidore', 'Soto', NULL, 'Isidore@Soto.com', 0, 'images/avatars/013.jpg', 'Sri Lanka', '2018-09-11 12:00:28', '2018-09-11 12:00:28', '$2y$10$wI4ViEFXcmiAL7i.z//od.P.sWKt8idnHSmbfg7EDvUM6M5ZShL/q', NULL),
(268, 'Tony', 'Gomez', NULL, 'Tony@Gomez.com', 0, 'images/avatars/017.jpg', 'Chad', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$3rJGxegDdNAOzB5ZL0p2s.E68r/K1O0/9vEPUx5Ban1/z/djH3oPW', NULL),
(269, 'Kumiko', 'Lloyd', NULL, 'Kumiko@Lloyd.com', 0, 'images/avatars/019.jpg', 'China', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$DKgitLAUBPYf6rVSwhGa9epMQx9hVavQFOczfqKAFXREKr6N9WGkC', NULL),
(270, 'Isidore', 'Brown', NULL, 'Isidore@Brown.com', 0, 'images/avatars/014.jpg', 'India', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$/CxKQEOQa5VvPhO0G7Qn9u4u1DoGwj0aifydYw6k4ZJuWfuw0lnB2', NULL),
(271, 'Lili', 'Vinson', NULL, 'Lili@Vinson.com', 0, 'images/avatars/009.jpg', 'Faroe Islands', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$eD0j95v6A4duL/nQdAF5qeHk9MF1JQSIKEbcgwj62SOLEFK0lrvUa', NULL),
(272, 'Barry', 'Zamora', NULL, 'Barry@Zamora.com', 0, 'images/avatars/035.jpg', 'Canada', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$eudsZINZku2N.O2HdJ/NpO0e/yC.4MhpqPwxxP6n0PHZjhrKq9RBi', NULL),
(273, 'Bill', 'Serrano', NULL, 'Bill@Serrano.com', 0, 'images/avatars/005.jpg', 'France Metropolitan', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$4fne.jW7b.u7.mm6BZJ3SeeI3z0aGXvGuEuJ9Q5t7LC/5wSN./efu', NULL),
(274, 'Jose', 'Bolton', NULL, 'Jose@Bolton.com', 0, 'images/avatars/025.jpg', 'Mozambique', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$LFytgp.U..C9teLdYdMVye6.biqsYEwKWV11.PA3HL9Bo9CCIGiT.', NULL),
(275, 'Harvey', 'Hardy', NULL, 'Harvey@Hardy.com', 0, 'images/avatars/031.jpg', 'Niger', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$uvZpPYshkAvVJon5y6FN6O5LqZodGTaobByIJ8IM4HbG44hfcC2du', NULL),
(276, 'Gaston', 'Velez', NULL, 'Gaston@Velez.com', 0, 'images/avatars/015.jpg', 'Norfolk Island', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$DwzQLu73S.zxF0rs25AnPO.3q2sObSRQhLxBbypDD4IneVZPExncy', NULL),
(277, 'Wendy', 'Hines', NULL, 'Wendy@Hines.com', 0, 'images/avatars/040.jpg', 'Bangladesh', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$z4Wl36zsOyuh0/NeAav5FOT540Lr4Nmph6DeYCBuMXNc.73Oyq5ti', NULL),
(278, 'Leslie', 'Thomas', NULL, 'Leslie@Thomas.com', 0, 'images/avatars/031.jpg', 'Dominica', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$NOY2rt7dYjzqcjJgMpUhROuDCA68olCEOBeNsJvbUx1aMPALMF3IW', NULL),
(279, 'Barry', 'Clarke', NULL, 'Barry@Clarke.com', 0, 'images/avatars/008.jpg', 'Djibouti', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$ZsGuslHsrufqQSmm1CuGM.Ap7hpCu9o5wim4stZ.7aEamqnVUTDya', NULL),
(280, 'Peter', 'Edwards', NULL, 'Peter@Edwards.com', 0, 'images/avatars/005.jpg', 'Hungary', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$P8jV3ME8WfSctOt1Q1SwCuYuSOVdC8Vr1/L1fX1jvtXpbIFSyrOry', NULL),
(281, 'Helene', 'Clemons', NULL, 'Helene@Clemons.com', 0, 'images/avatars/040.jpg', 'Honduras', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$PnkOW9R1Scqy4RJyde62UOtgm4IkUJnvmLRBFZTL1F3Q8vDnXQBEe', NULL),
(282, 'Bret', 'Koch', NULL, 'Bret@Koch.com', 0, 'images/avatars/002.jpg', 'Papua New Guinea', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$2sVqMGt4H7hSyWwa5QfD8.UHTcy11zB.6w1delRrP3QS5JCnjB9..', NULL),
(283, 'Mindy', 'Stuart', NULL, 'Mindy@Stuart.com', 0, 'images/avatars/022.jpg', 'Antigua and Barbuda', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$skaOwObVHK7zxFH96lYnZeqsmVAm/BeCCsgQjTll7layC4MYz1urW', NULL),
(284, 'Leslie', 'Branch', NULL, 'Leslie@Branch.com', 0, 'images/avatars/015.jpg', 'Hungary', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$DVfAdLelVuRSHjgNk1axhueDusZVKsKMsVjunC2qkvc2SkqGXpLE6', NULL),
(285, 'Florence', 'Johnson', NULL, 'Florence@Johnson.com', 0, 'images/avatars/005.jpg', 'Macedonia, The Former Yugoslav Republic of', '2018-09-11 12:00:29', '2018-09-11 12:00:29', '$2y$10$5mkT1iYuWJZalCcgQJL9GObzU0Q6KjxDmPiMdLiWf8f88uIRIJ0Gy', NULL),
(286, 'Reina', 'Zimmerman', NULL, 'Reina@Zimmerman.com', 0, 'images/avatars/028.jpg', 'Myanmar', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$5RKrhymsSzLn4jmrO512dOthE.VGPxovrU2GrHWlHx5YsAveJ3equ', NULL),
(287, 'Tony', 'Bush', NULL, 'Tony@Bush.com', 0, 'images/avatars/005.jpg', 'Lebanon', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$ebzQ9sfFghPpi1yGhgS7fuT7YRsu97mBEWcWTU0hH6q7SfG9lV3La', NULL),
(288, 'Helene', 'Hale', NULL, 'Helene@Hale.com', 0, 'images/avatars/022.jpg', 'Mali', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$2IdxJHRzNH8uykJa1FIBGevKJSfDK1vGokk4r1ZStpW9se5AzRE3W', NULL),
(289, 'Nanaho', 'Dyer', NULL, 'Nanaho@Dyer.com', 0, 'images/avatars/033.jpg', 'Netherlands Antilles', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$4f.pAIiobxX0unHwSJKCoexfC3JdjRoK3bdmLhtbn3sFBZlzN98mi', NULL),
(290, 'Hitomi', 'Deleon', NULL, 'Hitomi@Deleon.com', 0, 'images/avatars/021.jpg', 'Gambia', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$SVeg3/ACo72AkPC8JG7umO6B.8W6xrnIui7/rCH32UO4bqmvSg6nC', NULL),
(291, 'Isaac', 'Sawyer', NULL, 'Isaac@Sawyer.com', 0, 'images/avatars/021.jpg', 'Uruguay', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$drBNbVEbBFeXdwJQ5PxT8OI.qSYlmaikNkqj7YousMqHU0.eB2W4C', NULL),
(292, 'Tammy', 'Campos', NULL, 'Tammy@Campos.com', 0, 'images/avatars/027.jpg', 'Mayotte', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$JpLAuYRHUbpHgUhnD.PIsOnR13QP1YehcHNkKoQGT0J6QkIz.rdZ6', NULL),
(293, 'Claudette', 'Wynn', NULL, 'Claudette@Wynn.com', 0, 'images/avatars/022.jpg', 'Switzerland', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$Q6ivLmgvGF9q37KpZOWVyeWORfPzuD9TYLSv81UmKHYJX0G4DCvNW', NULL),
(294, 'Harvey', 'Waller', NULL, 'Harvey@Waller.com', 0, 'images/avatars/002.jpg', 'Zimbabwe', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$1dXZRInErLPdj6yDq9w/.OpBncoi0fKN647/dFrfy805ncobYTVaa', NULL),
(295, 'Miyako', 'Park', NULL, 'Miyako@Park.com', 0, 'images/avatars/001.jpg', 'Norfolk Island', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$73e2H4EsN6hI.QpshTQ21up./X77JV/EnYLVjfePyKSOCw3QYT9FG', NULL),
(296, 'Felix', 'Payne', NULL, 'Felix@Payne.com', 0, 'images/avatars/026.jpg', 'Morocco', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$9D/SYqfLTGPIbJtBSVEck.rsnSg7RENc6seRxwpb5E23nzske.Eq.', NULL),
(297, 'Franklin', 'Wood', NULL, 'Franklin@Wood.com', 0, 'images/avatars/019.jpg', 'France', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$XpbSvkDgr.kvajTn5yp8vOfC6UMAfZDZWjVOV3gzwp77nEPTC1bYe', NULL),
(298, 'Walter', 'Chapman', NULL, 'Walter@Chapman.com', 0, 'images/avatars/006.jpg', 'Cameroon', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$A7JjM6LUpipUQewXsglV8O2loOnrhLUjqFPlCFgTyy1A4iGcGw7.a', NULL),
(299, 'Itoe', 'Chan', NULL, 'Itoe@Chan.com', 0, 'images/avatars/011.jpg', 'Sudan', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$cgfPdHkIPVZL63RHXMXlm.bIHRxyZ8fPYxWG8WPgHmIC2boKoD3u.', NULL),
(300, 'Patty', 'Durham', NULL, 'Patty@Durham.com', 0, 'images/avatars/006.jpg', 'Singapore', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$n25vn.WTfByt0I3iLzZvqeGH5p.1bJPogQfjcnDw5rO2tq0C8S8yS', NULL),
(301, 'Rene', 'Blackwell', NULL, 'Rene@Blackwell.com', 0, 'images/avatars/035.jpg', 'Australia', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$Q0GoBv0Y5xqyaVLOOW974eOrp9YiDconUTXjXu25JgPtsGgBa3/lW', NULL),
(302, 'Allison', 'Wise', NULL, 'Allison@Wise.com', 0, 'images/avatars/014.jpg', 'Bulgaria', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$NZQl01W1OfUUoWu8Ugd0X.i4nnIIQGh6qdKg/ILMQPL0jQOH5WtmW', NULL),
(303, 'Chantal', 'White', NULL, 'Chantal@White.com', 0, 'images/avatars/035.jpg', 'Micronesia, Federated States of', '2018-09-11 12:00:30', '2018-09-11 12:00:30', '$2y$10$pPyevSOeKZJM0.g3WDnDZeZhSBFmMhpxJ/7EFu.JfaTGZkvVDdNTi', NULL),
(304, 'Dolly', 'Cote', NULL, 'Dolly@Cote.com', 0, 'images/avatars/015.jpg', 'Ireland', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$ztbMc2CblBXtsNlWZzLHUuN3OxucHuekp9ftNhRd4PkvkWVeX7PU6', NULL),
(305, 'Nadine', 'Santos', NULL, 'Nadine@Santos.com', 0, 'images/avatars/014.jpg', 'Eritrea', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$ngHb6qJI3lKnUEx7VVxaK.uSEbXXN0oHKERTi/IPPyiN/b9ilUDpG', NULL),
(306, 'Franklin', 'Rose', NULL, 'Franklin@Rose.com', 0, 'images/avatars/035.jpg', 'Reunion', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$FAmrXpxubXuzyZkV0R6NKe717XUNQG/k9BxDZaKMawzAhlykS3Bkq', NULL),
(307, 'Dennis', 'Cunningham', NULL, 'Dennis@Cunningham.com', 0, 'images/avatars/025.jpg', 'Ethiopia', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$oA2EFTwWLN7s8cPPptnAqOoeJi1TbjLsInCjQyfzGDdEgow70vicu', NULL),
(308, 'Danielle', 'Snyder', NULL, 'Danielle@Snyder.com', 0, 'images/avatars/025.jpg', 'Maldives', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$u.NPuetEnEOWvfo.bZELTOpDG7s.JJUbgOcO3bkm6WgHmhk0SWfaS', NULL),
(309, 'Rebekah', 'Christensen', NULL, 'Rebekah@Christensen.com', 0, 'images/avatars/011.jpg', 'Central African Republic', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$f1O4P2ipcC6wKsgY7mncEudwXue.ADsXklc3wYhhWFuRMnatJNa/.', NULL),
(310, 'Arlene', 'Meyers', NULL, 'Arlene@Meyers.com', 0, 'images/avatars/010.jpg', 'Belize', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$AO2YBqxGuUduLn5zyvwYh.3aaYQrFk7Siyr.JrYUEEhEhm2F6f84K', NULL),
(311, 'Mieko', 'Buck', NULL, 'Mieko@Buck.com', 0, 'images/avatars/036.jpg', 'Martinique', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$2MAkLz/pSG6J.4al.DrRxeFefqtQPvMA7iE6fNpULPwdOgIYUHfUa', NULL),
(312, 'Bret', 'Santiago', NULL, 'Bret@Santiago.com', 0, 'images/avatars/033.jpg', 'Dominica', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$CpCCu9DQJbRfCSMJvIOejuJ78w/dlD0tDXZbCqRDmOmOO9Ve0.FQS', NULL),
(313, 'Etsuko', 'Morton', NULL, 'Etsuko@Morton.com', 0, 'images/avatars/036.jpg', 'Panama', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$P4VJk//ul3s331V/nzTNZ.zqYSExw5QysRvLCLWFOXPsddRbA.hJi', NULL),
(314, 'Danielle', 'Cote', NULL, 'Danielle@Cote.com', 0, 'images/avatars/030.jpg', 'Bolivia', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$oQy7tDc3MgFRJF/VPtj/YO5HA2nqfMuRZDZcmUhRcoL.VXLelYEka', NULL),
(315, 'Yumi', 'Copeland', NULL, 'Yumi@Copeland.com', 0, 'images/avatars/016.jpg', 'United States Minor Outlying Islands', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$hP95Ck/cr3DoAd6PjSmaY.PL4dY.ShrI38OZPS3AaJLJt/zTMq4oa', NULL),
(316, 'Juan', 'Barrera', NULL, 'Juan@Barrera.com', 0, 'images/avatars/036.jpg', 'Peru', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$y6WN51ZfeFHRiGtVSAPySO9Shsi4yLIctfb6tuQYBwWjlY53C25Pa', NULL),
(317, 'Marco', 'Osborn', NULL, 'Marco@Osborn.com', 0, 'images/avatars/017.jpg', 'Guyana', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$VdMNIN7H48cOp4WnP/W2guFyzPbFySjml47hUh3oPS3up5.n8PycO', NULL),
(318, 'Jeanne', 'Sutton', NULL, 'Jeanne@Sutton.com', 0, 'images/avatars/025.jpg', 'Nepal', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$Wtrs1ChVA05ceh4qIMTFiOPmd.iyah0ljAZVl9gocYGEbWUJOKjMy', NULL),
(319, 'Rafael', 'Santiago', NULL, 'Rafael@Santiago.com', 0, 'images/avatars/017.jpg', 'Croatia (Hrvatska)', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$R7Dl44NjfbQJCkMhoV/oZucn9Efv6g4rg7ajy0IKmynvIen5nH.Eu', NULL),
(320, 'Odette', 'Reynolds', NULL, 'Odette@Reynolds.com', 0, 'images/avatars/017.jpg', 'China', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$meGBO/qJmEw8kwVNKyZiS.xji/lZx4ldTPij2MxlQcPae3X/IGRye', NULL),
(321, 'Sandy', 'Neal', NULL, 'Sandy@Neal.com', 0, 'images/avatars/008.jpg', 'Barbados', '2018-09-11 12:00:31', '2018-09-11 12:00:31', '$2y$10$ej9bvzGD1xEk/YMk1lP4dexCs7MrPqbFx0xGLSKrhevTJyCkruI9e', NULL),
(322, 'Nana', 'Simon', NULL, 'Nana@Simon.com', 0, 'images/avatars/014.jpg', 'Latvia', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$kSAAN52fgR0oGtjIujEU4OVInIdRJRjNIx3BaR9U.JlSbA5eYOJgq', NULL),
(323, 'Kumiko', 'Leblanc', NULL, 'Kumiko@Leblanc.com', 0, 'images/avatars/029.jpg', 'Kiribati', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$oue6t/.sZz5NcAXvx/gPXus.T/WE7dJpzOKKPt0HKeHRLZWs8Z7qm', NULL),
(324, 'Harvey', 'Navarro', NULL, 'Harvey@Navarro.com', 0, 'images/avatars/035.jpg', 'Tanzania, United Republic of', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$Q00RJ5a1xZ2IA.8F9cPHleueho1ixAFifN8iqQwj5qNYqY53qe5TS', NULL),
(325, 'Miyuki', 'Mckay', NULL, 'Miyuki@Mckay.com', 0, 'images/avatars/007.jpg', 'Dominica', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$0nrlMq81wwLdl7Z4yIIX2u3VupgxhyLbukclXWCNXJHHHMSDirZ/K', NULL),
(326, 'Bret', 'Mercer', NULL, 'Bret@Mercer.com', 0, 'images/avatars/008.jpg', 'Maldives', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$huxgJ1kjNRaT0BtrJnuPsudDjk7f3mg9JSJM8dubAVkV6yeDJTvwm', NULL),
(327, 'Dolly', 'Hyde', NULL, 'Dolly@Hyde.com', 0, 'images/avatars/035.jpg', 'Iran (Islamic Republic of)', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$klg/XYBMakbO.t53P2RsbOOMJm3YN/nUbSnxvXGmUWgk0nM4TJghq', NULL),
(328, 'Emi', 'Carey', NULL, 'Emi@Carey.com', 0, 'images/avatars/039.jpg', 'Wallis and Futuna Islands', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$ELbM2tuvYzvaYfjEsea5DOrvQvd8oc7NUWmAj9Spu5CairayDiIXO', NULL),
(329, 'Michael', 'Miller', NULL, 'Michael@Miller.com', 0, 'images/avatars/024.jpg', 'St. Helena', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$l.jkXPt2JBRSCxRSDWhbHOhGEO9BXyOv3ejxqoCOJd.o3RgK1J4bm', NULL),
(330, 'Jerry', 'Thornton', NULL, 'Jerry@Thornton.com', 0, 'images/avatars/035.jpg', 'Barbados', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$h47lfQB/B4lVy/1W6fiQq.o6VQPIcA/Wj1lmmj/7VkPWFZnmZmXqy', NULL),
(331, 'Humberto', 'Larson', NULL, 'Humberto@Larson.com', 0, 'images/avatars/026.jpg', 'Japan', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$MuAYIIzWyIKA.vg8wOrsX.GPTKDbgMOoIfHhDOzGvng5FFxjnVhuO', NULL),
(332, 'Josephine', 'Nelson', NULL, 'Josephine@Nelson.com', 0, 'images/avatars/020.jpg', 'Cocos (Keeling) Islands', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$pXQyC4qYsu58oii1czwii.JBbi0R217lBjxw37G0J8U1e0RgHsSSi', NULL),
(333, 'Fabian', 'Roach', NULL, 'Fabian@Roach.com', 0, 'images/avatars/026.jpg', 'Guadeloupe', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$Z4Zxp1LyXywkki20KGllL.DJ1OohGnnd2fNvPctl26ohInkwMSi06', NULL),
(334, 'Barry', 'Hawkins', NULL, 'Barry@Hawkins.com', 0, 'images/avatars/017.jpg', 'Andorra', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$.5WYg14vP1C4d0pia9EIK./E7z2GBuDI.eoTQqJOwnjMO69j4MlAa', NULL),
(335, 'Kate', 'Shaw', NULL, 'Kate@Shaw.com', 0, 'images/avatars/010.jpg', 'St. Helena', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$/cHcRKZDhgIzBSaZ01kOAeyc/9ZgBh34/RrRfsGdkln/KFoAllvpi', NULL),
(336, 'Tony', 'Howell', NULL, 'Tony@Howell.com', 0, 'images/avatars/008.jpg', 'French Southern Territories', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$Ry3QfdTUgk1SEmcWmkMoa.CeoilOmQs2NpkkG4OJl8lkJYKjGYu2K', NULL),
(337, 'Lili', 'Martin', NULL, 'Lili@Martin.com', 0, 'images/avatars/004.jpg', 'Zambia', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$o0KqNGMAKHV7UvCjHNtRUO6G65yA0mLDwhe20ujQw/Msshpxwj9a2', NULL),
(338, 'Oscar', 'Terrell', NULL, 'Oscar@Terrell.com', 0, 'images/avatars/020.jpg', 'Gambia', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$ATiy4LS1TnEBuv3w1rXEqOnJWu/T7g.84IFdCyRfyV7qm.XoqpeVC', NULL),
(339, 'Tammy', 'George', NULL, 'Tammy@George.com', 0, 'images/avatars/027.jpg', 'Switzerland', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$nP6buNeGC/RkVJql9x.sX.6loqVxHuHnSuAsbx/SgN7A1gva35Pta', NULL),
(340, 'Ophelia', 'Stephens', NULL, 'Ophelia@Stephens.com', 0, 'images/avatars/033.jpg', 'Saudi Arabia', '2018-09-11 12:00:32', '2018-09-11 12:00:32', '$2y$10$m85lBpn97eriSawi3R08TO6s36VymdulDuALPWnTaCPvsxxmmTPEi', NULL),
(341, 'Naoko', 'Pate', NULL, 'Naoko@Pate.com', 0, 'images/avatars/031.jpg', 'Japan', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$OtmDEVT2l56PpZYkWYz3IOrFF1l21sn0BgybQiK.tZA2beKdi3Lq6', NULL),
(342, 'Patty', 'Jarvis', NULL, 'Patty@Jarvis.com', 0, 'images/avatars/040.jpg', 'Equatorial Guinea', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$xr6lqRI9nAnu.mQIU/ounOceIyiD549CPUdmwgpfWEBDPAMU96UFO', NULL),
(343, 'Jeanne', 'Quinn', NULL, 'Jeanne@Quinn.com', 0, 'images/avatars/006.jpg', 'Chad', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$0Ennru9GaBCXRWlGCy070uRW2D2KaQcAEa/20bpI1r6ouEROUN1qm', NULL),
(344, 'Nanaho', 'Chandler', NULL, 'Nanaho@Chandler.com', 0, 'images/avatars/001.jpg', 'Saint Vincent and the Grenadines', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$eJo9lFWDfp16Y.sYf.aH4.6syj1x7O9pHObBHbwwVWmgSFykWlo1q', NULL),
(345, 'Barry', 'Lang', NULL, 'Barry@Lang.com', 0, 'images/avatars/015.jpg', 'Ireland', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$Y/ZQH6mWJRkA8lgJ4rn7XOkrIwr3kVabdpZH/xVW71KBjuhzyDtEm', NULL),
(346, 'Lili', 'Sampson', NULL, 'Lili@Sampson.com', 0, 'images/avatars/018.jpg', 'French Southern Territories', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$PJBPlMZ8INe1WAcTJCeXwOoG/krRr2.E3p33bqanaFA3vqHcTqIxe', NULL),
(347, 'Victor', 'Mcintosh', NULL, 'Victor@Mcintosh.com', 0, 'images/avatars/013.jpg', 'Tajikistan', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$jgN2VJ5iU0yTaJuifyGcLe5O4e2JOiL2s2mkq5efn.bO.lBxXjCNm', NULL),
(348, 'Nana', 'Middleton', NULL, 'Nana@Middleton.com', 0, 'images/avatars/040.jpg', 'Dominica', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$GOA0VpedDffu.7c1eBM39.xpa0nHpdypQ6K/6GLPeaVdxBdtl3i7G', NULL),
(349, 'Reina', 'Valenzuela', NULL, 'Reina@Valenzuela.com', 0, 'images/avatars/027.jpg', 'Bahamas', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$YHPl3InJKT/reTXkqZRlMuozh/JWL1PvqBye/9VCeymQ1arkRZ4Pa', NULL),
(350, 'Itoe', 'Russell', NULL, 'Itoe@Russell.com', 0, 'images/avatars/006.jpg', 'Trinidad and Tobago', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$XY8bugZQgVO7HmEwLyoARehlDGkdWA3oHBlgUnQiEuOEZso2E2lCS', NULL),
(351, 'Irene', 'Fuller', NULL, 'Irene@Fuller.com', 0, 'images/avatars/029.jpg', 'Tokelau', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$a6G8sMzhAYUy49Iup1PCLusz591BvQ88jRJhdrwrKWxneMmzsJcMK', NULL),
(352, 'Beryl', 'Durham', NULL, 'Beryl@Durham.com', 0, 'images/avatars/018.jpg', 'Israel', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$vdVjPsqYwm2on39aQsRpP.Hh1Es6OCrqzFXYI06ZfP3UVujfpMPg2', NULL),
(353, 'Nate', 'Melton', NULL, 'Nate@Melton.com', 0, 'images/avatars/022.jpg', 'Lao, People\'s Democratic Republic', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$Q6YubIhc3M68/9EGstn8IOV9yCgyO8OP5vuK2nzlmAy/ChFU7GI9m', NULL),
(354, 'Iris', 'Vazquez', NULL, 'Iris@Vazquez.com', 0, 'images/avatars/030.jpg', 'Mali', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$DQTIyH2hsH/odAoqOgOXyuz/Edmj7nwnZCJgGChuWZzjR95IeDJbm', NULL),
(355, 'Allison', 'Goodwin', NULL, 'Allison@Goodwin.com', 0, 'images/avatars/036.jpg', 'Syrian Arab Republic', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$bE9axPsodV5TuMfar4h6i.ImcxeF28LN9JEX3NgqcmKEYiyZH005W', NULL),
(356, 'Sebastien', 'Green', NULL, 'Sebastien@Green.com', 0, 'images/avatars/036.jpg', 'Benin', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$T5SoM/kR4iJ.6l48jV1vF.aoHMBAdW.331Z1.muYY/kz9r1PG0hZq', NULL),
(357, 'Danny', 'Stewart', NULL, 'Danny@Stewart.com', 0, 'images/avatars/004.jpg', 'Cuba', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$hYdagalaz/78dz03hM8IrO.zr96QeRZ0X3Iy1TU2fG3CGtQiIATMC', NULL),
(358, 'Richard', 'Parker', NULL, 'Richard@Parker.com', 0, 'images/avatars/017.jpg', 'Trinidad and Tobago', '2018-09-11 12:00:33', '2018-09-11 12:00:33', '$2y$10$MU9Cclwa3nmI1nRnKwNemeLWzhfBhzhBt2GC58ZDSNivQzx77wH/G', NULL),
(359, 'Leslie', 'Hammond', NULL, 'Leslie@Hammond.com', 0, 'images/avatars/030.jpg', 'Belize', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$mWKtvq/PbV5hY48nN7VBPObERrXjBg8RSfzyziuJe79SFnMXjd.Ra', NULL),
(360, 'Isidore', 'Lamb', NULL, 'Isidore@Lamb.com', 0, 'images/avatars/035.jpg', 'Congo, the Democratic Republic of the', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$4rx1ue83Xyqk6chJ00/dw.Po5vAOjbEB9qcUUxNNhQwXLQghFQnQG', NULL),
(361, 'Itoe', 'Mcmillan', NULL, 'Itoe@Mcmillan.com', 0, 'images/avatars/027.jpg', 'Argentina', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$8rYO.mThERDdub1GiXbwgemBSGADoa74Wm2NOmf2Y.vTBsrmQcgyC', NULL),
(362, 'Peter', 'Underwood', NULL, 'Peter@Underwood.com', 0, 'images/avatars/037.jpg', 'St. Pierre and Miquelon', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$tmk/v3mbF5ZM0r8QrzwEeeVG02s3uEotu9HyQrLUgo73/k1UAehcS', NULL),
(363, 'Iris', 'Willis', NULL, 'Iris@Willis.com', 0, 'images/avatars/023.jpg', 'Guinea-Bissau', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$UtILaujEGKmcPRK4NT9spuhEY2ocKeP/3DOtEkuUhiI8a6HoIvZ4O', NULL),
(364, 'Bertha', 'Keller', NULL, 'Bertha@Keller.com', 0, 'images/avatars/024.jpg', 'Madagascar', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$MnQTQ1gA16GXoPe5HXfjdOmcfTchoYXf9lA3OErepT1YASmFedbCu', NULL),
(365, 'Miyuki', 'Hendricks', NULL, 'Miyuki@Hendricks.com', 0, 'images/avatars/010.jpg', 'Armenia', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$A.odUhxk.Jj9avdtnH33Vux8RBIC64ZxPoI2ZZ88/2W7V3L0Xz55K', NULL),
(366, 'Cindy', 'Logan', NULL, 'Cindy@Logan.com', 0, 'images/avatars/021.jpg', 'Vanuatu', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$E//nA241tfqwVWtXv9bY3.8mgpG/NuBpe9Rd0P.B3pwlywpZZFkgu', NULL),
(367, 'Nadine', 'Dunn', NULL, 'Nadine@Dunn.com', 0, 'images/avatars/026.jpg', 'Belize', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$oCdprBarks2vgbccaT4Y.eqxiKOX3BOcEBcvoM3dQ4ApGQOJjg9h2', NULL),
(368, 'Chantal', 'Willis', NULL, 'Chantal@Willis.com', 0, 'images/avatars/014.jpg', 'Korea, Republic of', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$TqFGQxgRrrTbKIEWjDZGYefrIFuFRziA8XsFQ8guG02jhEmK919O6', NULL),
(369, 'Rebekah', 'Bradley', NULL, 'Rebekah@Bradley.com', 0, 'images/avatars/006.jpg', 'Botswana', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$jYWRTJ/2ZyfHokBaGHs5weLafzn5Ok9.Xbd8tawqKBynKru3f2sNO', NULL),
(370, 'William', 'Sloan', NULL, 'William@Sloan.com', 0, 'images/avatars/031.jpg', 'Poland', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$insBmBhF1J3IjrIPBukltONpKrHx04FuTvIRXDRkKrTx1ZEnqr.ZS', NULL),
(371, 'Bertha', 'Ayala', NULL, 'Bertha@Ayala.com', 0, 'images/avatars/020.jpg', 'Cuba', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$DCKc0.Vm7H9suGMAZs0gV.9sAC/LFhLDaKnWc4L3sSGiYjWLalTma', NULL),
(372, 'Youko', 'Prince', NULL, 'Youko@Prince.com', 0, 'images/avatars/023.jpg', 'Guatemala', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$HiIxTCmEamMgUMQFfx2EZ.Boolf1jEzAUtp7CqASiV/8nppYtFn/6', NULL),
(373, 'Akane', 'Whitney', NULL, 'Akane@Whitney.com', 0, 'images/avatars/014.jpg', 'Spain', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$zsBdLNIp6/ZNdA9VnwqGyeE73tkzvkWowIUbAul.AQ63AFWmYo39m', NULL),
(374, 'Marco', 'Fulton', NULL, 'Marco@Fulton.com', 0, 'images/avatars/034.jpg', 'Grenada', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$UalEtvur.WPTydMCr/QjgeNcS7lPmfELFRkm6VGkPjNRFubCik8g.', NULL),
(375, 'Valerie', 'Carlson', NULL, 'Valerie@Carlson.com', 0, 'images/avatars/028.jpg', 'Virgin Islands (British)', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$PMZ/ZgBZAwMF1dnSLhpExujyMBMTJaapPxZiim2NtDLGo4A3UpVue', NULL),
(376, 'Odette', 'Robertson', NULL, 'Odette@Robertson.com', 0, 'images/avatars/005.jpg', 'India', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$/ERZp47EyhfVSi6BfKa68O209LdEg3j23vmGRiqF3Y8Ze/JetRhMS', NULL),
(377, 'Ophelia', 'Navarro', NULL, 'Ophelia@Navarro.com', 0, 'images/avatars/024.jpg', 'Mauritania', '2018-09-11 12:00:34', '2018-09-11 12:00:34', '$2y$10$hNGeTH3bIeHQVXPlMyqkGuQT8hfKN.a9KLZMpr1QiIWhXQgGlm31C', NULL),
(378, 'Sam', 'Puckett', NULL, 'Sam@Puckett.com', 0, 'images/avatars/006.jpg', 'Congo', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$nUM3a/veVz6tkbUd2aHdMO9ylYnj5i/ra1vG/3NpE3MS9MszQRP2e', NULL),
(379, 'Ernesto', 'Huffman', NULL, 'Ernesto@Huffman.com', 0, 'images/avatars/017.jpg', 'United Kingdom', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$JAcBIu.GpePfnkz8BNixd.5yOagwpkqzVvci1ygUZj0ByJxjDEBIO', NULL),
(380, 'Wilma', 'Moon', NULL, 'Wilma@Moon.com', 0, 'images/avatars/036.jpg', 'New Caledonia', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$kAa3NQc1L.YI/1NHw2h4AOmA3qjTfDJxQFLZk2xbBrA3QO3perOkS', NULL),
(381, 'Oscar', 'Wall', NULL, 'Oscar@Wall.com', 0, 'images/avatars/010.jpg', 'Martinique', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$wmmxExm8ZyAHko8oPJI0YOqCTdb6RsNhHQpG5KHHgDBidgCycFV.y', NULL),
(382, 'Sachiko', 'Cain', NULL, 'Sachiko@Cain.com', 0, 'images/avatars/024.jpg', 'Guadeloupe', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$TUSgp9gW/14uv3yaGnBQaeLpjVgb5u631YdhCzA1cBnjJLKRvGjre', NULL),
(383, 'Sachiko', 'Lamb', NULL, 'Sachiko@Lamb.com', 0, 'images/avatars/039.jpg', 'Costa Rica', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$61pigqPRNX6zrR6spge0rOqagROjLzo7BeRMEKBVA6GWecbRJRTfi', NULL),
(384, 'Omar', 'Bradley', NULL, 'Omar@Bradley.com', 0, 'images/avatars/008.jpg', 'Saudi Arabia', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$HEPD10XId1q7ICAQ.GFrq.kMNuWuWyFLjJZJ9BTzPtsnovJYeGPiW', NULL),
(385, 'Beryl', 'Nelson', NULL, 'Beryl@Nelson.com', 0, 'images/avatars/024.jpg', 'Aruba', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$/SwKRYS8TYqasxJBuJohXO8Lzn6XscNMIMpaUWIhTLefYmuJiXALG', NULL),
(386, 'Florence', 'Benjamin', NULL, 'Florence@Benjamin.com', 0, 'images/avatars/040.jpg', 'Reunion', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$4lWaj6wRHp2LSeUUhmiVx.GiUNAqm9HTInFTPVlqZJpUTpT/WKAP6', NULL),
(387, 'Josephine', 'Burris', NULL, 'Josephine@Burris.com', 0, 'images/avatars/010.jpg', 'Yemen', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$7OXOmc/Jb6wwkY5vfi3eA.itGzVjmLddSEAhINDHvRPb7sOquam.q', NULL),
(388, 'Danielle', 'William', NULL, 'Danielle@William.com', 0, 'images/avatars/029.jpg', 'United Arab Emirates', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$vkdH2ia//hL9ifWSSlq/h.Z1BsZ/oTlNUmp7Lia3Xooo1k7E/irqW', NULL),
(389, 'Tomas', 'Hall', NULL, 'Tomas@Hall.com', 0, 'images/avatars/028.jpg', 'Panama', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$UplBmIpvwVbFCastyFSTIOGAkgyJyMoJMa4zDgvpxgEzuNKHYZP2m', NULL),
(390, 'J unko', 'Jimenez', NULL, 'J unko@Jimenez.com', 0, 'images/avatars/038.jpg', 'Moldova, Republic of', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$MqvHFZuvXfZ1949Op33acehJjaBvUtJIMkRvHoJ9QtMUw2FYcQNFm', NULL),
(391, 'Bonnie', 'Morse', NULL, 'Bonnie@Morse.com', 0, 'images/avatars/015.jpg', 'Turks and Caicos Islands', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$Fb2hclV4CpQHJ2jHLNaAMec5bdWBJNpyKE7RFVIJvv/phaohBXpHK', NULL),
(392, 'Virginie', 'Hudson', NULL, 'Virginie@Hudson.com', 0, 'images/avatars/029.jpg', 'Uganda', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$TP/E4vvYgsymIXgHkzBaLus5bfgbITiJngNmsfads2C4Tiz5T.wii', NULL),
(393, 'Michelle', 'Farrell', NULL, 'Michelle@Farrell.com', 0, 'images/avatars/025.jpg', 'Nicaragua', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$HLuFbDfibvQPtf7n3PgGRe1t6ZuLEShlw927m6jngLPq9zobLiIRC', NULL),
(394, 'Iris', 'Herrera', NULL, 'Iris@Herrera.com', 0, 'images/avatars/012.jpg', 'Turks and Caicos Islands', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$p11fGq1uGh7UWkQalAtXnOYNYf7CEl9wPstSsofLImXtHZDY7HuUq', NULL),
(395, 'Michael', 'Middleton', NULL, 'Michael@Middleton.com', 0, 'images/avatars/018.jpg', 'Gambia', '2018-09-11 12:00:35', '2018-09-11 12:00:35', '$2y$10$AhZPk.IeN8GESS/5hHtiOu9/vh5TN6mx.g0Q0VCS/GD//MahYHhqO', NULL),
(396, 'Sam', 'Lowery', NULL, 'Sam@Lowery.com', 0, 'images/avatars/014.jpg', 'Gambia', '2018-09-11 12:00:36', '2018-09-11 12:00:36', '$2y$10$vNYXKYyhH15hVTDKG0yNBe7mh7sObgwzSA0OTolkt/B8LA8TA4046', NULL),
(397, 'Stan', 'Walls', NULL, 'Stan@Walls.com', 0, 'images/avatars/018.jpg', 'Svalbard and Jan Mayen Islands', '2018-09-11 12:00:36', '2018-09-11 12:00:36', '$2y$10$CtntEQvqE29OYvFDhEsgJukbR6XApqFYHZgmuDwk1yk3piSAPWo6m', NULL),
(398, 'William', 'Chaney', NULL, 'William@Chaney.com', 0, 'images/avatars/017.jpg', 'Cyprus', '2018-09-11 12:00:36', '2018-09-11 12:00:36', '$2y$10$UT.2/.TeI9fAffHluevvPuDFhYpFHccuNDB5diCVpN7UxscJ/8oju', NULL),
(399, 'Nadine', 'Joyner', NULL, 'Nadine@Joyner.com', 0, 'images/avatars/033.jpg', 'St. Pierre and Miquelon', '2018-09-11 12:00:36', '2018-09-11 12:00:36', '$2y$10$Rb/Ufp3Iu1UOw9ai9rng..8deUdNxYRDvL6uNes9Er5VsejmCIpMG', NULL),
(400, 'Chantal', 'Peterson', NULL, 'Chantal@Peterson.com', 0, 'images/avatars/006.jpg', 'Uganda', '2018-09-11 12:00:36', '2018-09-11 12:00:36', '$2y$10$sD2F6unMr5mWwXsPldsNvOKJzzqwNjB0ume0zBwOc8OS9bqMZ4Xi2', NULL),
(401, 'Jeanne', 'Shepard', NULL, 'Jeanne@Shepard.com', 0, 'images/avatars/009.jpg', 'Sudan', '2018-09-11 12:00:36', '2018-09-11 12:00:36', '$2y$10$gNqDTUodyhNyFsVSC9CZiO9kaQe0dQouBWRQV8z3P9NGju6prgjS.', NULL),
(402, 'Bertha', 'Buck', NULL, 'Bertha@Buck.com', 0, 'images/avatars/006.jpg', 'Botswana', '2018-09-11 12:00:36', '2018-09-11 12:00:36', '$2y$10$beIbe5PsiCSLNBqmo2xz3uIQBis.2FU63WllF3BwAq4BqH.Mks2NC', NULL),
(403, 'Wendy', 'Wilcox', NULL, 'Wendy@Wilcox.com', 0, 'images/avatars/007.jpg', 'Venezuela', '2018-09-11 12:00:36', '2018-09-11 12:00:36', '$2y$10$1TYpcjHPa/bPnRxpnM5iZ.i/qK86fQFt5HGDi8hqXlvbQIn/isPEG', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_posts`
--

CREATE TABLE `user_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_posts`
--

INSERT INTO `user_posts` (`id`, `author`, `body`, `image`, `created_at`, `updated_at`) VALUES
(95, 214, 'Good Morning Folks!', 'images/status-uploaded/Kn3WY8egkzQGmacRuRWVXy7hcVnl78meJvQuzylW.jpeg', '2018-09-12 07:35:21', '2018-09-12 07:35:21'),
(98, 1, 'Prova', 'images/status-uploaded/MQjMmkdGFb3thIkXgMsYoiWXaDIJhrdsSjcVVJrK.jpeg', '2018-09-12 08:45:54', '2018-09-12 08:45:54'),
(99, 213, 'Prova post', 'images/status-uploaded/PDfHnWlLS3CoQTxEs60OEFNknThvPeUK7GIhtHjt.jpeg', '2018-09-12 08:47:30', '2018-09-12 08:47:30');

-- --------------------------------------------------------

--
-- Table structure for table `user_post_comments`
--

CREATE TABLE `user_post_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_post_comments_likes`
--

CREATE TABLE `user_post_comments_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_post_likes`
--

CREATE TABLE `user_post_likes` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_post_likes`
--

INSERT INTO `user_post_likes` (`id`, `post_id`, `user_id`) VALUES
(343, 99, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section` (`section`);

--
-- Indexes for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_from_fk` (`from_id`),
  ADD KEY `user_to_fk` (`to_id`);

--
-- Indexes for table `community_news`
--
ALTER TABLE `community_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `forum_posts_replies`
--
ALTER TABLE `forum_posts_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_replies_post_id_foreign` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `friends_relations`
--
ALTER TABLE `friends_relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`friend_id`),
  ADD KEY `friend_user_id` (`user_id`),
  ADD KEY `friend_2_user_id` (`friend_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups_members`
--
ALTER TABLE `groups_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_group_foreign` (`user_id`),
  ADD KEY `group_id_foreign` (`group_id`);

--
-- Indexes for table `groups_post`
--
ALTER TABLE `groups_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id_post_foreign` (`group_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_comments`
--
ALTER TABLE `news_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_comments_news_id_foreign` (`news_id`),
  ADD KEY `news_comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `news_comment_likes`
--
ALTER TABLE `news_comment_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_comment_likes_news_comments_id_foreign` (`news_comments_id`),
  ADD KEY `news_comment_likes_user_id_foreign` (`user_id`);

--
-- Indexes for table `news_likes`
--
ALTER TABLE `news_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_likes_news_id_foreign` (`news_id`),
  ADD KEY `news_likes_user_id_foreign` (`user_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_post_fk_nt` (`post_id`),
  ADD KEY `user_id_fk_nt` (`user_id`),
  ADD KEY `user_id_fk_nt_from` (`user_from`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `role_groups`
--
ALTER TABLE `role_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_groups_services`
--
ALTER TABLE `role_groups_services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groupId_2` (`groupId`,`serviceID`),
  ADD KEY `serviceID` (`serviceID`),
  ADD KEY `groupId` (`groupId`) USING BTREE;

--
-- Indexes for table `role_groups_user`
--
ALTER TABLE `role_groups_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_id` (`group_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surveys`
--
ALTER TABLE `surveys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_survey_foreign` (`author`);

--
-- Indexes for table `survey_answers`
--
ALTER TABLE `survey_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_answers_survey_option_id_foreign` (`survey_option_id`),
  ADD KEY `survey_answers_survey_id_foreign` (`survey_id`),
  ADD KEY `survey_answers_user_id_foreign` (`user_id`);

--
-- Indexes for table `survey_options`
--
ALTER TABLE `survey_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_options_survey_id_foreign` (`survey_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_posts`
--
ALTER TABLE `user_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_posts_author_foreign` (`author`);

--
-- Indexes for table `user_post_comments`
--
ALTER TABLE `user_post_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_comment` (`user_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `user_post_comments_likes`
--
ALTER TABLE `user_post_comments_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_like_author` (`user_id`),
  ADD KEY `comment_id` (`comment_id`);

--
-- Indexes for table `user_post_likes`
--
ALTER TABLE `user_post_likes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id` (`post_id`,`user_id`),
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `chat_messages`
--
ALTER TABLE `chat_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=683;

--
-- AUTO_INCREMENT for table `community_news`
--
ALTER TABLE `community_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `forum_posts`
--
ALTER TABLE `forum_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `forum_posts_replies`
--
ALTER TABLE `forum_posts_replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `friends_relations`
--
ALTER TABLE `friends_relations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups_members`
--
ALTER TABLE `groups_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `news_comments`
--
ALTER TABLE `news_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news_comment_likes`
--
ALTER TABLE `news_comment_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_likes`
--
ALTER TABLE `news_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=773;

--
-- AUTO_INCREMENT for table `role_groups`
--
ALTER TABLE `role_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `role_groups_services`
--
ALTER TABLE `role_groups_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `role_groups_user`
--
ALTER TABLE `role_groups_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `surveys`
--
ALTER TABLE `surveys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `survey_answers`
--
ALTER TABLE `survey_answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `survey_options`
--
ALTER TABLE `survey_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=405;

--
-- AUTO_INCREMENT for table `user_posts`
--
ALTER TABLE `user_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `user_post_comments`
--
ALTER TABLE `user_post_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `user_post_comments_likes`
--
ALTER TABLE `user_post_comments_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_post_likes`
--
ALTER TABLE `user_post_likes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=344;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `sectionID` FOREIGN KEY (`section`) REFERENCES `section` (`id`);

--
-- Constraints for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD CONSTRAINT `user_from_fk` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_to_fk` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `forum_posts_replies`
--
ALTER TABLE `forum_posts_replies`
  ADD CONSTRAINT `posts_replies_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `forum_posts` (`id`),
  ADD CONSTRAINT `user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `friends_relations`
--
ALTER TABLE `friends_relations`
  ADD CONSTRAINT `friend_2_user_id` FOREIGN KEY (`friend_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `friend_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups_members`
--
ALTER TABLE `groups_members`
  ADD CONSTRAINT `group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `user_id_group_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `groups_post`
--
ALTER TABLE `groups_post`
  ADD CONSTRAINT `group_id_post_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `groups_post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `groups_members` (`id`);

--
-- Constraints for table `news_comments`
--
ALTER TABLE `news_comments`
  ADD CONSTRAINT `news_comments_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `community_news` (`id`),
  ADD CONSTRAINT `news_comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `news_comment_likes`
--
ALTER TABLE `news_comment_likes`
  ADD CONSTRAINT `news_comment_likes_news_comments_id_foreign` FOREIGN KEY (`news_comments_id`) REFERENCES `news_comments` (`id`),
  ADD CONSTRAINT `news_comment_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `news_likes`
--
ALTER TABLE `news_likes`
  ADD CONSTRAINT `news_likes_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `community_news` (`id`),
  ADD CONSTRAINT `news_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `user_id_fk_nt` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id_fk_nt_from` FOREIGN KEY (`user_from`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_post_fk_nt` FOREIGN KEY (`post_id`) REFERENCES `user_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD CONSTRAINT `fk_email` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_groups_services`
--
ALTER TABLE `role_groups_services`
  ADD CONSTRAINT `fk_groupid` FOREIGN KEY (`groupId`) REFERENCES `role_groups` (`id`),
  ADD CONSTRAINT `fk_serviceid` FOREIGN KEY (`serviceID`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_groups_user`
--
ALTER TABLE `role_groups_user`
  ADD CONSTRAINT `fk_group_id` FOREIGN KEY (`group_id`) REFERENCES `role_groups` (`id`),
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `surveys`
--
ALTER TABLE `surveys`
  ADD CONSTRAINT `author_survey_foreign` FOREIGN KEY (`author`) REFERENCES `users` (`id`);

--
-- Constraints for table `survey_answers`
--
ALTER TABLE `survey_answers`
  ADD CONSTRAINT `survey_answers_survey_id_foreign` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`),
  ADD CONSTRAINT `survey_answers_survey_option_id_foreign` FOREIGN KEY (`survey_option_id`) REFERENCES `survey_options` (`id`),
  ADD CONSTRAINT `survey_answers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `survey_options`
--
ALTER TABLE `survey_options`
  ADD CONSTRAINT `survey_options_ibfk_1` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`);

--
-- Constraints for table `user_posts`
--
ALTER TABLE `user_posts`
  ADD CONSTRAINT `user_posts_author_foreign` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_post_comments`
--
ALTER TABLE `user_post_comments`
  ADD CONSTRAINT `post_id` FOREIGN KEY (`post_id`) REFERENCES `user_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id_author` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_post_comments_likes`
--
ALTER TABLE `user_post_comments_likes`
  ADD CONSTRAINT `comment_like_author` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_post_likes`
--
ALTER TABLE `user_post_likes`
  ADD CONSTRAINT `author_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_post_id` FOREIGN KEY (`post_id`) REFERENCES `user_posts` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
