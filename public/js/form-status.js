$(document).ready(function () {

    
    var image = '';
    var postResponse;

    /** listen for changes in chat class (open/close) */
    observeChatClass();

    /** Uploads a temp image and shows it in post content box */
    $("#browsePhoto").change(function () {

        $.ajax({
            url:'/create/post/preloadImage',
            data:new FormData($("#upload_form")[0]),
            async:true,
            type:'post',
            processData: false,
            contentType: false,
            success:function(response){
                
                image = response;
                $('#post_content').append('<center id="preloaded-image"><img style="width: 100%;padding: 29px;max-width:300px" src='+'"/storage/'+response+'"></center>');
            },
          });
    });

    /** Insert new comment on post and shows below it **/ 
    $(".comment-text").keypress(function(e) {

        var postId = $(this).attr('post-id');
        
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();

            var data = $('#comment-form-'+postId).serializeArray(); // convert form to array

            data.push({name: "post-id",value: postId});

            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/comment/post',
                data: $.param(data),
                success:function(response){
                    $('#comment-list-'+postId).append(response);
                    $('#comment-form-'+postId).trigger('reset');
                   
                    if(!$('#comment-list-'+postId).is(':visible')){
                        $('#comment-list-'+postId).toggle();
                    }
                },
            });

        }
    });

    /** Post a new status and loads it at the top of the feed **/ 
    $('#post-status').click(function(){
        
        if(image!=null||$('post_body').val()!=""){
            //var postResponse = $.post('/status/post', $('#status-form').serialize() + "&image=" + image);

            var data = $('#status-form').serializeArray(); // convert form to array
            data.push({name: "image", value: image});
            $.ajax({
                type: 'POST',
                url: '/status/post',
                data: $.param(data),
                success:function(response){
                    $('#newsfeed-items-grid').prepend(response);
                },
            });

            $('#status-form').trigger('reset');
            $('#preloaded-image').remove();
            $('#upload-form').trigger('reset');

        }
        image = '';
    });

    /** Likes or dislikes a post */
    $('.like-post').click(function(){
        
        var pid = $(this).attr('post-id');

        $('#like-top-'+pid).toggleClass("iconClicked");
        $('#like-bottom-'+pid).toggleClass("svgClicked");
        
        if(($('#like-bottom-'+pid).hasClass("iconClicked")||$('#like-top-'+pid).hasClass("iconClicked"))){
            $.ajax({
                type: "POST",
                url: '/post/like',
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                data: {
                        'post-id': pid
                    },
                success:function(response){
                    $('#bottom-bar-'+pid).html(response);
                }
            });
        }
        else{
            $.ajax({
                type: "DELETE",
                url: '/post/like',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                data: {
                        'post-id': pid
                    },
                success:function(response){
                    $('#bottom-bar-'+pid).html(response);
                }
            });
        }

    });

    /* Check for new notifications and append them to list */

    setInterval(function(){

        /* All Notifications */
        $.ajax({
            type: "POST",
            url: '/notification/all',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                
                $('#header-notification-list').html(response['thunder_notifications']);
                $('#notification-counter').html($('#notification-amount').val());
                
                if($('#notification-amount').val() == '0' || $('#notification-amount').val() == '' ){
                    $('#notification-counter').hide();
                };
                    if($('#notification-amount').val() != '0'){
                        $('#notification-counter').html($('#notification-amount').val());
                        $('#notification-counter').show();    
                    }

                $('#message-notification-list').html(response['message_notifications']);
                $('#message-notification-counter').html($('#message-notification-amount').val());
                
                if($('#message-notification-amount').val() == '0' || $('#message-notification-amount').val() == '' ){
                    $('#message-notification-counter').hide();
                };
                if($('#message-notification-amount').val() != '0'){
                    $('#message-notification-counter').html($('#message-notification-amount').val());
                    $('#message-notification-counter').show();    
                }
                
                $('#friends-notification-list').html(response['friends_notifications']);
                $('#friends-notification-counter').html($('#friends-notification-amount').val());
                
                if($('#friends-notification-amount').val() == '0' || $('#friends-notification-amount').val() == '' ){
                    $('#friends-notification-counter').hide();
                };
                if($('#friends-notification-amount').val() != '0'){
                    $('#friends-notification-counter').html($('#friends-notification-amount').val());
                    $('#friends-notification-counter').show();    
                }
            }
        });


    }, 8000)

    /* Set all notification in window to read */
    
    $('.read-all-this').click(function(){
        var el = $(this);
        var type = $(this).attr('nt_type');
        $.ajax({
            type: "POST",
            url: '/notification/read?type='+type,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                console.log(type);
                if(type == "thunder"){
                    $('#header-notification-list').html("");
                    $('#notification-counter').hide();
                }
                if(type == "messages"){
                    $('#message-notification-list').html("");
                    $('#message-notification-counter').hide();
                }
                if(type == "friends"){
                    $('#friends-notification-list').html("");
                    $('#friends-notification-counter').hide();
                }
            }

        });

    });

    $('.message-notification').click(function(){

        var friend_id = $(this).attr("friend_id");
        $('#chat-friend-{{$mn->user_from_id}}').trigger('click');

    })

    /* Opens new chat for selected users */

    $('.chat-user').click(function openChat(){

        var friend = $(this).attr('friend-id');

        $.ajax({
            type: "POST",
            url: '/chat/user/'+$(this).attr('friend-id'),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                if (!$('#sub-container-'+friend).length > 0) {
                  
                    $('#chat-container').append(response);
                    $('#current-scroll-bar-'+friend).perfectScrollbar({wheelPropagation:false});
                    $('#current-scroll-bar-'+friend).scrollTop( $( '#current-scroll-bar-'+friend ).prop( "scrollHeight" ) );
                    
                    if($('#chat-sidebar').hasClass('open')){
                        $('#chat-container').css('right','160px');
                    }
                    
                    /*Register chat header events */
                    
                    /* Register click event to Close selected chat */
                    $('#close-chat-'+friend).click(function closeChat(){
                        $('#sub-container-'+$(this).attr('friend_id')).remove();
                    });
                    
                    $('#modal-header-'+friend).click(function (){
                        var friend_id = $(this).attr('friend_id');
                        $('#chat-window-'+friend_id).toggleClass('cs-closed-chat');
                    });
                    
                    /** Send new message event **/ 
                    $("#chat-form-"+friend).keypress(function(e) {
                        if(e.keyCode == 13 && !e.shiftKey){
                            e.preventDefault();
                            console.log($("#chat-text").val());
                            if( $(this).val("#chat-text")!=""){
                                var data = $("#chat-form-"+friend).serializeArray();
                                $.ajax({
                                    type: 'POST',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    url: '/chat/message/user/'+friend,
                                    data: $.param(data),
                                    success:function(response){
                                        $('#current-message-list-'+friend).append(response);
                                        $('#current-scroll-bar-'+friend).scrollTop( $( '#current-scroll-bar-'+friend ).prop( "scrollHeight" ) );
                                        $("#chat-form-"+friend).trigger("reset");
                                        $("#chat-form-"+friend).children().toggleClass('is-empty');
                                    },
                                });
                            }
                        }
                    });

                    /* check new messages and append event for open chat only */
                    setInterval(function(){ 
                        $.ajax({
                            type: "POST",
                            url: '/message/new/get/'+friend,
                            data: {
                               'last_message_time' : $("#current-message-list-"+friend).find('.entry-date').last().text()
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            },
                            success:function(response){
                                if(response){
                                    $('#current-message-list-'+friend).append(response);
                                    $('#current-scroll-bar-'+friend).scrollTop( $( '#current-scroll-bar-'+friend ).prop( "scrollHeight" ) );
                                    var base_url =  window.location.origin;
                                   /*  var audio = new Audio(base_url+'/sounds/appointed.mp3');
                                    audio.play(); */
                                }
                            }
                        });
                    },1500);
                }
                else{
                    $('#chat-window-'+friend).toggleClass('cs-closed-chat');
                }    
            }

        });

    });
    
    /* listening for chat windows changes */
    function observeChatClass(){
        var $div = $("#chat-sidebar");
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.attributeName === "class") {
                    
                    if($('#chat-sidebar').hasClass('open')){
                        $('#chat-container').css('right','160px');
                    }
                    else{
                        $('#chat-container').css('right','0px');
                    }
                }
            });
        });
        if($div[0]){
            observer.observe($div[0], {
                attributes: true
            })
        };
    };

     /** Uploads a temp image and shows it in post content box */
     $("#change-photo").change(function () {

        $.ajax({
            url:'/profile/settings/change/user-photo',
            data:new FormData($("#upload_form")[0]),
            async:true,
            type:'post',
            processData: false,
            contentType: false,
            success:function(response){
                
                image = response;
                $('#user-image-container').html('<img src='+'"/storage/'+response+'"class="avatar" style="width: 244px; height:244px;" alt="author">');
                location.reload();

            },
          });
    });

    /* Validation for password_update form */
    $("#submit-pwd-form").click(function(){

        if($('#new-pwd').val() == $('#confirm-new-pwd').val()){
            if($('#new-pwd').hasClass('has-error')){
                $("#new-pwd").parent().toggleClass('has-error');
                $("#confirm-new-pwd").parent().toggleClass('has-error');
            }
            
            $('#update-password').submit();

        }
        else{
            $("#new-pwd").parent().toggleClass('has-error');
            $("#confirm-new-pwd").parent().toggleClass('has-error');
            $("#new-pwd").parent().toggleClass('is-invalid');
            $("#confirm-new-pwd").parent().toggleClass('is-invalid');
            $(".error-span").html('<b style="color:red">Passwords don\'t match!</b>');
        }
    });

    /* Accept Friend Request */
    $('#accept-friend-request').click(function(){
        var span = $(this);
        $.ajax({
            url:'/profile/settings/accept/request/'+$(this).attr('friend_id'),
            type:'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                
                $(span).replaceWith("<span style='color: grey;font-size: 15px;'>Request Accepted!</span>");
            },
          });
    });

    /* Send Friend Request */
    $('.send-friend-request').click(function(){
        var span = $(this);
        console.log(span);
        $.ajax({
            url:'/profile/settings/send/request/'+$(this).attr('friend_id'),
            type:'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                
                $(span).replaceWith("<span style='color: grey;font-size: 15px;'>Request Sent!</span>");
            },
            });
    });

    /* Send Friend Request */
    $('.send-friend-request-short').click(function(){
        var span = $(this);
        console.log(span);
        $.ajax({
            url:'/profile/settings/send/request/'+$(this).attr('friend_id'),
            type:'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                
                $(span).replaceWith("<span style='color:#38a9ff;font-size: 15px;'><b>Sent!</b></span>");
            },
            });
    });

    /* Open a conversation in profile messenger */
    $('.open-conversation').click(function(){
        var friend = $(this).attr('friend_id');
        var name = $(this).find(".h6").text();

        $.ajax({
            url:'/profile/messenger/get/conversation/'+friend,
            type:'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                
                $("#conversation-container").html(response);
                $("#conv-scroll").scrollTop( $('#conv-scroll').prop( "scrollHeight" ) );
                $('#conversation_title').text(name);
                $('#friend_id_input').val(friend);
            },
            });
    })

    /* Send message in  profile messenger */
    $('#sendMessage').click(function(){
        var friend =  $('#friend_id_input').val();
        var data = $('#messenger-form').serializeArray();
        $.ajax({
            url:'/profile/messenger/send/message/'+friend,
            type:'post',
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                
                $("#conversation-container").append(response);
                $("#conv-scroll").scrollTop( $('#conv-scroll').prop( "scrollHeight" ) );
                $('#messenger-form').trigger('reset');
            },
            });
    })
    
    /* filtering enter key without shift */
    $("#messenger-text-area").keypress(function(e) {

            var postId = $(this).attr('post-id');
        
            if(e.keyCode == 13 && !e.shiftKey){
                e.preventDefault();
                $('#sendMessage').trigger('click');
            }
        }
    );

    /* Submit registration */
    $('#submit-registration').click(function(){
        console.log($('#accept-checkbox').is(":checked"));
        if($('#accept-checkbox').is(":checked")) $("#submit-registration-hidden").click();
        else $('.error-accept').html('<b>You must accept our rules.</b>');
    })

    /* Go to next page */
    $('#page-forward').click(function(){
        var urlParams = new URLSearchParams(window.location.search);
        if(urlParams.get('page')) var page = parseInt(urlParams.get('page'))+1;
        else var page = 0;
        window.location.replace(window.location.pathname+"?page="+page);
    });

     /* Go to previous page */
     $('#page-backward').click(function(){
        var urlParams = new URLSearchParams(window.location.search);
        if(urlParams.get('page')) var page = parseInt(urlParams.get('page'))-1;
        else var page = 0;
        window.location.replace(window.location.pathname+"?page="+page);
    });

    /* Go to specific page */
    $('.goto-page').click(function(){
        var page = parseInt($(this).text());
        console.log(page);
        window.location.replace(window.location.pathname+"?page="+page);
    });

    /* Load more posts */
    $('#load-more-button').click(function(){
        var page = parseInt($('#feed-page').val())+1;
        $.ajax({
            url:'/feed/load-more/'+page,
            type:'get',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success:function(response){
                
                $("#newsfeed-items-grid").append(response);
                $('#feed-page').val(page);
                
            },
        });
    })
});