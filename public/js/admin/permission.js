
$(document).ready(function () {

    /* Load all checkboxes with AJAX call */
    var temp =$.ajax({
        url : "/admin/settings/permissions/init",
        type : "POST",
        data: {
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache : false,
        success: function(response) {
           res = temp.responseJSON;
           for (const key in res) {
                for(const s in (res[key])){
                   
                    $('#'+key+'_'+res[key][s]).prop('checked',true);
                }
        } 
        },
        error: function(response) {
            alert('An error occurred.');
        }
    });


    
    /* This enables select all and deselect 
     * all checkboxes on permission management page 
    */
    $('.selectAll').click(function () {

        var x = '.'+$(this).attr('ctrl');

        if($(this).prop('checked')){
            $(x).prop('checked', true);
        }
        else{
            $(x).prop('checked', false);
        }

    });
    
    /* Sending permissions changes through AJAX */
    $('#submit').click(function(){
        var j=1;
        var add=[];
        var remove=[];

            while($('.allow_'+j).prop('type')!=undefined){
                
                add[j]=[];
                remove[j]=[];

                for (i=0;i<$('.allow_'+j).length;i++){
                    
                    if($($('.allow_'+j)[i]).prop('checked')){
                        add[j].push($($('.allow_'+j)[i]).attr('service'));
                    }
                    else{
                        remove[j].push($($('.allow_'+j)[i]).attr('service'));
                    }                    
                }
                j++;
            }
        
            $.ajax({
                url : "/admin/settings/permissions",
                type : "POST",
                data: {
                  'permission_add': add,
                  'permission_remove': remove 
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
    })
})



