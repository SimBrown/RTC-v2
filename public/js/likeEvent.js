function likeEvent(){
    /** Likes or dislikes a post */
    $('.like-post').click(function(){
    
        var pid = $(this).attr('post-id');

        $('#like-top-'+pid).toggleClass("iconClicked");
        $('#like-bottom-'+pid).toggleClass("svgClicked");
        
        if(($('#like-top-'+pid).hasClass("iconClicked")||$('#like-top-'+pid).hasClass("iconClicked"))){
            $.ajax({
                type: "POST",
                url: '/post/like',
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                data: {
                        'post-id': pid
                    },
                success:function(response){
                    $('#bottom-bar-'+pid).html(response);
                }
            });
        }
        else{
            $.ajax({
                type: "DELETE",
                url: '/post/like',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                data: {
                        'post-id': pid
                    },
                success:function(response){
                    $('#bottom-bar-'+pid).html(response);
                }
            });
        }

    });
}