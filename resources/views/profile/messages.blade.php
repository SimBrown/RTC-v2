@extends('profile.settings.settings-layout')
@section('settings-content')
<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
<div class="ui-block">
    <div class="ui-block-title">
        <h6 class="title">Chat / Messages</h6>
        <a href="#" class="more"><svg class="olymp-three-dots-icon">
                <use{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use>
            </svg></a>
    </div>

    <div class="row">
        <div class="col col-xl-5 col-lg-6 col-md-12 col-sm-12  padding-r-0">


            <!-- Notification List Chat Messages -->
            <div class="mCustomScrollbar ps ps--theme_default ps--active-y" style="max-height: 450px;" data-mcs-theme="dark" data-ps-id="">
                <ul class="notification-list chat-message">          
                @foreach($contact as $c)   
                    <li class="open-conversation" friend_id="{{$c->user_id}}">
                        <div class="author-thumb">
                            <img src="{{asset('/storage/'.$c->image)}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="/profile/user/{{$c->user_id}}" class="h6 notification-friend">{{$c->name." ".$c->surname}}</a>
                            <span class="chat-message-item">{{$c->body}}</span>
                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">{{$c->time}}</time></span>
                        </div>
                       
                    </li>
                @endforeach
                </ul>
                <div class="ps__scrollbar-y-rail" style="top: 0px; height: 450px; right: 0px;">
                    <div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 441px;"></div>
                </div>
            </div>

            <!-- ... end Notification List Chat Messages -->

        </div>

        <div  class="col col-xl-7 col-lg-6 col-md-12 col-sm-12  padding-l-0">
            <div class="chat-field">
                <div class="ui-block-title">
                    <h6 class="title" id="conversation_title">Elaine Dreyfuss</h6>
                    <a href="#" class="more"><svg class="olymp-three-dots-icon"><use{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg></a>
                </div>
                <div id="conv-scroll" class="mCustomScrollbar ps ps--theme_default ps--active-y" data-mcs-theme="dark">
                    <ul id="conversation-container" class="notification-list chat-message chat-message-field"></ul>
                </div>

                <form id="messenger-form" autocomplete="off">
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">Write your reply here...</label>
                        <textarea id="messenger-text-area" name="message-body" class="form-control" placeholder=""></textarea>
                        <span class="material-input"></span>
                        <input id="friend_id_input" type="hidden">
                    </div>
            
                    <div class="add-options-message">
                        <button type="button" id="sendMessage" class="btn btn-primary btn-sm">Send Message</button>
                    </div>
                </form>
            </div>    
        </div>
    </div>

</div>
</div>
@endsection
