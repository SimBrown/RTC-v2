@extends('layouts.app')

@section('content')
<div class="main-header">
    <div class="content-bg-wrap bg-group"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>Find new Friends</h1>
                    <p>Welcome to your friends groups! Do you wanna know what your close friends have been up to?
                        Groups
                        will let you easily manage your friends and put the into categories so when you enter you’ll
                        only
                        see a newsfeed of those friends that you placed inside the group. Just click on the plus button
                        below and start now!</p>
                </div>
            </div>
        </div>
    </div>

    
</div>
<div class="header-spacer"></div>
<div class="container">
    <div class="row">
        @foreach($find_friends as $r)
        <div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="ui-block">

                <!-- Friend Item -->

                <div class="friend-item">
                    <div class="friend-header-thumb">
                        <img src="{{asset('/storage/'.$r->image)}}" alt="friend">
                    </div>

                    <div class="friend-item-content">
                        <div class="friend-avatar">
                            <div class="author-thumb">
                                <img src="{{asset('/storage/'.$r->image)}}" alt="author">
                            </div>
                            <div class="author-content">
                                <a href="/profile/user/{{$r->id}}" class="h5 author-name">{{$r->name." ".$r->surname}}</a>
                                <div class="country">{{$r->address}}</div>
                            </div>
                        </div>

                        <div class="swiper-container swiper-swiper-unique-id-0 initialized swiper-container-horizontal"
                            data-slide="fade" id="swiper-unique-id-0">
                            <div class="swiper-wrapper" style="width: 972px; transform: translate3d(-243px, 0px, 0px); transition-duration: 0ms;">
                                <div class="swiper-slide swiper-slide-duplicate swiper-slide-prev swiper-slide-duplicate-next"
                                    data-swiper-slide-index="1" style="width: 243px;">
                                    <p class="friend-about" data-swiper-parallax="-500" style="transform: translate3d(-500px, 0px, 0px); transition-duration: 0ms;">
                                        Hi!, I’m Marina and I’m a Community Manager for “Gametube”. Gamer and full-time
                                        mother.
                                    </p>

                                    <div class="friend-since" data-swiper-parallax="-100" style="transform: translate3d(-100px, 0px, 0px); transition-duration: 0ms;">
                                        <span>Friends Since:</span>
                                        <div class="h6">December 2014</div>
                                    </div>
                                </div>
                                <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 243px;">
                                    <div class="friend-count" data-swiper-parallax="-500" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">52</div>
                                            <div class="title">Friends</div>
                                        </a>
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">240</div>
                                            <div class="title">Photos</div>
                                        </a>
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">16</div>
                                            <div class="title">Videos</div>
                                        </a>
                                    </div>
                                    <div class="control-block-button" data-swiper-parallax="-100" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                                        <span class="notification-icon">
                                                <div friend_id="{{$r->id}}" style="cursor:pointer" class="send-friend-request accept-request">
                                                    <span class="icon-add">
                                                        <svg class="olymp-happy-face-icon">
                                                            <use xlink:href="icons/icons.svg#olymp-happy-face-icon"></use>
                                                        </svg>
                                                    </span>
                                                    {{\App\Http\Controllers\HomeController::checkFriendship($r->id)}}
                                                </div>
                                        </span>
                                    </div>
                                </div>

                                <div class="swiper-slide swiper-slide-next swiper-slide-duplicate-prev"
                                    data-swiper-slide-index="1" style="width: 243px;">
                                    <p class="friend-about" data-swiper-parallax="-500" style="transform: translate3d(500px, 0px, 0px); transition-duration: 0ms;">
                                        Hi!, I’m Marina and I’m a Community Manager for “Gametube”. Gamer and full-time
                                        mother.
                                    </p>

                                    <div class="friend-since" data-swiper-parallax="-100" style="transform: translate3d(100px, 0px, 0px); transition-duration: 0ms;">
                                        <span>Friends Since:</span>
                                        <div class="h6">December 2014</div>
                                    </div>
                                </div>
                                <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active"
                                    data-swiper-slide-index="0" style="width: 243px;">
                                    <div class="friend-count" data-swiper-parallax="-500" style="transform: translate3d(500px, 0px, 0px); transition-duration: 0ms;">
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">52</div>
                                            <div class="title">Friends</div>
                                        </a>
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">240</div>
                                            <div class="title">Photos</div>
                                        </a>
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">16</div>
                                            <div class="title">Videos</div>
                                        </a>
                                    </div>
                                    <div class="control-block-button" data-swiper-parallax="-100" style="transform: translate3d(100px, 0px, 0px); transition-duration: 0ms;">
                                        <a href="#" class="btn btn-control bg-blue">
                                            <svg class="olymp-happy-face-icon">
                                                <use{{asset('svg-icons/sprites/icons.svg')}}#olymp-happy-face-icon"></use>
                                            </svg>
                                        </a>

                                        <a href="#" class="btn btn-control bg-purple">
                                            <svg class="olymp-chat---messages-icon">
                                                <use{{asset('svg-icons/sprites/icons.svg')}}#olymp-chat---messages-icon"></use>
                                            </svg>
                                        </a>

                                    </div>
                                </div>
                            </div>

                            <!-- If we need pagination -->
                            <div class="swiper-pagination pagination-swiper-unique-id-0 swiper-pagination-clickable swiper-pagination-bullets"><span
                                    class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>
                        </div>
                    </div>
                </div>

                <!-- ... end Friend Item -->
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection