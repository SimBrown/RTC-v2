<li>
    <div class="author-thumb">
        <img src="{{asset('/storage/'.Auth::User()->image)}}" alt="author">
    </div>
    <div class="notification-event" style="width:100%;">
        <a href="/profile/user/{{$c->user_id}}" class="h6 notification-friend">{{Auth::User()->name." ".Auth::User()->surname}}</a>
        <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">{{$c->time}}</time></span>
        <span class="chat-message-item" style="width:100%;font-size: 16px">{{$c->body}}</span>
    </div>
</li>