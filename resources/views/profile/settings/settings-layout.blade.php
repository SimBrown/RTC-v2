@extends('layouts.app')
@section('content')

<div class="main-header" style="margin-bottom:30px;">
	<div class="content-bg-wrap bg-account"></div>
	<div class="container">
		<div class="row">
			<div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
				<div class="main-header-content">
					<h1>Your Account Dashboard</h1>
					<p>Welcome to your account dashboard! Here you’ll find everything you need to change your profile
	information, settings, read notifications and requests, view your latest messages, change your pasword and much
	more! Also you can create or manage your own favourite page, have fun!</p>
				</div>
			</div>
		</div>
	</div>
	<img class="img-bottom" src="img/account-bottom.png" alt="friends">
</div>

<div class="container">
	<div class="row">
		
		@yield('settings-content')

		<div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">
			<div class="ui-block">

				
				
				<!-- Your Profile  -->
				
				<div class="your-profile">
					<div class="ui-block-title ui-block-title-small">
						<h6 class="title">Your PROFILE</h6>
					</div>
				
					<div id="accordion" role="tablist" aria-multiselectable="true">
						<div class="card">
							<div class="card-header" role="tab" id="headingOne">
								<h6 class="mb-0">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Profile Settings
										<svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-dropdown-arrow-icon"></use></svg>
									</a>
								</h6>
							</div>
				
							<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
								<ul class="your-profile-menu">
									<li>
										<a href="{{route('profile.informations')}}">Personal Information</a>
									</li>
									<li>
										<a href="{{route('profile.change-password')}}">Change Password</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				
					<div class="ui-block-title">
						<a href="{{route('profile.notifications')}}?page=1" class="h6 title">Notifications</a>
					</div>
					<div class="ui-block-title">
						<a href="{{route('profile.messages')}}" class="h6 title">Chat / Messages</a>
					</div>
					<div class="ui-block-title">
					<a href="{{route('profile.friend_requests')}}" class="h6 title">Friend Requests</a>
					</div>
				</div>
				
				<!-- ... end Your Profile  -->
				

			</div>
		</div>
	</div>
</div>

@endsection