@extends('profile.settings.settings-layout')
@section('settings-content')
<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
    <div class="ui-block">
        <div class="ui-block-title">
            <h6 class="title">Personal Information</h6>
        </div>
        <div class="ui-block-content">
            <form class="need-validation" id="update-password" method="POST" action="{{route('profile.change-password')}}"
                autocomplete="off">
                @csrf
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group label-floating" style="margin-bottom:0px">
                            <label class="control-label">New Password</label>
                            <input id="new-pwd" class="form-control" required="required" name="new_pwd" placeholder=""
                                type="password" value="">
                        </div>
                        <span class="error-span" style="margin-top:5px;display:inline-block"></span>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group label-floating" style="margin-bottom:0px">
                            <label class="control-label">Confirm Password</label>
                            <input id="confirm-new-pwd" class="form-control" required="required" name="confirm_new_pwd"
                                placeholder="" type="password" value="">
                        </div>
                        <span class="error-span" style="margin-top:5px;display:inline-block"></span>
                    </div>

                    <div style="width:100%;border-bottom:2px solid #efefef;margin:22px 40px 30px 40px;"></div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        @if(isset($error))
                        <div class="form-group label-floating has-error is-invalid" style="margin-bottom:0px">
                            <label class="control-label">Current Password</label>
                            <input id="current-pwd" class="form-control" required="required" name="current_pwd"
                                placeholder="" type="password" value="">
                        </div>
                            <span class="error-span" style="color:red;margin-top:5px;display:inline-block"><b>Wrong Password!</b></span>
                        @else
                        <div class="form-group label-floating">
                            <label class="control-label">Current Password</label>
                            <input id="current-pwd" class="form-control" required="required" name="current_pwd"
                                placeholder="" type="password" value="">
                        </div>
                        @endif
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <button type="button" id="submit-pwd-form" class="btn btn-primary btn-lg full-width">Save all
                            Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<form hidden enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="">
    <input name="image" id="change-photo" type="file">
    @csrf
</form>
@endsection