@extends('profile.settings.settings-layout')
@section('settings-content')
<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
	<div class="ui-block">
		<div class="ui-block-title">
			<h6 class="title">Personal Information</h6>
		</div>
		<div class="ui-block-content">
			<form class="need-validation" method="POST" action="{{route('update.userInfo')}}" autocomplete="off">
				@csrf
				<div class="row">

					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group label-floating">
							<label class="control-label">First Name</label>
							<input class="form-control" required="required" name="first_name" placeholder="" type="text" value="{{Auth::User()->name}}">
						</div>

						<div class="form-group label-floating">
							<label class="control-label">Your Email</label>
							<input class="form-control" required="required" type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="{{Auth::User()->email}}">
						</div>

					</div>

					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group label-floating">
							<label class="control-label">Last Name</label>
							<input class="form-control" required="required" name="last_name" placeholder="" type="text" value="{{Auth::User()->surname}}">
						</div>

						<div class="form-group date-time-picker label-floating">
							<label class="control-label">Your Birthday</label>
							<input name="datetimepicker" required="required" value="{{ \Carbon\Carbon::parse(Auth::User()->birthdate)->format('d/m/Y')}}"  pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" />
							<span class="input-group-addon">
								<svg class="olymp-month-calendar-icon icon">
									<use xlink:href="icons/icons.svg#olymp-month-calendar-icon"></use>
								</svg>
							</span>
						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group label-floating is-select">
							<label class="control-label">Your Country</label>
							<input class="form-control" required="required" name="address" placeholder="" type="text" value="{{Auth::User()->address}}">
						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						
					</div>
					
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<button class="btn btn-primary btn-lg full-width">Save all Changes</button>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display:flex;margin-bottom:20px;">
						<div class="author-thumb" style="width:76%;margin:0 auto">
							<label id="user-image-container" for="change-photo"><img src="{{ asset('storage/' . Auth::user()->image) }}" class="avatar" style="width: 244px; height:244px;" alt="author"></label>
							<label class="btn btn-secondary btn-lg" style="margin:0;margin-left: 7%;" for="change-photo">Change Picture</label>
						</div>
					</div>

				</div>
			</form>
		</div>
	</div>
</div>

<form hidden enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="">
	<input name="image" id="change-photo" type="file">
	@csrf
</form>
@endsection