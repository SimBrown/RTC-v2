@extends('profile.settings.settings-layout')
@section('settings-content')
<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Notifications</h6>
					<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg></a>
				</div>

				
				<!-- Notification List -->
				
				<ul class="notification-list">
					@include('profile.settings.notification-block')
				</ul>
				
				<!-- ... end Notification List -->

			</div>

			
			<!-- Pagination -->
			
			<nav aria-label="Page navigation">
				<ul class="pagination justify-content-center">
					@if(isset($_GET['page']) && $_GET['page']>1)
					<li id="page-backward" class="page-item">
						<a class="page-link" tabindex="-1">Previous</a>
					</li>
					@else
					<li class="page-item disabled" style="cursor:no-drop;">
						<a class="page-link" tabindex="-1">Previous</a>
					</li>
					@endif
					@if(isset($all_notification)&& count($all_notification)>0)
						@for ($i = 1; $i <= ceil(($all_notification[0]->notifications_amount)/8); $i++)
							@if(isset($_GET['page']) && $_GET['page']==$i)
							<li class="page-item active">
								<a class="goto-page page-link">{{$i}}</a>
							</li>
							@else
							<li class="page-item">
								<a class="goto-page page-link">{{$i}}</a>
							</li>
							@endif
						@endfor
					@endif
					@if(isset($_GET['page']) && count($all_notification)>0 && $_GET['page'] < ceil(($all_notification[0]->notifications_amount)/8)-1)
					<li id="page-forward" class="page-item">
						<a class="page-link" tabindex="-1">Next</a>
					</li>
					@else
					<li class="page-item disabled" style="cursor:no-drop;">
						<a class="page-link" tabindex="-1">Next</a>
					</li>
					@endif
				</ul>
			</nav>
			
			<!-- ... end Pagination -->

</div>
@endsection