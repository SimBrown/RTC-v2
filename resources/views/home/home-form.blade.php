<!-- News Feed Form  -->
				
<div class="news-feed-form">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active inline-items" data-toggle="tab" role="tab" style="background: none;border: none;" aria-expanded="true">
				
								<svg class="olymp-status-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-status-icon"></use></svg>
				
								<span>Status</span>
							</a>
						</li>
						
					</ul>
				
					<!-- Status Form -->
					<div class="tab-content">
						<div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
							<form id="status-form" method='POST' enctype="multipart/form-data" action="{{route('post.status')}}">
							@csrf
								<div class="author-thumb">
											@if (Auth::user()->image)
                                                <img src="{{ asset('storage/' . Auth::user()->image) }}" class="avatar" style="width: 48px; height:48px;" alt="author">
                                            @else
                                                <img src="img/author-page.jpg" alt="author">
                                            @endif
								</div>
								<div id="post_content" class="form-group with-icon label-floating is-empty">
									<textarea id="post_body" name="post_body" class="form-control" style="min-height:50px;" autocomplete="off" placeholder="Share what you are thinking here..."></textarea>
								<span class="material-input"></span></div>
								<div class="add-options-message">
                                    
                                    <label for="browsePhoto" style="margin-right:35px;">
                                        <a id="addPhoto" class="options-message" data-toggle="tooltip" data-placement="top" data-original-title="ADD PHOTO">
                                            <svg class="olymp-camera-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-camera-icon"></use></svg>
                                        </a>
                                    </label>
				
									<button id="post-status" type="button" class="btn btn-primary btn-md-2">Post Status</button>
				
								</div>
				
							</form>
						</div>
						
					</div>
				</div>
				

                <form  hidden enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="">
                    <input name="image" id="browsePhoto" type="file">
                    @csrf
				</form>
				<!-- ... end News Feed Form  -->			
			</div>