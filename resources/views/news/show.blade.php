@extends('layouts.app')

@section('content')

@if (count($news) >0)
<h2>News </h2>
  <h3>{{ $news[0]->title }} </h2>
  <p>{{ $news[0]->content }}</p>
  <p>likes:  {{ $news[0]->likes }}</p>
  <button id="news_like" value="{{$news[0]->liked}}"> Press to like</button>
@if ($comments)
<h2>Commenti </h2>
@foreach ($comments as $n)
  <ul id="comments_list">   
    <li>{{ $n->body }} </li>
  </ul>
@endforeach

<div> 
      <label>Inserisci commento ... </label>
      <input id="body" type="text">  
</div>

<button type="submit" id="comment_button" class="btn btn-default">Save</button>

@endif
@endif

<script>
  const PRESSED = 1;
  const NOT_PRESSED = 0;

	$('#comment_button').on('click', function () {
		var comment = $('#body').val();
    $.ajax({
      url : "comment",
      type : "POST",
      data: {'body': comment, 'user_id': {{Auth::user()->id}}, 'news_id': {{$news[0]->id}} },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      cache : false,
      success: function(response) {     
        $('#comment_button').val("");
        $('#comments_list').append("<li>"+ comment +"</li>")
      },
      error: function(response) {
        alert('An error occurred.');
      }
      });
   	});
	
    $('#news_like').on('click', function () {
      var new_value = $('#news_like').val() == PRESSED ? NOT_PRESSED : PRESSED;
      var value = $('#news_like').val() == PRESSED ? PRESSED : NOT_PRESSED;
      console.log(value)
      $.ajax({
        url : "like",
        type : "POST",
        data: {'like': value, 'user_id': {{Auth::user()->id}}, 'news_id': {{$news[0]->id}} },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache : false,
        success: function(response) {
          $('#news_like').val(new_value);
        },
        error: function(response) {
          alert('An error occurred.');
        }
        });
   	});

</script>

@endsection