<div class="post-additional-info inline-items">
	<a post-id="{{$n[0]->pid}}" class="post-add-icon inline-items">
		@if($n[0]->has_liked == '1')
		<svg id="like-bottom-{{$n[0]->pid}}" post-id="{{$n[0]->pid}}" onclick="$('#like-top-{{$n[0]->pid}}').click()" class="like-post svgClicked olymp-heart-icon">
			@else
			<svg class="olymp-heart-icon" onclick="$('#like-top-{{$n[0]->pid}}').click()">
				@endif
				<use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-heart-icon"></use>
			</svg>
			<span>{{$n[0]->likes}}</span>
        </a>

	<ul class="friends-harmonic">
		@foreach($feed_likes as $fl) @if($fl->lpid == $n[0]->pid)
		<li>
			<a href="#">
				<img src="{{ asset('storage/').'/'.$fl->image}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$fl->name}}"
				 alt="friend">
			</a>
		</li>
		@endif @endforeach
	</ul>

	<div class="names-people-likes">
		@if($n[0]->likes > 2 && $n->has_liked==1)
		<a href="#">You</a>
		</a>
		and
		<br>{{$n[0]->likes}} more liked this 
		@elseif($n[0]->likes == 1 && $n[0]->has_liked==1)
		<a href="#">You</a>
		<br>liked this 
		@elseif($n[0]->likes == 1 && !$n[0]->has_liked==1 )
		1 liked this 
		@elseif($n[0]->likes > 1 && !$n[0]->has_liked==1 )
		{{$n[0]->likes}} liked this   
		@elseif($n[0]->likes == 0)
		<strong>Noone</strong>
		<br>liked this
		@else
		<a href="#">You</a> and</a>
		<br>{{$n[0]->likes - 1}} more liked this 
		@endif
	</div>

	<div class="comments-shared">
		<a style="cursor:pointer" onclick="$('#comment-list-{{$n[0]->pid}}').toggle()" class="post-add-icon inline-items">
			<svg class="olymp-speech-balloon-icon">
				<use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-speech-balloon-icon"></use>
			</svg>
			<span>Commenti: {{$n[0]->comment_numb}}</span>
		</a>
	</div>
</div>