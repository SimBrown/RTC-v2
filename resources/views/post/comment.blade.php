<li class="comment-item">
    <div class="post__author author vcard inline-items">
        <img src="{{ asset('storage/').'/'.Auth::User()->image}}" alt="author">

        <div class="author-date">
            <a class="h6 post__author-name fn" href="#">{{Auth::User()->name.' '.Auth::User()->surname}}</a>
            <div class="post__date">
                <time class="published" datetime="2017-03-24T18:18">
                   now
                </time>
            </div>
        </div>

        <a href="#" class="more">
            <svg class="olymp-three-dots-icon">
                <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use>
            </svg>
        </a>

    </div>

    <p>{{$comment->body}}</p>

    <a href="#" class="post-add-icon inline-items">
        <svg class="olymp-heart-icon">
            <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-heart-icon"></use>
        </svg>
        <span>8</span>
    </a>
    <a href="#" class="reply">Reply</a>
</li>
