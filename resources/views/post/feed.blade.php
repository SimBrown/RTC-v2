<div id="newsfeed-items-grid">
                                 
					@if ($feed)
					@foreach ($feed as $n)
					
					
                    <div id="post-{{$n->pid}}" class="ui-block">
                        <!-- Post -->
                        
                        <article class="hentry post">
                        
                            <div class="post__author author vcard inline-items">
                                <img src="{{ asset('storage/').'/'.$n->user_image}}" alt="author">
                        
                                <div class="author-date">
                                    <a class="h6 post__author-name fn">{{$n->author_name." ".$n->author_surname}}</a>
                                    <div class="post__date">
                                        <time class="published" datetime="2017-03-24T18:18">
                                            {{$n->created}}
                                        </time>
                                    </div>
                                </div>
                        
                                <div class="more">
                                    <svg class="olymp-three-dots-icon">
                                        <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use>
                                    </svg>
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Cancella Post</a>
                                        </li>
                                    </ul>
                                </div>
                        
                            </div>
                        
                            <p style="font-size:19px">{{$n->body}}</p>
                            @if(!empty($n->image))
                            <div><img class="post-image-wd" src="{{asset('/storage/'.$n->image)}}"></div>
                            @endif
                            <div id="bottom-bar-{{$n->pid}}" class="bottom-bar">
                                <div class="post-additional-info inline-items">
                            
                                    <a id="like-bottom-{{$n->pid}}" post-id="{{$n->pid}}" class="like-post post-add-icon inline-items">
                                        @if($n->has_liked == '1')
                                        <svg class="svgClicked olymp-heart-icon">
                                        @else
                                        <svg class="olymp-heart-icon">
                                        @endif
                                            <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-heart-icon"></use>
                                        </svg>
                                        <span>{{$n->likes}}</span>
                                    </a>
                            
                                    <ul class="friends-harmonic">
                                @foreach($feed_likes as $fl)
                                    @if($fl->lpid == $n->pid)
                                        <li>
                                            <a href="#">
                                                <img src="{{ asset('storage/').'/'.$fl->image}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$fl->name}}" alt="friend">
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                                    </ul>
                            
                                    <div class="names-people-likes">
                                        @if($n->likes > 2 && $n->has_liked==1)
                                        <a href="#">You</a>
                                        </a>
                                        and
                                        <br>{{$n->likes}} more liked this 
                                        @elseif($n->likes == 1 && $n->has_liked==1)
                                        <a href="#">You</a>
                                        <br>liked this 
                                        @elseif($n->likes == 1 && !$n->has_liked==1 )
                                        1 liked this 
                                        @elseif($n->likes > 1 && !$n->has_liked==1 )
                                        {{$n->likes}} liked this   
                                        @elseif($n->likes == 0)
                                        <strong>Noone</strong>
                                        <br>liked this
                                        @else
                                        <a href="#">You</a> and</a>
                                        <br>{{$n->likes - 1}} more liked this 
                                        @endif
                                    </div>
                            
                            
                                    <div class="comments-shared">
                                        <a style="cursor:pointer" onclick="$('#comment-list-{{$n->pid}}').toggle()" class="post-add-icon inline-items">
                                            <svg class="olymp-speech-balloon-icon">
                                                <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-speech-balloon-icon"></use>
                                            </svg>
                                            <span>Commenti: {{$n->comment_numb}}</span>
                                        </a>
                                    </div>
                            
                            
                                </div>
                            </div><!-- end bottom bar container -->
                        
                            <div class="control-block-button post-control-button">
                        
                                @if($n->has_liked == '1')
                                <a id="like-top-{{$n->pid}}" post-id="{{$n->pid}}" class="iconClicked like-post btn btn-control">
                                @else
                                <a id="like-top-{{$n->pid}}" post-id="{{$n->pid}}" class="like-post btn btn-control">
                                @endif
                                    <svg class="olymp-like-post-icon">
                                        <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-like-post-icon"></use>
                                    </svg>
                                </a>
                        
                                <a post-id="{{$n->pid}}" class="btn btn-control">
                                    <svg class="olymp-comments-post-icon">
                                        <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use>
                                    </svg>
                                </a>

                        
                            </div>
                        
                        </article>
                        
                        <!-- .. end Post -->
                        
                      <!-- Comment Block -->
                    <ul id="comment-list-{{$n->pid}}" style="display:none" class="comments-list">
                        @foreach($feed_comments as $pc)
                            @if($pc->id == $n->pid)
                                <li class="comment-item">
                                        <div class="post__author author vcard inline-items">
                                            <img src="{{ asset('storage/').'/'.$pc->author_image}}" alt="author">
                                
                                            <div class="author-date">
                                                <a class="h6 post__author-name fn" href="#">{{$pc->name." ".$pc->surname}}</a>
                                                <div class="post__date">
                                                    <time class="published" datetime="2017-03-24T18:18">
                                                    {{$pc->created_at}}
                                                    </time>
                                                </div>
                                            </div>
                                
                                            <a href="#" class="more">
                                                <svg class="olymp-three-dots-icon">
                                                    <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use>
                                                </svg>
                                            </a>
                                
                                        </div>
                                
                                        <p>{{$pc->body}}</p>
                                
                                        <a href="#" class="post-add-icon inline-items">
                                            <svg class="olymp-heart-icon">
                                                <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-heart-icon"></use>
                                            </svg>
                                            <span>8</span>
                                        </a>
                                        <a href="#" class="reply">Reply</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <!-- End Comment Block -->
                    <div style="background-color:white">    
                        <!-- <a href="#" class="more-comments">View more comments <span>+</span></a> -->
                        <form id="comment-form-{{$n->pid}}" class="comment-form inline-items" method='POST' enctype="multipart/form-data">
                        
                            <div class="post__author author vcard inline-items">
                                <img src="{{ asset('storage/').'/'.Auth::User()->image}}" alt="author">
                        
                                <div class="form-group with-icon-right is-empty">
                                    <textarea name="comment-body" post-id="{{$n->pid}}" class="comment-text form-control" autocomplete="off" placeholder="Inserisci Commento..."></textarea>
                                <span class="material-input"></span></div>
                            </div>
                    
                        </form>
                    </div>
                    
                        <!-- ... end Comment Form  -->		
                    </div>

					
					@endforeach
    				@endif
</div>