@section('news-block')
    <table class="table table-striped">
        <tbody>
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Body</th>
                </tr>
            </thead>
    @if ($news)
        @foreach ($news as $n)
            <tr class='clickable-row' data-href='news/{{$n->id}}' style="cursor: pointer;">
                <th scope="row">{{$n->title}}</th>
                <td>{{$n->content}}</td>
            </tr>
        @endforeach
    @endif        
        </tbody>
    </table>

    <script>
    jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
    </script>

@endsection
