@if(isset($friends_notification))
    @foreach($friends_notification as $mn)

        @switch($mn->type)
            
            @case('friends')
            <li>
                <div class="author-thumb">
                    <img src="{{ asset('storage/' . $mn->image) }}" alt="author">
                </div>
                <div class="notification-event">
                    <div><a href="/profile/user/{{$mn->user_from_id}}" class="h6 notification-friend">{{$mn->user_from_name." ".$mn->user_from_surname}}</a> Sent you a <a href="/profile/settings/friend-requests" class="notification-link">friend request.</a></div>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">now</time></span>
                </div>
                    <span class="notification-icon">
                        <svg class="olymp-comments-post-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use></svg>
                    </span>

                <div class="more">
                    <svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>
                    <svg class="olymp-little-delete"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-little-delete"></use></svg>
                </div>
            </li>
            @break

        @endswitch
    @endforeach
@else
@endif

@if(count($friends_notification))
<input id="friends-notification-amount" type="hidden" value="{{$friends_notification[0]->notifications_amount}}">
@else
<input id="friends-notification-amount" type="hidden" value="0">
@endif
