@if(isset($message_notification))
    @foreach($message_notification as $mn)

        @switch($mn->type)
            
            @case('message')
            <li>
                <div class="author-thumb">
                    <img src="{{ asset('storage/' . $mn->image) }}" alt="author">
                </div>
                <div class="notification-event">
                    <div><a href="#chat-friend-{{$mn->user_from_id}}" friend_id="{{$mn->user_from_id}}" class="h6 message-notification notification-friend">{{$mn->user_from_name." ".$mn->user_from_surname}}</a>Sent you a message!</div>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">now</time></span>
                </div>
            </li>
            @break

        @endswitch
    @endforeach
@else
@endif

@if(count($message_notification))
<input id="message-notification-amount" type="hidden" value="{{$message_notification[0]->notifications_amount}}">
@else
<input id="message-notification-amount" type="hidden" value="0">
@endif