@if(isset($notification))
    @foreach($notification as $n)

        @switch($n->type)
            
            @case("like")
            <li>
                <div class="author-thumb">
                    <img src="{{ asset('storage/' . $n->image) }}" alt="author">
                </div>
                <div class="notification-event">
                    <div><a href="/profile/user/{{$n->user_from_id}}" class="h6 notification-friend">{{$n->user_from_name." ".$n->user_from_surname}}</a> liked your <a href="/profile/user/{{Auth::User()->id}}#post-{{$n->post_id}}" class="notification-link">post.</a></div>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">now</time></span>
                </div>
                    <span class="notification-icon">
                        <svg class="olymp-comments-post-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use></svg>
                    </span>

                <div class="more">
                    <svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>
                    <svg class="olymp-little-delete"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-little-delete"></use></svg>
                </div>
            </li>
            @break

            @case("comment")
            <li>
                <div class="author-thumb">
                    <img src="{{ asset('storage/' . $n->image) }}" alt="author">
                </div>
                <div class="notification-event">
                    <div><a href="/profile/user/{{$n->user_from_id}}" class="h6 notification-friend">{{$n->user_from_name." ".$n->user_from_surname}}</a> commented your <a href="/profile/user/{{Auth::User()->id}}#post-{{$n->post_id}}" class="notification-link">post.</a></div>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">now</time></span>
                </div>
                    <span class="notification-icon">
                        <svg class="olymp-comments-post-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use></svg>
                    </span>

                <div class="more">
                    <svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>
                    <svg class="olymp-little-delete"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-little-delete"></use></svg>
                </div>
            </li>
            @break

        @endswitch
    @endforeach
@else
@endif

@if(count($notification))
<input id="notification-amount" type="hidden" value="{{$notification[0]->notifications_amount}}">
@else
<input id="notification-amount" type="hidden" value="0">
@endif