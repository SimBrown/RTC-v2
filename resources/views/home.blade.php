@extends('layouts.app')

@section('content')
<!-- ... end Responsive Header-BP -->
<div class="header-spacer"></div>

<div class="container">
	<div class="row row-feed">

		<!-- Main Content -->

		<main class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">

			<div class="ui-block">
				
				@include('home.home-form')
				
				<!-- ... News Feed  -->
				@include('post.feed')

			<a id="load-more-button" href="#" class="btn btn-control btn-more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>

		</main>

		<!-- ... end Main Content -->


		<!-- Left Sidebar -->

		@include('layouts.left-sidebar')

		<!-- ... end Left Sidebar -->		
		
		<!-- Right Sidebar -->
		@include('layouts.right-sidebar')
		<!-- ... end Right Sidebar -->

	</div>
</div>

<a class="back-to-top" href="#">
	<img src="/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

<input id="feed-page" hidden autocomplete="off" type=text value="1">


</div>

<script>
	const PRESSED = 1;
  	const NOT_PRESSED = 0;

	$('.news_like').on('click', function () {
      var news_like_span = this.querySelector('#news_like_span');
	  var news_like_a = this;
	  var new_value = news_like_a.style.fill == 'rgb(255, 94, 58)' ? NOT_PRESSED : PRESSED;
      var value = new_value == PRESSED ? NOT_PRESSED : PRESSED;
      $.ajax({
        url : "news/like",
        type : "POST",
        data: {'like': value, 'user_id': {{Auth::user()->id}}, 'news_id': news_like_span.dataset.newsId },
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        cache : false,
        success: function(response) {
			if (new_value == PRESSED) {
				news_like_a.style.fill = '#ff5e3a';
				news_like_span.style.color = '#ff5e3a';
				news_like_span.innerHTML =  Number(news_like_span.innerHTML) + 1;
			}
			else {
				news_like_a.style.fill = '#c2c5d9';
				news_like_span.style.color = '#c2c5d9';
				news_like_span.innerHTML =  Number(news_like_span.innerHTML) - 1;
			}
        },
        error: function(response) {
          alert('An error occurred.');
        }
        });
   	});

	$('.post_comment').on('click', function () {
      var user_id = this.dataset.userId;
	  var news_id = this.dataset.newsId;
	  var body = $('#comment_body'+news_id).val();
      $.ajax({
        url : "news/comment",
        type : "POST",
        data: {'body': body, 'user_id': user_id, 'news_id': news_id },
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        cache : false,
        success: function(response) {
			var comment = {};
			comment.name = '{{ Auth::user()->name }}';
			comment.body = body;
			comment.created_at = '{{ Carbon\Carbon::now() }}';
			addComment(news_id, comment);
			$('#news_comments_count'+news_id).text(Number($('#news_comments_count'+news_id).text()) + 1);
			$('#comment_body'+news_id).val('');
        },
        error: function(response) {
          alert('An error occurred.');
        }
	  });
   	});

	function showCommentTab(div) {
		var newsId = div.dataset.newsId;
		$('#news_comment_area'+newsId)[0].style.display = 'block';


		$.ajax({
        url : "news/comment",
        type : "GET",
        data: {'news_id': newsId },
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        cache : false,
        success: function(response) {
			for (var i = 0;i < response.news_comments.length; i++) {
				addComment(newsId, response.news_comments[i]);
			}
        },
        error: function(response) {
          alert('An error occurred.');
        }
	  });
	}

	function addComment(newsId, comment) {
		$('#comments_list' + newsId).append(' <li class="comment-item"> <div class="post__author author vcard inline-items"> <img src="img/author-page.jpg" alt="author"> <div class="author-date"> <a class="h6 post__author-name fn" href="02-ProfilePage.html">'+ comment.name + '</a> <div class="post__date"> <time class="published" datetime="2004-07-24T18:18"> ' + comment.created_at +' </time> </div> </div> <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg></a> </div> <p>' + comment.body +'</p></li>')
	}
</script>
@endsection
