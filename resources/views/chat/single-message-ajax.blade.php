<li>
    <div class="author-thumb my-thumb-chat">
        <img src="{{ asset('storage/' . Auth::user()->image) }}" alt="author">
    </div>
    <div class="notification-event my-notification-event-chat">
        <span class="chat-message-item my-message-item-chat">{{$message->body}}</span>
        <!-- <span class="chat-message-item my-message-item-chat">I already bought everything</span> -->
        <span class="notification-date" style="float:right!important;"><time class="entry-date updated" datetime="2004-07-24T18:18">{{$message->time}}</time></span>
    </div>
</li>