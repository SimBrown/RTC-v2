@foreach($new_messages as $nm)
    @if($nm->from_id == Auth::User()->id)	
    <!-- My Message -->
    <li>
        <div class="author-thumb my-thumb-chat">
            <img src="{{ asset('storage/' . Auth::user()->image) }}" alt="author">
        </div>
        <div class="notification-event my-notification-event-chat">
            <span class="chat-message-item my-message-item-chat">{{$nm->body}}</span>
            <!-- <span class="chat-message-item my-message-item-chat">I already bought everything</span> -->
            <span class="notification-date" style="float:right!important;"><time class="entry-date updated" datetime="2004-07-24T18:18">{{$nm->time}}</time></span>
        </div>
    </li>
    @else
    <!-- Friend Message -->
    <li>
        <div class="author-thumb friend-thumb-chat">
            <img src="{{ asset('storage/' . $image[0]->image) }}" alt="author" class="mCS_img_loaded">
        </div>
        <div class="notification-event friend-notification-event-chat">
            <span class="chat-message-item friend-message-item-chat">{{$nm->body}}</span><br>
            <span class="notification-date" style="float:left!important;min-width: 90%;"><time class="entry-date updated" datetime="2004-07-24T18:18">{{$nm->time}}</time></span>
            </div>
        </li>
    @endif
@endforeach