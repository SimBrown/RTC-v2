<div id="sub-container-{{$friend[0]->id}}" class="chat-sub-container">
	<div id="chat-window-{{$friend[0]->id}}"class="ui-block popup-chat popup-chat-responsive open-chat cs-chat-window" tabindex="-1" role="dialog"
	 aria-labelledby="update-header-photo" aria-hidden="true">

		<div class="modal-content" style="height:100%!important">
			<div friend_id="{{$friend[0]->id}}" id="modal-header-{{$friend[0]->id}}" class="modal-header">
				<span class="icon-status online"></span>
				<h6 class="title"><a style="color:white;" href="/profile/user/{{$friend[0]->id}}">{{$friend[0]->name." ".$friend[0]->surname}}</a></h6>				
				<div class="more">
					<svg friend_id="{{$friend[0]->id}}" id="close-chat-{{$friend[0]->id}}" class="close-chat olymp-little-delete js-chat-open">
						<use xlink:href="/svg-icons/sprites/icons.svg#olymp-little-delete"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<div id="current-scroll-bar-{{$friend[0]->id}}" style="height: 84%;" class="mCustomScrollbar ps ps--theme_default ps--active-y" data-ps-id="04ea294e-e57b-545d-a8b5-2ecb2b57cb7a">
					<ul id="current-message-list-{{$friend[0]->id}}" class="notification-list chat-message chat-message-field">
					@foreach($chat_messages as $cm)
						@if($cm->from_id == Auth::User()->id)	
						<!-- My Message -->
						<li>
							<div class="author-thumb my-thumb-chat">
								<img src="{{ asset('storage/' . Auth::user()->image) }}" alt="author">
							</div>
							<div class="notification-event my-notification-event-chat">
								<span class="chat-message-item my-message-item-chat">{{$cm->body}}</span>
								<!-- <span class="chat-message-item my-message-item-chat">I already bought everything</span> -->
								<span class="notification-date" style="float:right!important;"><time class="entry-date updated" datetime="2004-07-24T18:18">{{$cm->time}}</time></span>
							</div>
						</li>
						@else
						<!-- Friend Message -->
						<li>
							<div class="author-thumb friend-thumb-chat">
								<img src="{{ asset('storage/' . $cm->image) }}" alt="author" class="mCS_img_loaded">
							</div>
							<div class="notification-event friend-notification-event-chat">
								<span class="chat-message-item friend-message-item-chat">{{$cm->body}}</span><br>
								<span class="notification-date" style="float:left!important;min-width: 90%;"><time class="entry-date updated" datetime="2004-07-24T18:18">{{$cm->time}}</time></span>
								</div>
							</li>
						@endif
					@endforeach
					</ul>
					<div class="ps__scrollbar-x-rail" style="left: 0px; bottom: -218px;">
						<div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
					</div>
					<div class="ps__scrollbar-y-rail" style="top: 218px; height: 337px; right: 0px;">
						<div class="ps__scrollbar-y" tabindex="0" style="top: 133px; height: 204px;"></div>
					</div>
				</div>

				<form id="chat-form-{{$friend[0]->id}}"  class="need-validation">

					<div class="form-group label-floating is-empty">
						<label class="control-label">Press enter to post...</label>
						<textarea id="chat-text" name="message-body" class="form-control" placeholder=""></textarea>
						<span class="material-input"></span>
					</div>

				</form>
			</div>
		</div>
	</div>
</div><!-- end chat 1 -->