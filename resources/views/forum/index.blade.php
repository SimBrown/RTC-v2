@extends('layouts.app')
@include('forum.section-block')
@section('content')


<!-- ... end Responsive Header-BP -->
<div class="header-spacer header-spacer-small"></div>


<!-- Main Header Groups -->

<div class="main-header spacer">
	<div class="content-bg-wrap bg-group"></div>
	<div class="container">
		<div class="row">
			<div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
				<div class="main-header-content">
					<h1>Welcome to the Forums!</h1>
					<p>Here in the forums you’ll be able to easily create and manage categories and topics to share with the
	 community! We included some of the most used topics, like music, comics, movies, and community, each one with a cool
	  and customizable illustration so you can have fun with them! </p>
				</div>
			</div>
		</div>
	</div>

	
</div>

<!-- ... end Main Header Groups -->
<div class="container">
	<div class="row">
		<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="ui-block responsive-flex">
				<div class="ui-block-title">
					<div class="h6 title">RTC Forum</div>
					<form class="w-search">
						<div class="form-group with-button">
							<input class="form-control" type="text" placeholder="Cerca nel forum...">
							<button>
								<svg class="olymp-magnifying-glass-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use></svg>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container">
	<div class="row">
		<div class="col col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">

			<div class="ui-block">
				
				<table class="forums-table">

				@yield('section-block')
				<!-- Forums Table -->
				
				</table>
				
				<!-- ... end Forums Table -->
			</div>

		</div>

		<div class="col col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Featured Topics</h6>
				</div>
				<div class="ui-block-content">

					
					<!-- Widget Featured Topics -->
					
					<ul class="widget w-featured-topics">
					@if(count($featured_topics)>0)
                    @foreach ($featured_topics as $f)
						<li>
							<i class="icon fa fa-star" aria-hidden="true"></i>
							<div class="content">
								<a href="/forum/showpost/{{$f->id}}" class="h6 title">{{ $f->title }}</a>
								<time class="entry-date updated" datetime="2017-06-24T18:18">{{ $f->created_at }}</time>
							</div>
						</li>
					@endforeach
					@endif
					</ul>
					
					<!-- ... end Widget Featured Topics -->
				</div>
			</div>

			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Recent Topics</h6>
				</div>
				<div class="ui-block-content">

					
					<!-- Widget Recent Topics -->
					
					<ul class="widget w-featured-topics">
						@if(count($recent_topics)>0)
						@foreach ($recent_topics as $r)
							<li>
								<div class="content">
									<a href="/forum/showpost/{{$r->id}}" class="h6 title">{{ $r->title }}</a>
									<time class="entry-date updated" datetime="2017-06-24T18:18">{{ $r->created_at }}</time>
									<div class="forums">{{ $r->name }}</div>
								</div>
							</li>
						@endforeach
						@endif
					</ul>
					
					<!-- ... end Widget Recent Topics -->
				</div>
			</div>
		</div>
	</div>
</div>

<a class="back-to-top" href="#">
	<img src="/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>


@endsection
