@extends('layouts.app')
@section('content')


<!-- ... end Responsive Header-BP -->
<div class="header-spacer header-spacer-small"></div>


<!-- Main Header Groups -->

<div class="main-header spacer">
	<div class="content-bg-wrap bg-group"></div>
	<div class="container">
		<div class="row">
			<div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
				<div class="main-header-content">
					<h1>Welcome to the Forums!</h1>
					<p>Here in the forums you’ll be able to easily create and manage categories and topics to share with the
						community! We included some of the most used topics, like music, comics, movies, and community, each one with a cool
						and customizable illustration so you can have fun with them!</p>
				</div>
			</div>
		</div>
	</div>

	
</div>

<!-- ... end Main Header Groups -->

<div class="container">
	<div class="row">
		<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="ui-block responsive-flex">
				<div class="ui-block-title">
					<div class="h6 title">
						<a class="h6 title" href="{{route('forum.index')}}"> RTC Forum </a> / {{$path['section']}} /  <a class="h6 title" href="/forum/category/{{$path['category_id']}}">{{$path['category']}}</a> / <span class="path_underlined">{{$post[0]->title}}</span></div>
					<form class="w-search" method="GET" action="{{route('forum.search')}}">
					{{ csrf_field() }}
						<div class="form-group with-button">
							<input class="form-control" name="keyword" type="text" placeholder="Search the forum...">
							<button type="submit">
								<svg class="olymp-magnifying-glass-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use></svg>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">

			<div class="ui-block responsive-flex">
				<div class="ui-block-title">
					<h5>
						<i class="icon fa fa-star c-yellow" aria-hidden="true" style="margin-right:25px"></i>
						{{$post[0]->title}}
					</h5>
					<div class="col col-xl-1 col-lg-1 col-md-3 col-sm-4 col-12">
						<a href="#reply" style="background-color:#2687d0" class="btn btn-sm">Rispondi</a>
					</div>
				</div>


				<!-- Open Topic Table -->

				<table class="open-topic-table">

					<thead>

					<tr>
						<th class="author">
							Author
						</th>

						<th class="posts">
							Post
						</th>
					</tr>

					</thead>

					<tbody>

					<tr>
						<td class="topic-date" colspan="2">
						{{ \Carbon\Carbon::parse($post[0]->p_created_at)->format('d-M-Y H:m')}}
						</td>
					</tr>

					<tr>
						<td class="author">
							<div class="author-thumb">
								@if ($post[0]->image)
									<img src="{{ asset('storage/' . $post[0]->image) }}" class="avatar" style="width: 48px; height:48px;" alt="author">
								@else
									<img src="/img/avatar2.jpg" alt="author">
								@endif
							</div>
							<div class="author-content">
								<a href="02-ProfilePage.html" class="h6 author-name">{{$post[0]->p_author}}</a>
							</div>
						</td>
						<td class="posts">
							{{$post[0]->body}}
						</td>
					</tr>
				@foreach($replies as $r)
					<tr>
						<td class="topic-date" colspan="2">
						{{ \Carbon\Carbon::parse($r->r_created_at)->format('d-M-Y H:m')}}
							<a href="#reply" class="reply-topic" body="{!!$r->body!!}" onclick="quote(this)">Quota</a>
						</td>
					</tr>

					<tr>
						<td class="author">
							<div class="author-thumb">
								<img src="/img/avatar3.jpg" alt="author">
							</div>
							<div class="author-content">
								<a href="02-ProfilePage.html" class="h6 author-name">{{$r->r_author}}</a>
								
							</div>
						</td>
						<td class="posts summernote-container">
							{!!$r->body!!}
						</td>
					</tr>

				@endforeach

					</tbody>
				</table>

				<!-- ... end Open Topic Table -->
			</div>


			<!-- Pagination -->

			<nav aria-label="Page navigation">
				<ul class="pagination justify-content-center">
					{{--  <li class="page-item disabled">
						<a class="page-link" href="#" tabindex="-1">Previous</a>
					</li>
					<li class="page-item"><a class="page-link" href="#">1<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: -10.3833px; top: -16.8333px; background-color: rgb(255, 255, 255); transform: scale(16.7857);"></div></div></a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item"><a class="page-link" href="#">...</a></li>
					<li class="page-item"><a class="page-link" href="#">12</a></li>
					<li class="page-item">
						<a class="page-link" href="#">Next</a>
					</li>  --}}
				</ul>
			</nav>

			<!-- ... end Pagination -->

			<div id="reply" class="ui-block">
				<div class="ui-block-title bg-blue">
					<h6 class="title c-white">Invia una risposta</h6>
				</div>
				<div class="ui-block-content">
					<form method="post" action="/forum/showpost/{{$post[0]->post_id}}">
						{{csrf_field()}}
						<div class="row">

							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

								<div class="form-group is-empty">
									<label class="control-label">Title</label>
									<input class="form-control" disabled placeholder="Re: {{$post[0]->title}}" type="text">
								<span class="material-input"></span></div>
								
								<label class="control-label">Text</label>
								<div class="form-group label-floating">
									<label class="control-label">Text</label>
									<textarea id="reply_body" class="form-control summernote"  style="height: 240px" name="reply_body"></textarea>
								<span class="material-input"></span></div>

							</div>

							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<button type="reset" onclick="cleanSummer()" class="btn btn-secondary btn-lg full-width">Cancel</button>
							</div>

							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<button type="submit" class="btn btn-blue btn-lg full-width">Send Reply</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- ... end Reply form -->


		</div>

		<div class="col col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Featured Topics</h6>
				</div>
				<div class="ui-block-content">


					<!-- Widget Featured Topics -->

					<ul class="widget w-featured-topics">
						@if(count($featured_topics)>0)
						@foreach ($featured_topics as $f)
							<li>
								<i class="icon fa fa-star" aria-hidden="true"></i>
								<div class="content">
									<a href="/forum/showpost/{{$f->id}}" class="h6 title">{{ $f->title }}</a>
									<time class="entry-date updated" datetime="2017-06-24T18:18">{{ $f->created_at }}</time>
								</div>
							</li>
						@endforeach
						@endif
					</ul>

					<!-- ... end Widget Featured Topics -->
				</div>
			</div>

			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Recent Topics</h6>
				</div>
				<div class="ui-block-content">


					<!-- Widget Recent Topics -->

					<ul class="widget w-featured-topics">
						@if(count($recent_topics)>0)
							@foreach ($recent_topics as $r)
								<li>
									<div class="content">
										<a href="/forum/showpost/{{$r->id}}" class="h6 title">{{ $r->title }}</a>
										<time class="entry-date updated" datetime="2017-06-24T18:18">{{ $r->created_at }}</time>
										<div class="forums">{{ $r->name }}</div>
									</div>
								</li>
							@endforeach
							@endif
					</ul>

					<!-- ... end Widget Recent Topics -->
				</div>
			</div>

		</div>

	</div>
</div>


<a class="back-to-top" href="#">
	<img src="/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

@endsection