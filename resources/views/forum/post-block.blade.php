@section('post-block')

<table class="forums-table">
				
        <thead>
    
        <tr>
    
            <th class="forum">
                Topic
            </th>
    
            <th class="topics">
                Risposte
            </th>
    
            <th class="posts">
                Visualizzazioni
            </th>
    
            <th class="freshness">
                Ultima Attività
            </th>
    
        </tr>
    
        </thead>
        
        <tbody>

            @if($post)
                            
                    @foreach ($post as $p)
                    
                    <tr>
                            <td class="forum">
                                <div class="forum-item">
                                    <img src="/img/forum2.png" alt="forum">
                                    <div class="content">
                                        <a href="/forum/showpost/{{$p->id}}" class="h6 title">{{$p->title}}</a>
                                        <p class="text">Inviato da <a href="#"  class="h6 title"><small style="font-weight: bold">{{$p->name}}</small></a> 4 hours, 26 minutes ago</p>
                                    </div>
                                </div>
                            </td>
                            <td class="topics">
                                <a href="#" class="h6 count">NUMERO POST TOPIC</a>
                            </td>
                            <td class="posts">
                                <a href="#" class="h6 count">NUMERO VISUALIZZAZIONI POST</a>
                            </td>
                            <td class="freshness">
                                <div class="author-freshness">
                                    <div class="author-thumb">
                                        <img src="/img/avatar51-sm.jpg" alt="author">
                                    </div>
                                    <a href="#" class="h6 title">{{$p->name}}</a>
                                    <time class="entry-date updated" datetime="2017-06-24T18:18">2 hours, 7 minutes ago</time>
                                </div>
                            </td>
                        </tr>
                    
                    @endforeach
                </ul>
            @else
                    <tr>
                        <th> Ancora Nessun Post!</th>
                    </tr>
            @endif

        </tbody>
@endsection