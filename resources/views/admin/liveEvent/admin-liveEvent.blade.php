@extends('admin.dash-layout')
@section('content')
<div class="row">
            <div class="text-center">
                <h2>Gestione Live Event</h2>
            </div>
            
            <table class="table table-fixed table-striped jambo_table ">
                    <thead style="padding-right:18px;">
                        <tr>
                            <th class="col" style="border-bottom:0px!important;width:5%;">Id</th>
                            <th class="col" style="border-bottom:0px!important;width:25%;">Titolo Post</th>
                            <th class="col" style="border-bottom:0px!important;width:10%;">Url</th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        @if(count($liveEvent)>0)
                            
                        @foreach ($liveEvent as $s)
                                <tr style="cursor: pointer;">
                                    <td class="col" style="width:5%;" id="liveEvent_id">{{$s->id}}</td>
                                    <td class="col" style="width:25%;" id="liveEvent_title">{{$s->title}}</td>
                                    <td class="col" style="width:10%;" id="liveEvent_url">{{$s->url}}</td>
                                    <td class="col" style="width:20%;display:table;" ><a class="btn btn-success btn-sm enable edit" style="margin:2px 5px 2px 5px">Modifica</a></td>
                                </tr>
                            @endforeach
                        @else
                                <tr>
                                    <th> Ancora Nessun Evento!</th>
                                </tr>
                        @endif
    
                        
                    </tbody>
                </table>
</div>
<div class="row">
<form method="POST" id="liveEvent_form" action="{{ route('liveEvent.insertOrUpdate') }}" style="margin-top: 12%">
        {{ csrf_field() }}
        <div class="bootstrap-iso">
          <div class="container-fluid">
           <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
             <form method="post">
              <div class="form-group ">
                <input type="hidden" id="form_liveEvent_id" name="liveEvent_id"/>
               <input class="form-control" id="form_title" name="title" placeholder="Insert Title" type="text" required />
              </div>
              <div class="form-group ">
      
               <textarea class="form-control" cols="40" id="form_url" name="url" rows="10" required></textarea>
              </div>
            </div>
            <button class="btn btn-primary " style="display:block;" name="submit" type="submit">Salva</button>
            <button class="btn btn-primary" id="cancel" style="display:block;" type="button">Annulla</button>
               </div>
              </div>
             </form>
            </div>
           </div>
          </div>
         </div>
        </form>
</div>
    

          <style>
                table {
                    width: 50%;
                }
                
                thead th {
                    height: 30px;
                }
                
                tbody {
                    overflow: -moz-scrollbars-vertical;
                    overflow-y: scroll;
                    height: 273px;
                }
                
                tbody td, thead th {
                    float: left;
                    width: 20%;
                }
                
                tr:after {
                    clear: both;
                    url: ' ';
                    display: block;
                    visibility: hidden;
                }
            </style>
            
    <script>

        jQuery(document).ready(function() {
            $(".edit").click(function() {
                var liveEvent_id = $(this).parent().parent().find("#liveEvent_id").text();
                var title = $(this).parent().parent().find("#liveEvent_title").text();
                var url = $(this).parent().parent().find("#liveEvent_url").text();

                $("#form_liveEvent_id").val(liveEvent_id);
                $("#form_title").val(title);
                $("#form_url").val(url);      
            });

            $(".cancel").click(function() {
                resetForm();
            });
        });

        function resetForm(){
            $("#form_liveEvent_id").val("");
            $("#form_title").val("");
            $("#form_url").val("");
        }

    </script>
    
    
@endsection