@extends('admin.dash-layout')
@section('content')
<div class="row">
            <div class="text-center">
                <h2>Gestione Post Archiviati</h2>
            </div>
            
            <table class="table table-fixed table-striped jambo_table ">
                    <thead style="padding-right:18px;">
                        <tr>
                            <th class="col" style="border-bottom:0px!important;width:5%;">Id</th>
                            <th class="col" style="border-bottom:0px!important;width:25%;">Description</th>
                            <th class="col" style="border-bottom:0px!important;width:10%;">cost</th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        @if(count($prize)>0)
                            
                        @foreach ($prize as $s)
                                <tr style="cursor: pointer;">
                                    <td class="col" style="width:5%;" id="prize_id">{{$s->id}}</td>
                                    <td class="col" style="width:25%;" id="prize_description">{{$s->description}}</td>
                                    <td class="col" style="width:10%;" id="prize_cost">{{$s->cost}}</td>
                                    <td class="col" style="width:20%;display:table;" ><a class="btn btn-success btn-sm enable edit" style="margin:2px 5px 2px 5px">Modifica</a></td>
                                </tr>
                            @endforeach
                        @else
                                <tr>
                                    <th> Ancora Nessun Post!</th>
                                </tr>
                        @endif
    
                        
                    </tbody>
                </table>
</div>
<div class="row">
<form method="POST" id="prize_form" action="{{ route('prize.insertOrUpdate') }}" style="margin-top: 12%">
        {{ csrf_field() }}
        <div class="bootstrap-iso">
          <div class="container-fluid">
           <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
             <form method="post">
              <div class="form-group ">
                <input type="hidden" id="form_prize_id" name="prize_id"/>
               <input class="form-control" id="form_description" name="description" placeholder="Insert description" type="text" required />
              </div>
              <div class="form-group ">
      
               <input class="form-control" cols="40" id="form_cost" name="cost" type="number"required></input>
              </div>
            </div>
            <button class="btn btn-primary " style="display:block;" name="submit" type="submit">Salva</button>
            <button class="btn btn-primary" id="cancel" style="display:block;" type="button">Annulla</button>
               </div>
              </div>
             </form>
            </div>
           </div>
          </div>
         </div>
        </form>
</div>
    

          <style>
                table {
                    width: 50%;
                }
                
                thead th {
                    height: 30px;
                }
                
                tbody {
                    overflow: -moz-scrollbars-vertical;
                    overflow-y: scroll;
                    height: 273px;
                }
                
                tbody td, thead th {
                    float: left;
                    width: 20%;
                }
                
                tr:after {
                    clear: both;
                    cost: ' ';
                    display: block;
                    visibility: hidden;
                }
            </style>
            
    <script>

        jQuery(document).ready(function() {
            $(".edit").click(function() {
                var prize_id = $(this).parent().parent().find("#prize_id").text();
                var description = $(this).parent().parent().find("#prize_description").text();
                var cost = $(this).parent().parent().find("#prize_cost").text();

                $("#form_prize_id").val(prize_id);
                $("#form_description").val(description);
                $("#form_cost").val(cost);      
            });

            $(".cancel").click(function() {
                resetForm();
            });
        });

        function resetForm(){
            $("#form_prize_id").val("");
            $("#form_description").val("");
            $("#form_cost").val("");
        }

    </script>
    
    
@endsection