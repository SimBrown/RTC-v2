@extends('admin.dash-layout')
@section('content')


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="container">

            <div class="row">
                <h2>Gestione Utenti</h2>
            </div>
            
            <table id="user-table" class="table table-fixed table-striped jambo_table ">
                <thead style="padding-right:18px;">
                    <tr>
                        <th class="col" style="border-bottom:0px!important;width:5%;">Id</th>
                        <th class="col" style="border-bottom:0px!important;width:25%;">Nome Utente</th>
                        <th class="col" style="border-bottom:0px!important;width:36%;">Email</th>
                        <th class="col" style="border-bottom:0px!important;width:17%;"></th>
                        <th class="col" style="border-bottom:0px!important;width:17%;"></th>
                    </tr>
                </thead>
                <tbody>
                  
                    @if(count($users)>0)       
                    @foreach ($users as $s)
                            <tr style="cursor: pointer;">  
                                <td class="col" style="width:5%;" id="user_id">{{$s->id}}</td>
                                <td class="col" style="width:25%;" id="user_name">{{$s->name}}</td>
                                <td class="col" style="width:35%;" id="user_email">{{$s->email}}</td>
                                <td class="col" style="width:10%;" ><a class="btn btn-warning btn-sm edit" style="margin:2px 5px 2px 5px">Modifica</a></td>
                                <td class="col" style="width:10%;" ><a class="btn btn-danger btn-sm archive" style="margin:2px 5px 2px 5px">Disattiva</a></td>
                            </tr>
                        @endforeach
                    @else
                            <tr>
                                <th> Ancora Nessun Utente!</th>
                            </tr>
                    @endif

                    
                </tbody>
            </table>
        </div>
        

    </div>
    <div class="col-md-1"></div>

        <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Modifica Utente<small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" method="POST" action="{{route('user.insertOrUpdate')}}" >
                      {{ csrf_field() }}
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome Utente</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input oninvalid="this.setCustomValidity('Devi riempire questo campo.')" required="" name="form_user_name" id="form_user_name" class="form-control" placeholder="Nome Utente" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input oninvalid="this.setCustomValidity('Devi riempire questo campo.')" required="" name="form_email" id="form_email" class="form-control" placeholder="Email" type="text">
                            </div>
                          </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Id Post</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input name="form_user_id" id="form_user_id" class="form-control" readonly="readonly" placeholder="Id Utente" type="text" value="">
                        {{--  <input name="form_sec_id" type="hidden" id="form_sec_id" class="form-control" value="">  --}}
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Gruppo</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select autocomplete="off" oninvalid="this.setCustomValidity('Devi selezionare un valore.')"  id="role_select" name="role_select" class="form-control">
                              <option disabled selected hidden value>Seleziona Gruppo</option>
                              @foreach($groups as $g)
                                <option value="{{$g->id}}">{{$g->name}}</option>
                              @endforeach

                          </select>
                            <div  style="display:inline-block;min-height: 45px;">
                                <ul id="ajax-container" class="nav navbar-right panel_toolbox"></ul>
                            </div>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Annulla</button>
                          <button type="submit" class="btn btn-success">Invia</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>
          </div>

            
    <script>

        jQuery(document).ready(function() {
        $("#user-table").DataTable();

        $('#role_select').change(function(){
            var id = $(this).val();
            var name = $('#role_select option:selected').text();

            $('#ajax-container').append('<li class="ajax-li"><ul class="nav navbar-right panel_toolbox" style="min-width:0px;float:left!important"><li><a class="remove-element"><i class="fa fa-close"></i></a></li></ul><label class="ajax-label" groupId="'+id+'">'+name+'</label></li>');
            
            $('.remove-element').click(function(){
            
                $(this).closest('.ajax-li').remove();

            })
            
        })

        $('.remove-element').click(function(){
            
            $(this).closest('.ajax-li').remove();

        })
        

        $(".edit").click(function() {

            var user_id = $(this).parent().parent().find("#user_id").text()
            var user_name = $(this).parent().parent().find("#user_name").text()
            var user_email = $(this).parent().parent().find("#user_email").text()
            var role_id = $(this).parent().parent().find("#user_role").attr("role_id")

            $("#form_user_name").val(user_name);
            $("#form_email").val(user_email);
            $("#form_user_id").val(user_id);
            $("#role_select").val(role_id);
            
            });
        });

        jQuery(document).ready(function() {
          $(".archive").click(function() {
            var user_id = $(this).parent().parent().find("#user_id").text();

            if(confirm("Vuoi davvero disattivare l'utente: "+($(this).parent().parent().find("#user_name").text())+" ?"))
              $.ajax({
                url : "/admin/users",
                type : "POST",
                data: {
                    'user_id': user_id, 
                    'archived': 1
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                    
                    console.log(response['status']);
                    if(response['status']=='0'){alert('Operazione non permessa!');}
                      else {location.reload();}
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
            else
            return false;
          });
        });


    </script>
    
    
@endsection