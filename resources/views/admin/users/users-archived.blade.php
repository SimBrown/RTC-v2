@extends('admin.dash-layout')
@section('content')

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="container">

            <div class="row">
                <h2>Gestione Utenti Disabilitati</h2>
            </div>
            
            <table class="table table-fixed table-striped jambo_table ">
                <thead style="padding-right:18px;">
                    <tr>
                        <th class="col" style="border-bottom:0px!important;width:5%;">Id</th>
                        <th class="col" style="border-bottom:0px!important;width:25%;">Nome Utente</th>
                        <th class="col" style="border-bottom:0px!important;width:35%;">Email</th>
                        <th class="col" style="border-bottom:0px!important;width:15%;">Ruolo</th>
                        <th class="col" style="border-bottom:0px!important;width:20%;"></th>
                    </tr>
                </thead>
                <tbody>
                  
                    @if(count($users)>0)

                    @foreach ($users as $s)
                            <tr style="cursor: pointer;">
                                <td class="col" style="width:5%;" id="user_id">{{$s->id}}</td>
                                <td class="col" style="width:25%;" id="user_name">{{$s->name}}</td>
                                <td class="col" style="width:35%;" id="user_email">{{$s->email}}</td>
                                <td class="col" style="width:15%;" role_id="{{$s->role_id}}" id="user_role">{{$s->role_name}}</td>
                                <td class="col" style="width:20%;display:table;" ><a class="btn btn-success btn-sm enable" style="margin:2px 5px 2px 5px">Abilita</a></td>
                            </tr>
                        @endforeach
                    @else
                            <tr>
                                <th> Ancora Nessun User!</th>
                            </tr>
                    @endif

                    
                </tbody>
            </table>
        </div>
    </div>
</div>
    


          <style>
                table {
                    width: 50%;
                }
                
                thead, tbody, tr, td, th {
                    display: block;
                }
                
                thead th {
                    height: 30px;
                }
                
                tbody {
                    overflow: -moz-scrollbars-vertical;
                    overflow-y: scroll;
                    height: 273px;
                }
                
                tbody td, thead th {
                    float: left;
                    width: 20%;
                }
                
                tr:after {
                    clear: both;
                    content: ' ';
                    display: block;
                    visibility: hidden;
                }
            </style>
            
    <script>

        jQuery(document).ready(function() {
          $(".enable").click(function() {
            var user_id = $(this).parent().parent().find("#user_id").text();

            if(confirm("Vuoi davvero abilitare la categoria: "+($(this).parent().parent().find("#description").text())+" ?"))
              $.ajax({
                url : "/admin/users",
                type : "POST",
                data: {
                  'user_id': user_id,
                  'archived': 0
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
            else
            return false;
          });
        });


    </script>
    
    
@endsection