@extends('admin.dash-layout')
@include('admin.survey.create')
@include('admin.survey.index')

@section('content')

    @yield('index-survey')
    @yield('create-survey')

@endsection