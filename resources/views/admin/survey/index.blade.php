@extends('admin.dash-layout')
@section('content')


<div class="row">
            <div class="text-center">
                <h2>Gestione Survey</h2>
            </div>
            
            <table class="table table-fixed table-striped jambo_table ">
                    <thead style="padding-right:18px;">
                        <tr>
                            <th class="col" style="border-bottom:0px!important;width:2%;">Id</th>
                            <th class="col" style="border-bottom:0px!important;width:5%;">Titolo Post</th>
                            <th class="col" style="border-bottom:0px!important;width:5%;">Descrizione</th>
                            <th class="col" style="border-bottom:0px!important;width:3%;">Reward</th>
                            <th class="col" style="border-bottom:0px!important;width:5%;">Data creazione</th>
                            <th class="col" style="border-bottom:0px!important;width:5%;">Azione</th>
                        </tr>
                    </thead>
                    <tbody>
@if(count($surveys)>0)
    <table class="table table-striped">
        <tbody>
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Title</th>
                    <th scope="col">Descrizione</th>
                    <th scope="col">Reward</th>
                    <th scope="col">Data creazione</th>
                    <th scope="col">Azione</th>
                </tr>
            </thead>
        @foreach ($surveys as $s)
            <tr class='clickable-row' data-href='survey/{{$s->id}}' style="cursor: pointer;">
                <th id="survey_id" scope="row">{{$s->id}}</th>
                <td id="title">{{$s->title}}</td>
                <td id="description">{{$s->description}}</td>
                <td id="reward">{{$s->reward}}</td>
                <td id="created_at">{{$s->created_at}}</td>
                <td><a class="edit">Modifica</a></td>
            </tr>
        @endforeach
    @else
            <tr>
                <th> Ancora Nessun Post!</th>
            </tr>
    @endif
        </tbody>
    </table>

    <form method="POST" id="survey_form" action="{{ route('survey.insertOrUpdate') }}" style="margin-top: 12%">
        {{ csrf_field() }}
        <div class="bootstrap-iso">
          <div class="container-fluid">
           <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
             <form method="post">
              <div class="form-group ">
                <input type="hidden" id="form_survey_id" name="survey_id"/>
               <input class="form-control" id="form_title" name="title" placeholder="Insert Title" type="text" required />
              </div>
              <div class="form-group ">
      
               <textarea class="form-control" cols="40" id="form_description" name="description" rows="10" required></textarea>
              </div>
              <div class="form-group ">
               <input class="form-control" id="form_reward" name="reward" placeholder="Insert reward" type="number" required/>
              </div>
              <div id="survey_options_container">
            </div>
              <div class="form-group ">
                <input type="hidden" id="option_counter" name="option_counter" value="0"/>
                <button class="btn btn-primary" id="option_add" type="button">Aggiungi opzione</button>
            </div>
                <button class="btn btn-primary " name="submit" type="submit">Inserisci</button>
                <button class="btn btn-primary" id="cancel" type="button">Annulla</button>
               </div>
              </div>
             </form>
            </div>
           </div>
          </div>
         </div>
        </form>

         </div>
    <script>
    jQuery(document).ready(function($) {
        var options_counter = 1;

        $("#cancel").click(function() {
            $('#form_title').val("");
            $('#form_description').val("");
            $('#form_survey_id').val("");
            $('#form_reward').val("");
            $('#option_counter').val(0); 
            $("#survey_options_container").empty();
        });

        $(".edit").click(function() {

            var title = $(this).parent().parent().find('#title').text();
            var body = $(this).parent().parent().find('#description').text();
            var id = $(this).parent().parent().find('#survey_id').text();
            var reward = $(this).parent().parent().find('#reward').text();
            
            $('#form_title').val(title);
            $('#form_description').val(body);
            $('#form_survey_id').val(id);
            $('#form_reward').val(reward);

            $.ajax({
                url : "create/options",
                type : "GET",
                data: {'survey_id': id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                    $('#option_counter').val(0); 
                    $("#survey_options_container").empty();
                    for (i=1;i <= response.options.length;i++) {
                        addOptionInput();
                        $('#option' + i).val(response.options[i-1].description);
                        $('#option_hidden' + i).val(response.options[i-1].id);
                    }   

                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
   	    });     

        $("#option_add").click(function() {
            addOptionInput();
        });

        function addOptionInput() {
            options_counter = $('#option_counter').val();
            $('#option_counter').val(++options_counter);

            var formgroup = $("<div/>", {
                class: "form-group"
                });
            formgroup.append($("<label>", {
                class: "col-sm-2 control-label",
                text: "Option " + options_counter,
            }));
            var colsm = $("<div/>", {
                class: "col-sm-10"
                });
            var input = $("<input/>", {
                type: "text",
                class: "form-control",
                id: "option" + options_counter,
                name: "option" + options_counter,
                placeholder: "Enter option " + options_counter
            });
            var inputHidden = $("<input/>", {
                type: "hidden",
                class: "form-control",
                id: "option_hidden" + options_counter,
                name: "option_hidden" + options_counter
            });
            colsm.append(input);
            colsm.append(inputHidden);
            formgroup.append(colsm);
            $('#survey_options_container').append(formgroup);
        }
    });
    </script>
    
    
@endsection

