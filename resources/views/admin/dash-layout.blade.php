@include('admin.admin-head')

<!DOCTYPE html>
<html lang="en">

@yield('admin-head')

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/" class="site_title"><i class="fa fa-paw"></i> <span>RTC</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{Auth::User()->name.' '.Auth::User()->surname}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-users"></i> Utenti <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('user.manage')}}">Gestione Utenza</a></li>
                      <li><a href="{{route('user.friends')}}">Gestione Amicizie</a></li>
                      <li><a href="{{route('user.groups')}}">Gestione Gruppi</a></li>
                      <li><a href="{{route('user.friends')}}">Gestione Pagine</a></li>
                      <li><a href="{{route('user.archived')}}">Utenti Bloccati</a></li>
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-desktop"></i> Forum <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('category.manage')}}">Gestione Categorie</a></li>
                      <li><a href="{{route('category.archived')}}">Categorie Archiviate</a></li>
                      <li><a href="{{route('section.manage')}}">Gestione Sezioni</a></li>
                      <li><a href="{{route('section.archived')}}">Sezioni Archiviate</a></li>
                      <li><a href="{{route('post.manage')}}">Gestione Post</a></li>
                      <li><a href="{{route('post.archived')}}">Post Archiviati</a></li>
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-wrench"></i> Impostazioni <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('settings.permissions')}}">Gestione permessi</a></li>
                      <li><a href="{{route('settings.manageGroups')}}">Gestione gruppi</a></li>
                    </ul>
                  </li>
              </div>  
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">{{Auth::User()->name.' '.Auth::User()->surname}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="/settings"> Profile</a></li>
                    <li><a href="{{route('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" style="min-height:100vh">
          @yield('content')
        </div>
        <!-- /page content -->

      </div>
        <!-- footer content -->
        <footer>
          <div class="pull-right">
          <p><a href="/">RC - An open community</a></p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
    
    <script src="{{ asset('js/admin/fastclick.js') }}"  ></script>
    <!-- NProgress -->
    <script src="{{ asset('js/admin/nprogress.js') }}"  ></script>    
    <!-- Chart.js -->
    <script src="{{ asset('js/admin/Chart.min.js') }}"  ></script>
    <!-- gauge.js -->
    <script src="{{ asset('js/admin/gauge.min.js') }}"  ></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('js/admin/bootstrap-progressbar.min.js') }}"  ></script>
    <!-- iCheck -->
    <script src="{{ asset('js/admin/icheck.min.js') }}"  ></script>
    <!-- Skycons -->
    <script src="{{ asset('js/admin/skycons.js') }}"  ></script>
    <!-- Flot -->
    <script src="{{ asset('js/admin/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('js/admin/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('js/admin/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('js/admin/date.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('js/admin/jquery.vmap.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.vmap.sampledata.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('js/admin/moment.min.js') }}"></script>
    <script src="{{ asset('js/admin/daterangepicker.js') }}"></script>
    <!-- Custom Theme Scripts -->
    
      <!-- Bootstrap -->
      <script src="{{ asset('js/admin/bootstrap.min.js') }}"  ></script>
      <!-- FastClick -->
    <script src="{{ asset('js/admin/custom.min.js') }}"></script>
	
  </body>
</html>
