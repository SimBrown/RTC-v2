@extends('admin.dash-layout')
@section('content')


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="container">

            <div class="row">
                <h2>Gestione Post Archiviati</h2>
            </div>
            
            <table id="post-table" class="table table-fixed table-striped jambo_table ">
                    <thead style="padding-right:18px;">
                        <tr>
                            <th class="col" style="border-bottom:0px!important;width:5%;">Id</th>
                            <th class="col" style="border-bottom:0px!important;width:25%;">Titolo Post</th>
                            <th class="col" style="border-bottom:0px!important;width:10%;">Autore</th>
                            <th class="col" style="border-bottom:0px!important;width:15%;">Categoria</th>
                            <th class="col" style="border-bottom:0px!important;width:10%;">Sezione</th>
                            <th class="col" style="border-bottom:0px!important;width:15%;">Data Creazione</th>
                            <th class="col" style="border-bottom:0px!important;width:20%;"></th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        @if(count($posts)>0)
                            
                        @foreach ($posts as $s)
                                <tr style="cursor: pointer;">
                                    <td class="col" style="width:5%;" id="post_id">{{$s->postId}}</td>
                                    <td class="col" style="width:25%;" id="post_description">{{$s->title}}</td>
                                    <td class="col" style="width:10%;" id="post_author">{{$s->author}}</td>
                                    <td class="col" style="width:15%;" cat={{$s->cat_id}} id="post_cat_name">{{$s->catName}}</td>
                                    <td class="col" style="width:10%;" id="post_cat_id">{{$s->secName}}</td>
                                    <td class="col" style="width:15%;" id="post_time">{{$s->created_at}}</td>
                                    <td class="col" style="width:20%;" ><a class="btn btn-success btn-sm enable" style="margin:2px 5px 2px 5px">Abilita</a></td>
                                </tr>
                            @endforeach
                        
                        @endif
    
                        
                    </tbody>
                </table>
        </div>
    </div>
</div>
    
            
    <script>

        jQuery(document).ready(function() {
        
        $('#post-table').DataTable();

        $(".edit").click(function() {

            console.log("test");

            var cat_id = $(this).parent().parent().find("#cat_id").text()
            var body = $(this).parent().parent().find("#description").text()
            var sec_id = $(this).parent().parent().find("#section_id").text()
            var sec_name = $(this).parent().parent().find("#section_name").text()

            $("#form_cat_id").val(cat_id);
            $("#form_description").val(body);
            $("#form_sec_id").val(sec_id);
            $("#sec_select").val(sec_id);
            

            });
        });

        function resetForm(){
            $("#form_title").val("");
            $("#form_description").val("");
            $("#form_survey_id").val("");
        }
        jQuery(document).ready(function() {
          $(".enable").click(function() {
            var post_id = $(this).parent().parent().find("#post_id").text();

            if(confirm("Vuoi davvero ripristinare il post: "+($(this).parent().parent().find("#post_description").text())+" ?"))
              $.ajax({
                url : "/admin/forum/post",
                type : "POST",
                data: {
                  'post_id': post_id,
                  'archived': 0
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
            else
            return false;
          });
        });


    </script>
    
    
@endsection