@extends('admin.dash-layout')
@section('content')


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="container">

            <div class="row">
                <h2>Gestione Posts</h2>
            </div>

            
            
            <table id="posts-table" class="table table-fixed table-striped jambo_table ">
                <thead style="padding-right:18px;">
                    <tr>
                        <th class="col" style="border-bottom:0px!important;width:5%;">Id</th>
                        <th class="col" style="border-bottom:0px!important;width:25%;">Titolo Post</th>
                        <th class="col" style="border-bottom:0px!important;width:10%;">Autore</th>
                        <th class="col" style="border-bottom:0px!important;width:15%;">Categoria</th>
                        <th class="col" style="border-bottom:0px!important;width:10%;">Sezione</th>
                        <th class="col" style="border-bottom:0px!important;width:15%;">Data Creazione</th>
                        <th class="col" style="border-bottom:0px!important;width:10%;"></th>
                        <th class="col" style="border-bottom:0px!important;width:10%;"></th>
                    </tr>
                </thead>
                <tbody>
                  
                    @if(count($posts)>0)
                        
                    @foreach ($posts as $s)
                            <tr style="cursor: pointer;">
                                <td class="col" style="width:5%;" id="post_id">{{$s->postId}}</td>
                                <td class="col" style="width:25%;" id="post_description"><a target="_blank" href="/forum/showpost/{{$s->postId}}"><i class="fa fa-link"></i> {{$s->title}}</a></td>
                                <td class="col" style="width:10%;" id="post_author">{{$s->author}}</td>
                                <td class="col" style="width:15%;" cat={{$s->cat_id}} id="post_cat_name">{{$s->catName}}</td>
                                <td class="col" style="width:10%;" id="post_cat_id">{{$s->secName}}</td>
                                <td class="col" style="width:15%;" id="post_time">{{$s->created_at}}</td>
                                <td class="col" style="width:10%" ><a class="btn btn-warning btn-sm edit" style="margin:2px 5px 2px 5px">Modifica</a></td>
                                <td class="col" style="width:10%" ><a class="btn btn-danger btn-sm archive" style="margin:2px 5px 2px 5px">Archivia</a></td>
                            </tr>
                        @endforeach

                    @endif

                    
                </tbody>
            </table>
        </div>
        

    </div>
    <div class="col-md-1"></div>

        <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Modifica Post<small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" method="POST" action="{{route('post.insertOrUpdate')}}" enctype="multipart/form-data">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Titolo Post</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input oninvalid="this.setCustomValidity('Devi riempire questo campo.')" required="required" name="form_description" id="form_description" class="form-control" placeholder="Titolo Post" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Id Post</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input name="form_sec_id" id="form_cat_id" class="form-control" readonly="readonly" placeholder="Id Post" type="text" value="">
                        {{--  <input name="form_sec_id" type="hidden" id="form_sec_id" class="form-control" value="">  --}}
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoria</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select oninvalid="this.setCustomValidity('Devi selezionare un valore.')" required="" id="sec_select" name="sec_select" class="form-control">
                              <option disabled selected hidden value>Seleziona Categoria</option>
                              
                            @if(count($categories)>0)

                              @foreach ($categories as $s)
                                          <option value="{{$s->id}}">{{$s->description}}</td>
                                  @endforeach
                              @else
                                <option>Non ci sono categorie</option>
                            @endif
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="image">Image to upload</label><br>
                        <input type="file"  name="image" id="image" class='image'>
                    </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Annulla</button>
                          <button type="submit" class="btn btn-success">Invia</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>
          </div>

         
            
    <script>

        jQuery(document).ready(function() {
        
        $('#posts-table').DataTable();
        console.log('init');

        $(".edit").click(function() {

            console.log("test");

            var post_id = $(this).parent().parent().find("#post_id").text()
            var post_description = $(this).parent().parent().find("#post_description").text()
            var cat_id = $(this).parent().parent().find("#post_cat_name").attr("cat")

            console.log(cat_id)

            $("#form_cat_id").val(post_id);
            $("#form_description").val(post_description);
            $("#sec_select").val(cat_id);
            

            });
        });

        jQuery(document).ready(function() {
          $(".archive").click(function() {
            var post_id = $(this).parent().parent().find("#post_id").text();

            if(confirm("Vuoi davvero eliminare la categoria: "+($(this).parent().parent().find("#post_description").text())+" ?"))
              $.ajax({
                url : "/admin/forum/post",
                type : "POST",
                data: {
                    'post_id': post_id, 
                    'archived': 1
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
            else
            return false;
          });
        });


    </script>
    
    
@endsection