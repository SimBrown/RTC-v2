@extends('admin.dash-layout')
@section('content')


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="container">

            <div class="row">
                <h2>Gestione Categorie</h2>
            </div>
            
            <table id="cat-table" class="table table-fixed table-striped jambo_table ">
                <thead style="padding-right:18px;">
                    <tr>
                        <th class="col" style="border-bottom:0px!important;width:10%;">Id</th>
                        <th class="col" style="border-bottom:0px!important;width:30%;">Nome Categoria</th>
                        <th class="col" style="border-bottom:0px!important;width:10%;">Id Sezione</th>
                        <th class="col" style="border-bottom:0px!important;width:30%;">Nome Sezione</th>
                        <th class="col" style="border-bottom:0px!important;width:10%;"></th>
                        <th class="col" style="border-bottom:0px!important;width:10%;"></th>
                    </tr>
                </thead>
                <tbody>
                  
                    @if(count($categories)>0)

                    @foreach ($categories as $s)
                            <tr style="cursor: pointer;">
                                <td class="col" style="width:10%;" id="cat_id">{{$s->id}}</th>
                                <td class="col" style="width:30%;" id="description">{{$s->description}}</td>
                                <td class="col" style="width:10%;" id="section_id">{{$s->section}}</td>
                                <td class="col" style="width:30%;" id="section_name">{{$s->section_name}}</td>
                                <td class="col" style="width:10%" ><a class="btn btn-warning btn-sm edit" style="margin:2px 5px 2px 5px">Modifica</a></td>
                                <td class="col" style="width:10%" ><a class="btn btn-danger btn-sm delete" style="margin:2px 5px 2px 5px">Archivia</a></td>
                            </tr>
                        @endforeach
                      @endif
                </tbody>
            </table>
        </div>
        

    </div>
    <div class="col-md-1"></div>

        <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Inserisci o Modifica Categorie <small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" method="POST" action="{{route('category.insertOrUpdate')}}" >
                      {{ csrf_field() }}
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome Categoria</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input oninvalid="this.setCustomValidity('Devi riempire questo campo.')" required="required" name="form_description" id="form_description" class="form-control" placeholder="Nome Categoria" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Id Categoria</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input name="form_cat_id" id="form_cat_id" class="form-control" readonly="readonly" placeholder="Disabled Input" type="text" value="">
                          <input name="form_sec_id" type="hidden" id="form_sec_id" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Sezione</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select oninvalid="this.setCustomValidity('Devi selezionare un valore.')" required="required" id="sec_select" name="sec_select" class="form-control">
                              
                            @if(count($sections)>0)

                              @foreach ($sections as $s)
                                          <option value="{{$s->id}}">{{$s->description}}</td>
                                  @endforeach
                              @else
                                <option>Non ci sono sezioni</option>
                            @endif
                          </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Annulla</button>
                          <button type="submit" class="btn btn-success">Invia</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>
          </div>

        
    <script>

        jQuery(document).ready(function() {
        
        $('#cat-table').DataTable();
        
        $(".edit").click(function() {

            console.log("test");

            var cat_id = $(this).parent().parent().find("#cat_id").text()
            var body = $(this).parent().parent().find("#description").text()
            var sec_id = $(this).parent().parent().find("#section_id").text()
            var sec_name = $(this).parent().parent().find("#section_name").text()

            $("#form_cat_id").val(cat_id);
            $("#form_description").val(body);
            $("#form_sec_id").val(sec_id);
            $("#sec_select").val(sec_id);
            

            });
        });

        function resetForm(){
            $("#form_title").val("");
            $("#form_description").val("");
            $("#form_survey_id").val("");
        }
        jQuery(document).ready(function() {
          $(".delete").click(function() {
            var cat_id = $(this).parent().parent().find("#cat_id").text();

            if(confirm("Vuoi davvero eliminare la categoria: "+($(this).parent().parent().find("#description").text())+" ?"))
              $.ajax({
                url : "/admin/forum/category",
                type : "POST",
                data: {
                  'cat_id': cat_id,
                  'archived': 1
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
            else
            return false;
          });
        });


    </script>
    
    
@endsection