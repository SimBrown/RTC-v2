@extends('admin.dash-layout')
@section('content')


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="container">

            <div class="row">
                <h2>Gestione Sezioni</h2>
            </div>
            
            <table id="sec-table" class="table table-fixed table-striped jambo_table ">
                <thead style="padding-right:18px;">
                    <tr>
                        <th class="col" style="border-bottom:0px!important;width:20%;">Id</th>
                        <th class="col" style="border-bottom:0px!important;width:30%;">Nome Sezione</th>
                        <th class="col" style="border-bottom:0px!important;width:25%;"></th>
                        <th class="col" style="border-bottom:0px!important;width:25%;"></th>
                    </tr>
                </thead>
                <tbody>
                  
                    @if(count($sections)>0)

                    @foreach ($sections as $s)
                            <tr style="cursor: pointer;">
                                <td class="col" style="width:20%;" id="cat_id">{{$s->id}}</th>
                                <td class="col" style="width:60%;" id="description">{{$s->description}}</td>
                                <td class="col" style="width:10%" ><a class="btn btn-warning btn-sm edit" style="margin:2px 5px 2px 5px">Modifica</a></td>
                                <td class="col" style="width:10%" ><a class="btn btn-danger btn-sm archive" style="margin:2px 5px 2px 5px">Archivia</a></td>
                            </tr>
                        @endforeach
                    @else
                            <tr>
                                <th> Ancora Nessuna Sezione!</th>
                            </tr>
                    @endif

                    
                </tbody>
            </table>
        </div>
        

    </div>
    <div class="col-md-1"></div>

        <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Inserisci o Modifica Sezioni <small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" method="POST" action="{{route('section.insertOrUpdate')}}" >
                      {{ csrf_field() }}
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome Sezione</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required name="form_description" id="form_description" class="form-control" placeholder="Nome Sezione" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Id Sezione</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input name="form_sec_id" id="form_cat_id" class="form-control" readonly="readonly" placeholder="Disabled Input" type="text" value="">
                        {{--  <input name="form_sec_id" type="hidden" id="form_sec_id" class="form-control" value="">  --}}
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Annulla</button>
                          <button type="submit" class="btn btn-success">Invia</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>
          </div>
         
    <script>

        jQuery(document).ready(function() {

        $("#sec-table").DataTable();
        
        $(".edit").click(function() {

            console.log("test");

            var cat_id = $(this).parent().parent().find("#cat_id").text()
            var body = $(this).parent().parent().find("#description").text()
            var sec_id = $(this).parent().parent().find("#section_id").text()
            var sec_name = $(this).parent().parent().find("#section_name").text()

            $("#form_cat_id").val(cat_id);
            $("#form_description").val(body);
            $("#form_sec_id").val(sec_id);
            $("#sec_select").val(sec_id);
            

            });
        });

        function resetForm(){
            $("#form_title").val("");
            $("#form_description").val("");
            $("#form_survey_id").val("");
        }
        jQuery(document).ready(function() {
          $(".archive").click(function() {
            var sec_id = $(this).parent().parent().find("#cat_id").text();

            if(confirm("Vuoi davvero eliminare la categoria: "+($(this).parent().parent().find("#description").text())+" ?"))
              $.ajax({
                url : "/admin/forum/section",
                type : "POST",
                data: {
                    'sec_id': sec_id, 
                    'archived': 1
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
            else
            return false;
          });
        });


    </script>
    
    
@endsection