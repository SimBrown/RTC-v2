@extends('admin.dash-layout')
@section('content')


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="container">

            <div class="row">
                <h2>Gestione Categorie Archiviate</h2>
            </div>
            
            <table id="cat-table" class="table table-fixed table-striped jambo_table ">
                <thead style="padding-right:18px;">
                    <tr>
                        <th class="col" style="border-bottom:0px!important;width:10%;">Id</th>
                        <th class="col" style="border-bottom:0px!important;width:30%;">Nome Categoria</th>
                        <th class="col" style="border-bottom:0px!important;width:10%;">Id Sezione</th>
                        <th class="col" style="border-bottom:0px!important;width:30%;">Nome Sezione</th>
                        <th class="col" style="border-bottom:0px!important;width:20%;"></th>
                    </tr>
                </thead>
                <tbody>
                  
                    @if(count($categories)>0)

                    @foreach ($categories as $s)
                            <tr style="cursor: pointer;">
                                <td class="col" style="width:10%;" id="cat_id">{{$s->id}}</th>
                                <td class="col" style="width:30%;" id="description">{{$s->description}}</td>
                                <td class="col" style="width:10%;" id="section_id">{{$s->section}}</td>
                                <td class="col" style="width:30%;" id="section_name">{{$s->section_name}}</td>
                                <td class="col" style="width:20%;" ><a class="btn btn-success btn-sm enable" style="margin:2px 5px 2px 5px">Abilita</a></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
    

            
    <script>

        jQuery(document).ready(function() {
        
        $('#cat-table').DataTable();

        $(".edit").click(function() {

            console.log("test");

            var cat_id = $(this).parent().parent().find("#cat_id").text()
            var body = $(this).parent().parent().find("#description").text()
            var sec_id = $(this).parent().parent().find("#section_id").text()
            var sec_name = $(this).parent().parent().find("#section_name").text()

            $("#form_cat_id").val(cat_id);
            $("#form_description").val(body);
            $("#form_sec_id").val(sec_id);
            $("#sec_select").val(sec_id);
            

            });
        });

        function resetForm(){
            $("#form_title").val("");
            $("#form_description").val("");
            $("#form_survey_id").val("");
        }
        jQuery(document).ready(function() {
          $(".enable").click(function() {
            var cat_id = $(this).parent().parent().find("#cat_id").text();

            if(confirm("Vuoi davvero abilitare la categoria: "+($(this).parent().parent().find("#description").text())+" ?"))
              $.ajax({
                url : "/admin/forum/category",
                type : "POST",
                data: {
                  'cat_id': cat_id,
                  'archived': 0
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
            else
            return false;
          });
        });


    </script>
    
    
@endsection