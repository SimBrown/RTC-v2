@extends('admin.dash-layout')

@section('content')
                
<div class="row">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
            <div class="x_panel" style="min-height:90vh">
              <div class="x_title">
                <h2>Impostazioni<small>permessi</small></h2>
                <div class="clearfix"></div>
              </div>
              <br>
                <div class="x_content">

                        <div class="panel-body">
                            <p>
                                Access control list is a list of permissions attached to customer roles. This list specifies the access rights of users to objects.
                            </p>
                                <div class="col-md-3">
                                    <div class="scroll-wrapper">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr style="height: 57px;">
                                                    <th scope="col">
                                                        <strong>Nome dell'autorizzazione</strong>
                                                    </th>
                                                    
                                                </tr>
                                                    
                                                    @if($services)
                                                    @foreach($services as $s)
                                                    <tr>
                                                        <td>
                                                            <span>{{$s->description}}</span>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-9" style="margin-bottom:10px">
                                    <div class="scroll-wrapper" style="overflow-x:auto;">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                @if($groups)
                                                    @foreach($groups as $g)
                                                        <th scope="col">
                                                            <div class="checkbox">
                                                                <label>
                                                                @if($g->id == 1) 
                                                                        <input class="selectAll" ctrl="allow_{{$g->id}}" disabled="disabled" type="checkbox">
                                                                    @else
                                                                        <input class="selectAll" ctrl="allow_{{$g->id}}" type="checkbox">
                                                                    @endif
                                                                    
                                                                    <strong>{{$g->name}}</strong>
                                                                </label>
                                                            </div>
                                                        </th>
                                                    @endforeach
                                                @endif
                                                </tr>
                                                    
                                                    @if($services)
                                                    @foreach($services as $s)
                                                    <tr>
                                                        @if($groups)
                                                            @foreach($groups as $g)
                                                                <td style="min-width:100px">
                                                                    @if($g->id == 1) 
                                                                        <input id="{{$g->id}}_{{$s->id}}" class="allow_{{$g->id}}" disabled="disabled" service="{{$s->id}}"  type="checkbox">
                                                                    @else
                                                                        <input id="{{$g->id}}_{{$s->id}}" class="allow_{{$g->id}}" service="{{$s->id}}"  type="checkbox">
                                                                    @endif
                                                                </td>
                                                            @endforeach
                                                        @endif     
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <button id="reset" onclick="location.reload(true);"class="btn btn-primary">Annulla</button>
                                    <button id="submit" type="submit" class="btn btn-success">Invia</button>
                                </div>
                        </div>
                </div>
            </div>
        </div>

        <script src="{{asset('js/admin/permission.js')}}"></script>

        <style>
        .radio, .checkbox {
                    width: 170px;
                }
        </style>
     
@endsection