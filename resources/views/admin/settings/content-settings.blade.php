@extends('admin.dash-layout')
@section('content')

        <div class="row">
          <div class="col-md-1 col-xs-1"></div>
          <div class="col-md-10 col-xs-10">
                  <div class="x_panel" style="min-height:90vh">
                    <div class="x_title">
                      <h2>Impostazioni<small>contenuti</small></h2>
                      <div class="clearfix"></div>
                    </div>
                    <br>
                      <div class="x_content">

                          <div class="col-xs-3">
                              <!-- required for floating -->
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#Header" data-toggle="tab" aria-expanded="false">Header</a>
                                </li>
                                <li class=""><a href="#Footer" data-toggle="tab" aria-expanded="false">Footer</a>
                                </li>
                                <li class=""><a href="#Home-Page" data-toggle="tab" aria-expanded="false">HomePage</a>
                                </li>
                                <li class=""><a href="#Forum" data-toggle="tab" aria-expanded="false">Forum</a>
                                </li>
                              </ul>
                            </div>
                  
                            <div class="col-xs-9">
                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div class="tab-pane active" id="Header"><h2 style="text-align:center"><b>Header</b></h2>
                                  <div class="x_content" style="min-height:90vh">
                                      <br>
                                      <form class="form-horizontal form-label-left input_mask" method="post" action="{{route('settings.content')}}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Telefono</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" placeholder="Telefono" name="tel_number" value="{{$settings['tel_number']}}" type="text">
                                          </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Indirizzo</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                              <input class="form-control" placeholder="Indirizzo" name="header_address" value="{{$settings['header_address']}}" type="text">
                                            </div>
                                          </div>

                                        <div class="ln_solid"></div>
                                        <div class="form-group" style="position:relative;bottom:0;">
                                          <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                            <button class="btn btn-primary" type="reset">Reset</button>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Footer"><h2 style="text-align:center"><b>Footer</b></h2>
                                  <div class="x_content">
                                      <br>
                                      <form class="form-horizontal form-label-left input_mask" method="post" action="{{route('settings.content')}}">
                                        {{ csrf_field() }}
                                        <div class="x_title">
                                          <h2><small>Colonna 1</small></h2>
                                          <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Titolo Colonna 1</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" placeholder="Titolo Colonna 1" name="footer_column_1_title" value="{{$settings['footer_column_1_title']}}" type="text">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Contenuto colonna 1</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea row="4" class="form-control" placeholder="Contenuto colonna 1" name="footer_column_1_content" type="text">{{$settings['footer_column_1_content']}}</textarea>
                                          </div>
                                        </div>
                                        <div class="x_title">
                                          <h2><small>Colonna 2</small></h2>
                                          <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Titolo Colonna 2</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" placeholder="Titolo Colonna 2" value="{{$settings['footer_column_2_title']}}" type="text" name="footer_column_2_title">
                                          </div>
                                        </div>

                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Email colonna 2</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" placeholder="Email Colonna 2" value="{{$settings['footer_column_2_email']}}" type="text" name="footer_column_2_email">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Indirizzo colonna 2</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea rows="3" class="form-control" placeholder="Indirizzo colonna 2" type="text" name="footer_column_2_address">{{$settings['footer_column_2_address']}}</textarea>
                                          </div>
                                        </div>

                                        <div class="x_title">
                                          <h2><small>Colonna 3</small></h2>
                                          <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Titolo colonna 3</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" placeholder="Titolo Colonna 3" value="{{$settings['footer_column_3_title']}}" type="text" name="footer_column_3_title">
                                          </div>
                                        </div>
                                        
                                        <div class="x_title">
                                          <h2><small>Colonna 4</small></h2>
                                          <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Titolo colonna 4</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" placeholder="Titolo Colonna 4" value="{{$settings['footer_column_4_title']}}" type="text" name="footer_column_4_title">
                                          </div>
                                        </div>

                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Contenuto colonna 4</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea rows="3" class="form-control" placeholder="Contenuto colonna 4" type="text" name="footer_column_4_content">{{$settings['footer_column_4_content']}}</textarea>
                                          </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                          <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                            <button class="btn btn-primary" type="reset">Reset</button>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Home-Page"><h2 style="text-align:center"><b>HomePage</b></h2>
                                  <div class="x_content">
                                      <br>
                                      <form class="form-horizontal form-label-left input_mask" method="post" action="{{route('settings.content')}}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Default Input</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" placeholder="Default Input" type="text">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Disabled Input </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" disabled="disabled" placeholder="Disabled Input" type="text">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Read-Only Input</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" readonly="readonly" placeholder="Read-Only Input" type="text">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                          </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                          <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                            <button class="btn btn-primary" type="reset">Reset</button>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Forum"><h2 style="text-align:center"><b>Forum</b></h2>
                                  <div class="x_content">
                                      <br>
                                      <form class="form-horizontal form-label-left input_mask" method="post" action="{{route('settings.content')}}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Default Input</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" placeholder="Default Input" type="text">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Disabled Input </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" disabled="disabled" placeholder="Disabled Input" type="text">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Read-Only Input</label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control" readonly="readonly" placeholder="Read-Only Input" type="text">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                          </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                          <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                            <button class="btn btn-primary" type="reset">Reset</button>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                </div>
                              </div>
                            </div>
                  
                          <div class="clearfix"></div>
      
                        </div>
                    </div>
                  </div>
          </div>
          <div class="col-md-1 col-xs-1"></div>
        </div>

          <style>
                table {
                    width: 50%;
                }
                
                thead, tbody, tr, td, th {
                    display: block;
                }
                
                thead th {
                    height: 30px;
                }
                
                tbody {
                    overflow: -moz-scrollbars-vertical;
                    overflow-y: scroll;
                    height: 273px;
                }
                
                tbody td, thead th {
                    float: left;
                    width: 20%;
                }
                
                tr:after {
                    clear: both;
                    content: ' ';
                    display: block;
                    visibility: hidden;
                }
            </style>
            
    <script>

        jQuery(document).ready(function() {
        $(".edit").click(function() {

            console.log("test");

            var cat_id = $(this).parent().parent().find("#cat_id").text()
            var body = $(this).parent().parent().find("#description").text()
            var sec_id = $(this).parent().parent().find("#section_id").text()
            var sec_name = $(this).parent().parent().find("#section_name").text()

            $("#form_cat_id").val(cat_id);
            $("#form_description").val(body);
            $("#form_sec_id").val(sec_id);
            $("#sec_select").val(sec_id);
            

            });
        });

        function resetForm(){
            $("#form_title").val("");
            $("#form_description").val("");
            $("#form_survey_id").val("");
        }
        jQuery(document).ready(function() {
          $(".delete").click(function() {
            var cat_id = $(this).parent().parent().find("#cat_id").text();

            if(confirm("Vuoi davvero eliminare la categoria: "+($(this).parent().parent().find("#description").text())+" ?"))
              $.ajax({
                url : "/admin/forum/category",
                type : "POST",
                data: {
                  'cat_id': cat_id,
                  'archived': 1
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                    alert('An error occurred.');
                }
            });
            else
            return false;
          });
        });


    </script>
    
    
@endsection