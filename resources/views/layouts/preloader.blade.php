<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="  crossorigin="anonymous"></script>

<style>
        body {
            font-family: "Roboto", sans-serif;
        }
        
        button {
            display: block;
            width: 100%;
            padding: 20px;
        }
        
        /* Interesting part below this line */
        body {
            overflow: hidden;
            margin-right: 0px;
        }
        
        body.loaded {
            overflow: auto;
            margin-right: 0;
        }
        
        div.loader {

            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            
            background-color: rgba(20,20,20,1);
            color: #fff;
            
           display: flex;
           align-items: center;
           justify-content: center;
            
            z-index: 1050;
            overflow: hidden;
            
            transition: opacity 300ms linear 101ms, z-index 1ms linear 100ms;
        }
        
        /* fade loader after loaded */
        body.loaded > div.loader {
            opacity: 0;
            z-index: -1000;
            transition: opacity 300ms linear 100ms, z-index 1ms linear 400ms;
        }
        
        /* scale logo after loaded */
        div.loader > div.logo-container {
            transition: transform 400ms ease-out;
        }
        
        body.loaded > div.loader > div.logo-container {
            transform: scale(50);
            transition: transform 400ms ease-in;
        }
        
        div.loader > div.logo-container > svg {
            width: 100px;
            animation: heartbeat 1200ms infinite cubic-bezier(0.18, 0.89, 0.4, 1.67);
        }
        
        body.loaded > div.loader > div.logo-container > svg {
            animation: none;
        }
        
        
        div.loader > div.logo-container > svg > .spinner {
            stroke: #fff;
            stroke-width: 2px;
           stroke-dasharray: 200;
           stroke-dashoffset: 100;
            transform-origin: center center;
            transform: rotate(-90deg);
            animation: spin 500ms infinite linear;
        }
        
        body.loaded > div.loader > div.logo-container > svg > .spinner {
            animation: none;
        }
        
        div.wrapper {
            width: 1000px;
            margin: 0 auto;
            filter: blur(10px);
            transition: filter 400ms ease;
        }
        
        body.loaded > div.wrapper {
            filter: blur(0);
        }
        
        @keyframes spin {
            from {
                transform: rotate(-90deg);
            }
            
            to {
                transform: rotate(270deg);
            }
        }
        
        @keyframes heartbeat {
            0% {
                transform: scale(1);
            }
            12% {
                transform: scale(1.2);
            }
            24% {
                transform: scale(1);
            }
            36% {
                transform: scale(1.2);
            }
            48% {
                transform: scale(1);
            }
        }
    </style>
    


<div class="loader">
        <div class="logo-container">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                <linearGradient id="a" x1="10.33" x2="54.17" y1="53.67" y2="9.83" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#ff0674"/>
                    <stop offset="1" stop-color="#ff813f"/>
                </linearGradient>
                <circle cx="32" cy="32" r="31" fill="url(#a)"/><path fill="#fff" d="M33 14.7v.5c.2.8.2 3.5.1 4.4-.1.6-.1.9-.2 1.1 0 .1-.1.2-.1.3-.1.7-.5 1.8-.9 2.6-1.1 2.2-3 3.9-5.5 4.9l-.6.3-.1-.2c-.6-.8-1.2-2.7-1.4-4.3-.1-.8-.1-1-.2-1.1-.1-.2-.1-.2-.4 0l-.4.2-.8.6c-2.8 2.1-5 5.5-5.6 8.9-.2 1-.2 3.1 0 4.3.1.7.6 2.5.7 2.8 0 .1.1.2.2.4.2.5.9 1.9 1.2 2.4.6 1 1.2 1.7 2 2.5s1.9 1.6 2.8 2.1c.1.1.4.2.6.3.8.4 2.3 1 3.5 1.3 1 .2 1.1.3 2.5.4.5.1 3.3 0 3.5 0 .1 0 .3-.1.5-.1s.6-.1.8-.2c.3-.1.6-.1.7-.1 1.1-.2 3.2-1.1 4.4-1.8 1.4-.9 2.9-2.2 3.8-3.4.1-.1.2-.3.3-.4 1.3-1.7 2.4-4.3 2.7-6.4 0-.2 0-.3.1-.4.1-.7.1-2 .1-3.3-.1-.9-.1-1-.3-2.7-.1-.4-.5-2-.7-2.6-.4-1.1-1-2.5-1.5-3.4-.4-.7-.5-.9-.7-1.2-.2-.2-.3-.4-.3-.4l-.1-.1c0-.1-.5-.7-1.1-1.5-1.8-2.1-4.1-4.1-6.6-5.6-1-.6-2.4-1.3-2.5-1.3-.3 0-.4.1-.5.2z"/>
                <circle class="spinner" fill="none" stroke-width="6" stroke-linecap="round" cx="32" cy="32" r="30"></circle></svg>
        </div>
    </div>
<script>
        $(window).scrollTop(0);

            $(document).ready(function() {
                setTimeout(function(){
                    
                    $(".loader").css("display","none");
                    $("body").css("overflow","auto");
                }, 500);
            });
</script>