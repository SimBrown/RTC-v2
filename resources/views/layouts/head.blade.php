<title>Company Page - Home</title>

<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<!-- Auth Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Main Font -->
<script src="/js/webfontloader.min.js"></script>
<script>
    WebFont.load({
        google: {
            families: ['Roboto:300,400,500,700:latin']
        }
    });
</script>

<!-- Remove Bg animation for performances -->
<style>
    .content-bg-wrap{
        -webkit-animation: none!important;
        animation: none!important;
    }
</style>



<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/Bootstrap/dist/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="/Bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="stylesheet" type="text/css" href="/css/jquery.mCustomScrollbar.min.css">
<script src="{{asset('js/jquery.min.js')}}"></script>


<!-- Main Styles CSS -->
{{-- <link rel="stylesheet" type="text/css" href="css/main.css"> --}}
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="/css/custom.css">
<link rel="stylesheet" type="text/css" href="/css/fonts.min.css">
<link rel="stylesheet" type="text/css" href="/css/summernote.css">