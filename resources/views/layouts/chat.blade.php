<!-- Fixed Sidebar Right -->

<div id="chat-sidebar" class="fixed-sidebar right">
	<div class="fixed-sidebar-right sidebar--small" id="">

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="chat-users">
				@foreach($friends as $f)
				<li id="chat-friend-{{$f->user_id}}" friend-id="{{$f->user_id}}" class="inline-items js-chat-open chat-user">
					<div class="author-thumb">
						<img alt="author" src="{{ asset('storage/' . $f->image) }}" style="width: 38px;height: 38px;" class="avatar" data-toggle="tooltip" data-placement="left" data-original-title="{{$f->name." ".$f->surname}}">
						<span class="icon-status online"></span>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

		<a class="olympus-chat inline-items" onclick="$('.fixed-sidebar').toggleClass('open');">
			<svg class="olymp-chat---messages-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-menu-icon"></use></svg>
		</a>

	</div>

	<div class="fixed-sidebar-right sidebar--large" id="sidebar-right-1">

		<div class="mCustomScrollbar" data-mcs-theme="dark">

			<ul class="chat-users">
				@foreach($friends as $f)
				<li id="chat-friend-{{$f->user_id}}" friend-id="{{$f->user_id}}"class="inline-items js-chat-open chat-user">

					<div class="author-thumb">
						<img alt="author" src="{{ asset('storage/' . $f->image) }}" style="width: 38px;height: 38px;" class="avatar avatar-chat">
						<span class="icon-status online"></span>
					</div>
					<div class="author-status">
						<a class="h6 author-name">{{$f->name." ".$f->surname}}</a>
						<span class="status">AT WORK!</span>
					</div>

					<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>

						<ul class="more-icons">
							<li>
								<svg data-toggle="tooltip" data-placement="top" data-original-title="START CONVERSATION" class="olymp-comments-post-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use></svg>
							</li>

							<li>
								<svg data-toggle="tooltip" data-placement="top" data-original-title="ADD TO CONVERSATION" class="olymp-add-to-conversation-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-add-to-conversation-icon"></use></svg>
							</li>

							<li>
								<svg data-toggle="tooltip" data-placement="top" data-original-title="BLOCK FROM CHAT" class="olymp-block-from-chat-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-block-from-chat-icon"></use></svg>
							</li>
						</ul>

					</div>

				</li>
				@endforeach
			</ul>

		</div>

		<div class="search-friend inline-items">
			<form class="form-group" >
				<input class="form-control" placeholder="Search Friends..." value="" type="text">
			</form>

			<a href="29-YourAccount-AccountSettings.html" class="settings">
				<svg class="olymp-settings-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-settings-icon"></use></svg>
			</a>

			<a href="#" class="js-sidebar-open">
				<svg class="olymp-close-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-close-icon"></use></svg>
			</a>
		</div>

		<a href="#" class="olympus-chat inline-items" onclick="$('.fixed-sidebar').toggleClass('open');">

			<h6 class="olympus-chat-title">REAL CHAT</h6>
			<svg class="olymp-chat---messages-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-chat---messages-icon"></use></svg>
		</a>

	</div>
</div>

<!-- ... end Fixed Sidebar Right -->


<!-- Fixed Sidebar Right-Responsive -->

<div class="fixed-sidebar right fixed-sidebar-responsive">

	<div class="fixed-sidebar-right sidebar--small" id="sidebar-right-responsive">

		<a href="#" class="olympus-chat inline-items" onclick="$('.fixed-sidebar').toggleClass('open');">
			<svg class="olymp-chat---messages-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-chat---messages-icon"></use></svg>
		</a>

	</div>

</div>

<!-- ... end Fixed Sidebar Right-Responsive -->


<!-- Window-popup-CHAT for responsive min-width: 768px -->

<div class="ui-block popup-chat popup-chat-responsive" tabindex="-1" role="dialog" aria-labelledby="update-header-photo" aria-hidden="true">

		<div class="modal-content">
			<div class="modal-header">
				<span class="icon-status online"></span>
				<h6 class="title" >Chat</h6>
				<div class="more">
					<svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
					<svg class="olymp-little-delete js-chat-open"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-little-delete"></use></svg>
				</div>
			</div>
			<div class="modal-body">
				<div class="mCustomScrollbar">
					<ul class="notification-list chat-message chat-message-field">
						<li>
							<div class="author-thumb">
								<img src="{{asset('/img/avatar14-sm.jpg')}}" alt="author" class="mCS_img_loaded">
							</div>
							<div class="notification-event">
								<span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
							</div>
						</li>

						<li>
							<div class="author-thumb">
								<img src="{{asset('/img/author-page.jpg')}}" alt="author" class="mCS_img_loaded">
							</div>
							<div class="notification-event">
								<span class="chat-message-item">Don’t worry Mathilda!</span>
								<span class="chat-message-item">I already bought everything</span>
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:29pm</time></span>
							</div>
						</li>

						<li>
							<div class="author-thumb">
								<img src="{{asset('/img/avatar14-sm.jpg')}}" alt="author" class="mCS_img_loaded">
							</div>
							<div class="notification-event">
								<span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
							</div>
						</li>
					</ul>
				</div>

				<form class="need-validation">

			<div class="form-group label-floating is-empty">
				<label class="control-label">Press enter to post...</label>
				<textarea class="form-control" placeholder=""></textarea>
				<div class="add-options-message">
                    
                    <a href="#" class="options-message">
						<svg class="olymp-computer-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
					</a>
					
				</div>
			</div>

		</form>
			</div>
		</div>
	</div>
	
	<div id="chat-container" class="chat-container-inline">
		
	</div>
