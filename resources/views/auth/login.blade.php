<head>
	@include('layouts.head')

</head>

<body class="body-bg-white" style="overflow-x:hidden;">


	<div class="main-header main-header-fullwidth main-header-has-header-standard">


		<!-- Header Standard Landing  -->

		<div class="header--standard header--standard-landing" id="header--standard">
			<div class="container">
				<div class="header--standard-wrap">

					<a href="#" class="logo">
						<div class="img-wrap">
							<img src="{{asset('/img/logo.png')}}" alt="Olympus">
						</div>
						<div class="title-block">
							<h3 class="logo-title">rtc</h3>
							<h5 class="logo-title small">COMMUNITY</h5>
						</div>
					</a>

				</div>
			</div>
		</div>

		<!-- ... end Header Standard Landing  -->
		<div class="header-spacer--standard"></div>

		<div class="content-bg-wrap bg-landing"></div>

		<div class="container" style="max-width:80%">
			<div class="row display-flex">
				<div class="col col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12">
					<div class="landing-content">
						<h1>The best community in the world!</h1>
						<p>We are the best and biggest community with 5 billion active users all around the world. Share your thoughts, answer
							surveys, join the forum, and make the community even better!
						</p>
					</div>
				</div>

				<div class="col col-xl-8 col-lg-6 col-md-12 col-sm-12 col-12">

					<div class="col col-md-6" style="float:left;">

						<!-- Login-Registration Form  -->

						<div class="registration-login-form">
							<div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab" style="">
								<div class="title h6">Login to your Account</div>
								<form class="content" method="post" role="form" action="{{route('login')}}">
									@csrf
									<div class="row">
										<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
											@if(isset($errors) && in_array("password", $errors))
											<div class="form-group label-floating has-error is-invalid">
												<label class="control-label">Your Email</label>
												<input autocomplete="off" class="form-control" placeholder="" type="email" name="email" required>
											</div>
											<div class="form-group label-floating has-error is-invalid" style="margin-bottom:0px">
												<label class="control-label">Your Password</label>
												<input autocomplete="off" class="form-control" placeholder="" type="password" name="password" required>
											</div>
												<span class="error-span" style="color:red;margin-top:5px;display:inline-block"><b>Wrong Email or Password!</b></span>
											@else
											<div class="form-group label-floating">
												<label class="control-label">Your Email</label>
												<input autocomplete="off" class="form-control" placeholder="" type="email" name="email" required>
											</div>
											<div class="form-group label-floating">
												<label class="control-label">Your Password</label>
												<input autocomplete="off" class="form-control" placeholder="" type="password" name="password" required>
											</div>
											@endif

											<div class="remember">

												<div class="checkbox">
													<label>
														<input name="optionsCheckboxes" type="checkbox"> Remember Me
													</label>
												</div>
											</div>

											<button type="submit" class="btn btn-lg btn-primary full-width">Login</button>

										</div>
									</div>
								</form>
								<div style="display:flex">
									<img style="width:40%;margin:0 auto;position:relative;max-width: 147px;" src="{{asset('storage/images/login-icon.png')}}"></img>
								</div>
							</div>
						</div>
					</div>
					<!-- ... end Login-Registration Form  -->

					<!-- Registration -->
					<div class="col col-md-6" style="float:right;">

						<!-- Login-Registration Form  -->
						<img class="img-rocket" src="/img/rocket.png" alt="rocket">
						<div class="registration-login-form">
							<div class="tab-content">
								<div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab" style="">
									<div class="title h6">Register to Real Community</div>
									<form class="content need-validation" id="registration-form" role="presentation" method="post" action="{{route('register.post')}}" autocomplete="off">
										@csrf
										<div class="row">
											<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
												<div class="form-group label-floating">
													<label class="control-label">First Name</label>
													<input autocomplete="off" class="form-control" required="required" name="name" placeholder="" type="text">
													<span class="material-input"></span>
												</div>
											</div>
											<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
												<div class="form-group label-floating">
													<label class="control-label">Last Name</label>
													<input autocomplete="off" class="form-control" required="required"name="surname" placeholder="" type="text">
													<span class="material-input"></span>
												</div>
											</div>
											<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
												<div class="form-group label-floating is-focused">
													<label class="control-label">Your Email</label>
													<input autocomplete="off" class="form-control" required="required" name="email" placeholder="" type="email">
													<span class="material-input"></span>
												</div>

												<div class="form-group date-time-picker label-floating">
													<label class="control-label">Your Birthday</label>
													<input autocomplete="off" name="datetimepicker" required="required" type="date-cs" name="birthdate" value="">
													<span class="input-group-addon">
														<svg class="olymp-calendar-icon">
															<use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-calendar-icon"></use>
														</svg>
													</span>
												</div>

												<div class="form-group label-floating is-focused">
													<label class="control-label">Location</label>
													<input autocomplete="off" class="form-control" required="required" name="address" placeholder="" type="location">
													<span class="material-input"></span>
												</div>
												
												<div class="form-group label-floating is-focused">
													<label class="control-label">Your Password</label>
													<input autocomplete="nope" class="form-control" required="required" name="password" placeholder="" type="password">
													<span class="material-input"></span>
												</div>

												<div class="remember">
													<div class="checkbox">
														<label>
															<input autocomplete="off" id="accept-checkbox" name="optionsCheckboxes" required="required" type="checkbox">
															I accept the
															<a href="#">Terms and Conditions</a> of the website
														</label>
													</div>
														<br><span class="error-accept" style="color:red;margin-top:5px;display:inline-block"></span>
												</div>

												<button id="submit-registration" type="button" class="btn btn-purple btn-lg full-width">Complete Registration!</button>
												<button id="submit-registration-hidden" type="submit" hidden></button>
											</div>
										</div>
									</form>
								</div>

								<div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab" style="">
									<div class="title h6">Login to your Account</div>
									<form class="content">
										<div class="row">
											<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
												<div class="form-group label-floating is-empty">
													<label class="control-label">Your Email</label>
													<input class="form-control" placeholder="" type="email">
													<span class="material-input"></span>
												</div>
												<div class="form-group label-floating is-empty">
													<label class="control-label">Your Password</label>
													<input class="form-control" placeholder="" type="password">
													<span class="material-input"></span>
												</div>

												<div class="remember">

													<div class="checkbox">
														<label>
															<input name="optionsCheckboxes" type="checkbox">
															<span class="checkbox-material">
																<span class="check"></span>
															</span>
															Remember Me
														</label>
													</div>
													<a href="#" class="forgot">Forgot my Password</a>
												</div>

												<a href="#" class="btn btn-lg btn-primary full-width">Login</a>

												<div class="or"></div>

												<a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left">
													<svg class="svg-inline--fa fa-facebook-f fa-w-9" aria-hidden="true" data-prefix="fab" data-icon="facebook-f"
													 role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512" data-fa-i2svg="">
														<path fill="currentColor" d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path>
													</svg>
													<!-- <i class="fab fa-facebook-f" aria-hidden="true"></i> -->Login with Facebook</a>

												<a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left">
													<svg class="svg-inline--fa fa-twitter fa-w-16" aria-hidden="true" data-prefix="fab" data-icon="twitter" role="img"
													 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
														<path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
													</svg>
													<!-- <i class="fab fa-twitter" aria-hidden="true"></i> -->Login with Twitter</a>


												<p>Don’t you have an account?
													<a href="#">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<!-- ... end Login-Registration Form  -->
					</div>
					<!-- End Registration -->

				</div>


			</div>
		</div>

		
		
	</div>





	<!-- ... end Clients Block -->


	<!-- Section Img Scale Animation -->



	<!-- ... end Section Img Scale Animation -->





	<!-- Section Call To Action Animation -->

	<!-- <section class="align-right pt160 pb80 section-move-bg call-to-action-animation">
	<div class="container">
		<div class="row">
			<div class="col col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
				<a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#registration-login-form-popup">Start Making Friends Now!</a>
			</div>
		</div>
	</div>
	<img class="first-img" alt="guy" src="/img/guy.png">
	<img class="second-img" alt="rocket" src="/img/rocket1.png">
	<div class="content-bg-wrap bg-section1"></div>
</section>
 -->
	<!-- ... end Section Call To Action Animation -->




	<!-- ... end Footer Full Width -->




	<!-- Window-popup-CHAT for responsive min-width: 768px -->



	<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->




	@include('layouts.footer')

</body>

</html>