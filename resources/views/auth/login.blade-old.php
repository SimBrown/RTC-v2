        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

        <link href="{{ asset('css/login.css') }}" rel="stylesheet">
        <script src="{{ asset('js/login.js') }}"></script>

        <!------ Include the above in your HEAD tag ---------->

        <div class="container" style="justify-content: center;height:100%;display:flex">
                <div id="login-box">
                    <div class="logo">
                        <img src="" class="img img-responsive img-circle center-block"/>
                        <h1 class="logo-caption"><span class="tweak">L</span>ogin</h1>
                    </div><!-- /.logo -->
                    <div class="controls">
                        
                        <form class="form" role="form" method="post" action="{{route('login')}}" accept-charset="UTF-8" id="login-nav">    
                            @csrf    
                            <input type="text" name="email" placeholder="Email" required="required" class="form-control" />
                            <input type="password" name="password" placeholder="Password" required="required" class="form-control" />
                            <button type="submit" class="btn btn-default btn-block btn-custom">Login</button>
                        </form>

                    </div><!-- /.controls -->
                </div><!-- /#login-box -->
        </div><!-- /.container -->
            
        <div id="particles-js"></div>
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js"></script>



        