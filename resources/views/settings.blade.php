@extends('layouts.app')
@section('content')


<!-- ... end Responsive Header-BP -->
<div class="header-spacer"></div>

<div class="container">
	<div class="row">
		<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Personal Information</h6>
				</div>
				<div class="ui-block-content">

					
					<!-- Personal Information Form  -->
					
					<form method="POST" action="{{route('settings.update')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
						<div class="row">

						<div class="text-center col-lg-12" style="margin-bottom: 20px;">
						<a class="author-thumb">
						@if (Auth::user()->image)
						<div class="top-header-author">
							<img src="{{ asset('storage/' . Auth::user()->image) }}" style="width: 124px; height:124px; margin-bottom: 40px;" alt="author">
						</div>
						@else
						<div class="top-header-author">
								<img src="img/author-main1.jpg" alt="author" style="margin-bottom: 40px;">
						</div>
						@endif
						<input type="file"  name="image" id="image" class='image' style="border: 0; margin-top: 100px;">
						<input type="hidden"  name="user_id" id="user_id" value="{{ Auth::user()->id }}">
						</a>
						<div class="author-content">
							<a href="02-ProfilePage.html" class="h4 author-name"> </a>
						</div>
					</div>

					
							<div class="col col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating">
									<label class="control-label">Name</label>
									<input class="form-control" placeholder="" name="name" type="text" value="{{Auth::User()->name.' '.Auth::User()->surname}}">
								<span class="material-input"></span></div>
					
								<div class="form-group label-floating">
									<label class="control-label">Email address</label>
									<input class="form-control" type="email" name="email" value="{{Auth::user()->email}}">
								<span class="material-input"></span></div>
					
								<div class="form-group date-time-picker label-floating">
									<label class="control-label">Your Address</label>
									<input name="address" value="{{Auth::user()->address}}">
									<span class="input-group-addon">
															<svg class="olymp-month-calendar-icon icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-month-calendar-icon"></use></svg>
														</span>
								</div>
							</div>
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								
							</div>
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>
							</div>
					
						</div>
					</form>
					
					<!-- ... end Personal Information Form  -->
				</div>
			</div>
		</div>

		<div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">
			<div class="ui-block">

				
				
				<!-- Your Profile  -->
				
				<div class="your-profile">
					<div class="ui-block-title ui-block-title-small">
						<h6 class="title">Your PROFILE</h6>
					</div>
				
					<div id="accordion" role="tablist" aria-multiselectable="true">
						<div class="card">
							<div class="card-header" role="tab" id="headingOne">
								<h6 class="mb-0">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
										Profile Settings
									</a>
								</h6>
							</div>
						</div>
					</div>
				
					<div class="ui-block-title">
						<a href="/" class="h6 title">Calendar</a>
					</div>
					<div class="ui-block-title ui-block-title-small">
						<h6 class="title">FAVOURITE PAGE</h6>
					</div>
					<div class="ui-block-title">
						<a href="/forum" class="h6 title">Forum</a>
					</div>
					<div class="ui-block-title">
						<a href="/" class="h6 title">Home</a>
					</div>
				</div>
				
				<!-- ... end Your Profile  -->
				

			</div>
		</div>
	</div>
</div>

<script>
	const PRESSED = 1;
  	const NOT_PRESSED = 0;

	$('.news_like').on('click', function () {
      var news_like_span = this.querySelector('#news_like_span');
	  var news_like_a = this;
	  var new_value = news_like_a.style.fill == 'rgb(255, 94, 58)' ? NOT_PRESSED : PRESSED;
      var value = new_value == PRESSED ? NOT_PRESSED : PRESSED;
      $.ajax({
        url : "news/like",
        type : "POST",
        data: {'like': value, 'user_id': {{Auth::user()->id}}, 'news_id': news_like_span.dataset.newsId },
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        cache : false,
        success: function(response) {
			if (new_value == PRESSED) {
				news_like_a.style.fill = '#ff5e3a';
				news_like_span.style.color = '#ff5e3a';
				news_like_span.innerHTML =  Number(news_like_span.innerHTML) + 1;
			}
			else {
				news_like_a.style.fill = '#c2c5d9';
				news_like_span.style.color = '#c2c5d9';
				news_like_span.innerHTML =  Number(news_like_span.innerHTML) - 1;
			}
        },
        error: function(response) {
          alert('An error occurred.');
        }
        });
   	});

	$('.post_comment').on('click', function () {
      var user_id = this.dataset.userId;
	  var news_id = this.dataset.newsId;
	  var body = $('#comment_body'+news_id).val();
      $.ajax({
        url : "news/comment",
        type : "POST",
        data: {'body': body, 'user_id': user_id, 'news_id': news_id },
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        cache : false,
        success: function(response) {
			var comment = {};
			comment.name = '{{ Auth::user()->name }}';
			comment.body = body;
			comment.created_at = '{{ Carbon\Carbon::now() }}';
			addComment(news_id, comment);
			$('#news_comments_count'+news_id).text(Number($('#news_comments_count'+news_id).text()) + 1);
			$('#comment_body'+news_id).val('');
        },
        error: function(response) {
          alert('An error occurred.');
        }
	  });
   	});

	function showCommentTab(div) {
		var newsId = div.dataset.newsId;
		$('#news_comment_area'+newsId)[0].style.display = 'block';


		$.ajax({
        url : "news/comment",
        type : "GET",
        data: {'news_id': newsId },
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        cache : false,
        success: function(response) {
			for (var i = 0;i < response.news_comments.length; i++) {
				addComment(newsId, response.news_comments[i]);
			}
        },
        error: function(response) {
          alert('An error occurred.');
        }
	  });
	}

	function addComment(newsId, comment) {
		$('#comments_list' + newsId).append(' <li class="comment-item"> <div class="post__author author vcard inline-items"> <img src="img/author-page.jpg" alt="author"> <div class="author-date"> <a class="h6 post__author-name fn" href="02-ProfilePage.html">'+ comment.name + '</a> <div class="post__date"> <time class="published" datetime="2004-07-24T18:18"> ' + comment.created_at +' </time> </div> </div> <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg></a> </div> <p>' + comment.body +'</p></li>')
	}
</script>
@endsection
